/*
 * area.c
 *
 *  Created on: Nov 27, 2010
 *      Author: mritman
 */

#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "area.h"
#include "utlist.h"
#include "utils.h"

struct area* 		areas = NULL;												/* Important. Utils.h needs this to be initialized to NULL. */
pthread_rwlock_t 	areas_lock;

void area_init() {
	int rv;

    rv = pthread_rwlock_init(&areas_lock, NULL);
	if (rv != 0) {
		dno_print_error("Lock init failed on areas_lock");
		exit(EXIT_FAILURE);
    }
}

void initialize_router(struct router* router) {
	router->latency = 0;														/* 0 means the latency hasn't been measured yet. */
	router->connect = 0;														/* Specifies whether the local router will connect to this router or not. */
	router->estimated = 0;														/* Indicates whether the latency has been measured or estimated. */
}

void initialize_area(struct area* area) {
	area->routers 		= NULL;
	area->this_router 	= NULL;
	area->select_parent = 0;
}

void read_lock_areas() {
    if (pthread_rwlock_rdlock(&areas_lock) != 0) {
    	dno_print("read_lock_areas: Can't acquire readlock.\n");
    	exit(EXIT_FAILURE);
    }
}

void write_lock_areas() {
    if (pthread_rwlock_wrlock(&areas_lock) != 0) {
    	dno_print("read_lock_areas: Can't acquire writelock.\n");
    	exit(EXIT_FAILURE);
    }
}

void unlock_areas() {
	pthread_rwlock_unlock(&areas_lock);
}

void print_routers(struct router* routers) {
	struct router* router;

	DL_FOREACH(routers, router) {
		dno_printf(0, "\tRouter IP: %15s\tConnect: %d\tLatency: %6d μs\tEstimated: %d\n", router->ip_string, router->connect, router->latency, router->estimated);
	}
}

void print_areas() {
	struct area* area;

	read_lock_areas();
	DL_FOREACH(areas, area) {
		dno_printf(0, "========== Printing areas ==========");
		dno_printf(0, "Area ID: %d", area->area_id);
		dno_printf(0, "Routers:");
		print_routers(area->routers);
		dno_printf(0, "====================================");
	}
	unlock_areas();
}

void delete_router_from_areas_by_string(char* ip_string) {
	struct area* area;
	struct router* router, *tmp;

	write_lock_areas();

	DL_FOREACH(areas, area) {
		DL_FOREACH_SAFE(area->routers, router, tmp) {
			if (strncmp(router->ip_string, ip_string, INET6_ADDRSTRLEN) == 0) {
				DL_DELETE(area->routers, router);
				free(router);
			}
		}
	}

	unlock_areas();
}

void delete_router_from_areas(uint32_t router_id) {

	char ip_string[INET6_ADDRSTRLEN];

	inet_ntop(AF_INET, &(router_id), ip_string, INET6_ADDRSTRLEN);

	delete_router_from_areas_by_string(ip_string);
}

void free_routers(struct router* routers) {
	struct router* router;
	struct router* tmp;


	DL_FOREACH_SAFE(routers, router, tmp) {
		DL_DELETE(routers, router);
	    free(router);
	}
}

void free_areas() {
	struct area* area;
	struct area* tmp;

	write_lock_areas();
	DL_FOREACH_SAFE(areas, area, tmp) {
		free_routers(area->routers);
		DL_DELETE(areas, area);
	    free(area);
	}
	unlock_areas();
}

int	count_routers(struct router* routers) {
	struct router* 	router;
	int 			count = 0;

	DL_FOREACH(routers, router) {
		count++;
	}

	return count;
}

int sort_routers_by_ip(struct router* a, struct router* b) {
	char* suffix_a_string;
	char* suffix_b_string;
	int suffix_a;
	int suffix_b;

	suffix_a_string = strrchr(a->ip_string, '.')+1;
	suffix_b_string = strrchr(b->ip_string, '.')+1;

	suffix_a = atoi(suffix_a_string);
	suffix_b = atoi(suffix_b_string);

	if (suffix_a == suffix_b) {
		return 0;
	} else if (suffix_a > suffix_b) {
		return 1;
	} else {
		return -1;
	}
}

//struct router* find_lh_router(int place) {
//	struct router* 	router;
//	int 			best_suffix = 0;
//	struct router* 	best_ip_router = NULL;
//
//	DL_FOREACH(areas->routers, router) {
//		char* suffix = strrchr(router->ip_string, '.')+1;
//		int current_suffix = atoi(suffix);
//
//		if (place == HIGHEST) {
//			if (current_suffix > best_suffix) {
//				best_ip_router = router;
//				best_suffix = current_suffix;
//			}
//		} else if (place == LOWEST) {
//			if (current_suffix < best_suffix) {
//				best_ip_router = router;
//				best_suffix = current_suffix;
//			}
//		}
//	}
//
//	return best_ip_router;
//}
//
//struct router* find_middle_router(struct router* lowest, struct router* highest) {
//	struct router* router;
//
//	DL_FOREACH(areas->routers, router) {
//		if (router != lowest && router != highest) {
//			return router;
//		}
//	}
//
//	return NULL;
//}

int compare_routers(struct router* a, struct router* b) {
	if (a->latency < b->latency) {
		return -1;
	} else if (a->latency == b->latency) {
		return 0;
	} else {
		return 1;
	}
}
