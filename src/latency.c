/*
 * latency.c
 *
 *  Created on: May 13, 2010
 *      Author: mritman
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h>
#include <sys/time.h>
#include <string.h>

#include "latency.h"
#include "utils.h"
#include "socksvr.h"
#include "area.h"
#include "utlist.h"

#define ALPHA 									5													/* Dictates the accuracy of the estimations. A higher value means estimations are less likely to be accepted but leads to more measurements. */
#define NR_OF_MEASUREMENTS						4													/* The number of round trip measurements that are done and averaged to calculate the latency to a router. */
#define REQUEST_MEASURE							3
#define REQUEST_FINISH							4
#define RTT_SIZE								(INET6_ADDRSTRLEN + sizeof(uint32_t))				/* The number of bytes that are sent per round trip. Just the ip and latency. */

#define get_unknown_routers(area, count)		get_routers(area, count, 0)
#define get_known_routers(area, count)			get_routers(area, count, 1)

struct rtt {
	char		ip_string[INET6_ADDRSTRLEN];														/* The IP string of the router */
	uint32_t 	latency;																			/* Latency in microseconds */

	struct rtt* next;
	struct rtt* prev;
};

struct router_ptr {
	struct router* 		router;

	struct router_ptr* 	next;
	struct router_ptr* 	prev;
};

struct router_ptr* 	get_routers(struct area* area, int* count, int known);
struct router_ptr* 	get_unknown_router(struct router_ptr* unknown_routers, int index);
int 				measure_latency(int sockfd);
void 				send_replys(int socket);
struct rtt* 		receive_rtts(int sockfd, uint32_t area_id);
void 				free_rtts(struct rtt* rtts);
void				send_rtts();
struct area* 		get_area(int sockfd);
struct router_ptr* 	find_router(struct router_ptr* unknown_routers, struct rtt* rtt);

/* For each area
 * 		Make a list of pointers to the routers that have an unknown latency.
 * 		Randomly select from this list, measure/estimate, remove from this pointer list.
 * 		Repeat until all routers are known.
 */
void getlatencys() {
	struct area* area;

	setrandseed();																						/* Set the random seed for the random int generator. */

	read_lock_areas();																					/* Read-lock areas. */
	DL_FOREACH(areas, area) {
		struct router_ptr* 	unknown_routers;
		int 				unknown_routers_count;
		int					area_id = area->area_id;													/* Needed to indicate to the other router of which area to send its known latencys. */

		unknown_routers = get_unknown_routers(area, &unknown_routers_count);							/* Get a list of pointers to the routers of this area that need their latency measured. */

		unlock_areas();																					/* Unlock areas. */

		while(unknown_routers != NULL) {
			int 				sockfd;
			int 				random;
			struct router_ptr*	r;
			struct rtt* 		received_rtts;
			struct rtt*			rtt;

			random	= getrandomint(0, unknown_routers_count-1);
			r 		= get_unknown_router(unknown_routers, random);										/* Get a random router_ptr from the unknown_routers list. */

			read_lock_areas();																			/* Read-lock areas. */

			sockfd = connect_to_router(r->router, REQUEST_RTTS);										/* Connect to the router. */
			if (sockfd < 0) {
				DL_DELETE(unknown_routers, r);
				unknown_routers_count--;
				unlock_areas();
				delete_router_from_areas_by_string(r->router->ip_string);
				free(r);
				continue;
			}

			r->router->latency = measure_latency(sockfd);												/* Measure latency. */

			unlock_areas();																				/* Unlock areas. */

			received_rtts = receive_rtts(sockfd, area_id);												/* Receive the other router's round trips for this area. */

			close(sockfd);																				/* Close the socket since it will not be used anymore. */

			DL_DELETE(unknown_routers, r);																/* Remove r from unknown_routers. */
			unknown_routers_count--;																	/* Decrement routers_count. */

			/* Estimate latencys based on received latencys.
			 * 1. See if the received latency is in the unknown_routers list
			 * 2. If so try to estimate the latency.
			 * 3. If succesful remove the endpoint for which the latency was estimated from unkown_latencys and add it to known_latencys using mutex.
			 */
			DL_FOREACH(received_rtts, rtt) {
				struct router_ptr* matched_router;

				write_lock_areas();																		/* Get write-lock on areas. */

				matched_router = find_router(unknown_routers, rtt);										/* Look up the router to which this rtt belongs in the unknown_routers list. */

				if (matched_router == NULL) {
					//Do nothing																		/* The router belonging to this rtt was not in the unknown_routers list. */
				} else if (r->router->latency > (ALPHA * rtt->latency)) {
					matched_router->router->latency = r->router->latency;								/* The matched router is estimated to have the same latency as the latency from the local router to r. */
					matched_router->router->estimated = 1;

					DL_DELETE(unknown_routers, matched_router);											/* Remove matched router from unknown_routers. */
					free(matched_router);
					unknown_routers_count--;															/* Decrement unknown router count. */
				} else if (r->router->latency < (rtt->latency / ALPHA) ) {
					matched_router->router->latency = rtt->latency;										/* The matched router is estimated to have the same latency as from r to the matched router. */
					matched_router->router->estimated = 2;

					DL_DELETE(unknown_routers, matched_router);											/* Remove matched router from unknown_routers. */
					free(matched_router);
					unknown_routers_count--;															/* Decrement unknown router count. */
				}

				unlock_areas();																			/* Unlock areas. */
			}

			free_rtts(received_rtts);																	/* Free the list of received rtts. */

			free(r);																					/* Free random router_ptr. */
		}

		write_lock_areas();																				/* Areas needs to be locked again because the DL_FOREACH macro accesses it. Also, a write-lock (instead of just a read-lock) is used because the routers of the area are also being sorted. */

		DL_SORT(area->routers, compare_routers);														/* Sort routers based on latency. */
	}

	unlock_areas();
}

void *serve_latencys(void *arg) {
	int 				socket;
	struct arg_holder* 	arguments;
	struct area*		area;
	uint32_t 			known_router_count;
	struct router_ptr*	known_routers;

	arguments 	= (struct arg_holder*) arg;
	socket 		= arguments->fd;
	free(arguments);

	send_replys(socket);																				/* Send back the packets so that the client can measure the round trip. */

	read_lock_areas();

	area 			= get_area(socket);																	/* Get the area from the list of areas based on the area_id that is received. */

    known_routers 	= get_known_routers(area, (int *) &known_router_count);								/* Get the list of known routers for the area in question. */

    send_rtts(socket, known_routers, known_router_count);												/* Send the known router/latency pairs. */

    unlock_areas();

	close(socket);

	pthread_exit(NULL);
}

/* For each router in the area, create a router_ptr struct, store the pointer to the router in it and add it to the list of routers.
 * Store the total number of routers in *count.
 * Known = 0 means routers for which the latency is unknown are returned.
 * Known = 1 means routers for which the latency is known are returned.
 */
struct router_ptr* get_routers(struct area* area, int* count, int known) {
	struct router_ptr*	router_pointers = NULL;
	struct router* 		router;
	int	   				counter = 0;

	DL_FOREACH(area->routers, router) {
		struct router_ptr* new_ptr = NULL;

		if (router->latency == 0 && known == 0) {
			new_ptr = (struct router_ptr*) malloc(sizeof(struct router_ptr));
		} else if (router->latency > 0 && known == 1) {
			new_ptr = (struct router_ptr*) malloc(sizeof(struct router_ptr));
		}

		if (new_ptr != NULL) {
			new_ptr->router = router;
			DL_APPEND(router_pointers, new_ptr);
			counter++;
		}
	}

	*count = counter;

	return router_pointers;
}

/* Gets a router_ptr* from the list of unknown routers based on its position in the list. */
struct router_ptr* get_unknown_router(struct router_ptr* unknown_routers, int index) {
	struct router_ptr* result = unknown_routers;
	int i;

	for (i = 0; i < index; i++) {
		if (result->next == NULL) {
			dno_print("get_unknown_router: index out of range.");
			exit(EXIT_FAILURE);
		}
		result = result->next;
	}

	return result;
}

int measure_latency(int sockfd) {
	int 			avg_latency;
	int				latency_sum = 0;
	int 			byteswritten;
	int				bytesread;
	int				request_nbo;
	int				i;
	int				rv;
	struct timeval 	time_sent,
					time_received,
					time_delay;

	request_nbo = htons(REQUEST_MEASURE);

	for (i = 0; i < NR_OF_MEASUREMENTS; i++) {
		byteswritten = send(sockfd, &request_nbo, sizeof(uint16_t), 0);										/* Send short and measure time. */
		rv = gettimeofday(&time_sent, NULL);
		if (rv == -1) {
			dno_print_error("gettimeofday");
			exit(EXIT_FAILURE);
		}
		if (byteswritten == -1) {
			dno_print_error("measure_latency: send");
			exit(EXIT_FAILURE);
		}

		bytesread = recv(sockfd, &request_nbo, sizeof(uint16_t), 0);										/* Receive reply and measure time. */
		rv = gettimeofday(&time_received, NULL);
		if (rv == -1) {
			perror("gettimeofday");
			exit(EXIT_FAILURE);
		}
		if (bytesread == -1) {
			perror("recv");
			exit(EXIT_FAILURE);
		}

		time_delay.tv_sec = time_received.tv_sec - time_sent.tv_sec;										/* Calculate round-trip time. */
		time_delay.tv_usec = time_received.tv_usec - time_sent.tv_usec;

		latency_sum += (int)(time_delay.tv_sec * 1000000) + (int)time_delay.tv_usec;						/* Add to sum of measurements. */
	}

	avg_latency = latency_sum / NR_OF_MEASUREMENTS;															/* Calculate the average latency. */

	return avg_latency;
}

void send_replys(int socket) {
	int request_nbo;
	int i;
	int rv;

	for (i = 0; i < NR_OF_MEASUREMENTS; i++) {
		rv = recv(socket, &request_nbo, sizeof(uint16_t), 0);
		if (rv == -1) {
			dno_print_error("send_replys: recv");
			close(socket);
			exit(EXIT_FAILURE);
		}

		rv = send(socket, &request_nbo, sizeof(uint16_t), 0);
		if (rv == -1) {
			dno_print_error("send_replys: send");
			close(socket);
			exit(EXIT_FAILURE);
		}
	}
}

struct rtt* receive_rtts(int sockfd, uint32_t area_id) {
	int			rv;
	int			i;
	int			request_nbo;
	int			nr_of_rtts;
	uint32_t 	nr_of_rtts_nbo;
	struct rtt* received_rtts = NULL;

	rv = send(sockfd, &area_id, sizeof(uint32_t), 0);														/* Send the area_id */
	if (rv == -1) {
		dno_print_error("recv");
		exit(EXIT_FAILURE);
	}

	rv = recv(sockfd, &nr_of_rtts_nbo, sizeof(uint32_t), 0);												/* Ask number of latencys to receive. */
	if (rv == -1) {
		dno_print_error("recv");
		exit(EXIT_FAILURE);
	}
	nr_of_rtts = ntohl(nr_of_rtts_nbo);

	for (i = 0; i < nr_of_rtts; i++) {																		/* Receive latencys. */
		int 		bytesread 	= 0;
		struct rtt* new_rtt 	= (struct rtt*) malloc(sizeof(struct rtt));
		char*		buffer 		= (char*) malloc(RTT_SIZE);

		while (bytesread != RTT_SIZE) {
			rv = recv(sockfd, buffer+bytesread, RTT_SIZE - bytesread, 0);
			if (bytesread == -1) {
				dno_print_error("receive_rtts: recv");
				exit(EXIT_FAILURE);
			}

			bytesread += rv;
		}

		memcpy(new_rtt->ip_string, buffer, INET6_ADDRSTRLEN);
		memcpy(&(new_rtt->latency), buffer+INET6_ADDRSTRLEN, sizeof(new_rtt->latency));
		free(buffer);

		new_rtt->latency = ntohl(new_rtt->latency);															/* Convert the latency to host byte order. */
																											/* The next and prev pointer of new_rtt do not have to be initialized. */
		DL_APPEND(received_rtts, new_rtt);																	/* Add the received round trip to the list. */
	}

	request_nbo = htons(REQUEST_FINISH);																	/* Tell the server that it can close the connection. */
	rv = send(sockfd, &request_nbo, sizeof(uint16_t), 0);
	if (rv == -1) {
		dno_print_error("receive_rtts: send");
		exit(EXIT_FAILURE);
	}

	return received_rtts;
}

void send_rtts(int socket, struct router_ptr* known_routers, int count) {
	uint32_t 			known_routers_count;
	uint16_t 			request_nbo;
	struct router_ptr* 	router, *tmp;
	int					rv;

    known_routers_count = htonl(count);																		/* Send number of known_latencys */
    rv = send(socket, &known_routers_count, sizeof(uint32_t), 0);
	if (rv == -1) {
		dno_print_error("send_rtts: send");
		close(socket);
		exit(EXIT_FAILURE);
	}

	DL_FOREACH(known_routers, router) {
		int		byteswritten = 0;
		char* 	buffer = (char*) malloc(RTT_SIZE);
		int 	latency_nbo;
		int		n;
		int		len;

		memset(buffer, 0, RTT_SIZE);

		latency_nbo = htonl(router->router->latency);														/* Convert latency to network byte order. */
		n = INET6_ADDRSTRLEN;
		len = strlen(router->router->ip_string);
		memcpy(buffer, router->router->ip_string, len+1);
		memcpy(buffer+n, &latency_nbo, sizeof(uint32_t));

		while(byteswritten != RTT_SIZE) {																	/* Send the latency. */
			rv = send(socket, buffer+byteswritten, RTT_SIZE - byteswritten, 0);
			if (rv == -1) {
				dno_print_error("send_rtts: send");
				close(socket);
				exit(EXIT_FAILURE);
			}

			byteswritten += rv;
		}

		free(buffer);
	}

	rv = recv(socket, &request_nbo, sizeof(uint16_t), 0);													/* Wait until the client sends a reply indicating it is finished before closing the connection. */
	if (rv == -1) {
		dno_print_error("send_rtts: recv");
		close(socket);
		exit(EXIT_FAILURE);
	}

	DL_FOREACH_SAFE(known_routers, router, tmp) {															/* Free the list of known router pointers. */
		DL_DELETE(known_routers, router);
		free(router);
	}
}

void free_rtts(struct rtt* rtts) {
	struct rtt *rtt, *tmp;

    DL_FOREACH_SAFE(rtts, rtt, tmp) {
      DL_DELETE(rtts, rtt);
      free(rtt);
    }
}

struct area* get_area(int sockfd) {
	int 			rv;
	uint32_t 		area_id;
	struct	area* 	area = NULL;

	rv = recv(sockfd, &area_id, sizeof(uint32_t), 0);														/* Receive the area_id. */
	if (rv == -1) {
		dno_print_error("get_area: recv");
		close(sockfd);
		exit(EXIT_FAILURE);
	}

	area_id = ntohl(area_id);																				/* Convert to host byte order. */

	DL_FOREACH(areas, area) {																				/* Find the area in the areas list. */
		if (area->area_id == area_id) {
			return area;
		}
	}

	dno_print("get_area: area not found.");																	/* If the area was not found something is wrong. */
	exit(EXIT_FAILURE);
}

struct router_ptr* find_router(struct router_ptr* unknown_routers, struct rtt* rtt) {
	struct router_ptr* 	router_ptr = NULL;

	DL_FOREACH(unknown_routers, router_ptr) {
		if (strcmp(router_ptr->router->ip_string, rtt->ip_string) == 0) {
			return router_ptr;
		}
	}

	return router_ptr;
}
