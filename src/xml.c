/*
 * xml.c
 *
 *  Created on: Aug 2, 2010
 *      Author: mritman
 */
#include <string.h>

#include "xml.h"
#include "utils.h"
#include "utlist.h"
#include "area.h"

#define IP_RANGE_CHILDREN 3

struct ip_range {
	char 				ip_prefix[INET6_ADDRSTRLEN];
	uint16_t 			start;
	uint16_t 			end;

	struct ip_range* 	next;
	struct ip_range* 	prev;
};

/* Contains_ip checks if an IP occurs in an ip_range
 */
int contains_ip(struct ip_range* ranges, char* ip) {
	struct ip_range* range;

	DL_FOREACH(ranges, range) {
		char* 	id;
		int		ip_end;

		/* If range->ip_prefix is equal to the beginning of the string pointed to by ip
		 * then set the id pointer to the first character after this beginning. The remaining part of the ip string is converted to an int.
		 * If this number falls in the range between range->start and range->end return 1.
		 */
		if (strncmp(range->ip_prefix, ip, strlen(range->ip_prefix)) == 0) {
			id = ip + strlen(range->ip_prefix);
			ip_end = atoi(id);
			if (range->start <= ip_end && ip_end <= range->end) {
					return 1;
			}
		}
	}

	return 0;
}

void free_ranges(struct ip_range* ranges) {
	struct ip_range* range;
	struct ip_range* tmp;

	DL_FOREACH_SAFE(ranges, range, tmp) {
	      DL_DELETE(ranges, range);
	      free(range);
	}
}

int parse_range(xmlDocPtr doc, xmlNodePtr cur, struct ip_range* ip_range) {
	int 	res = 0;
	xmlChar *str;

	cur = cur->xmlChildrenNode;								/* Cur now points to the first child of the ip_range node */

	while (cur != NULL) {
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"ip"))) {
		    str = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
		    if (strlen((char*)str) >= sizeof(ip_range->ip_prefix)) {
		    	dno_print("Parse_range: Error: length of ip from <ip> member exceeds maximum length.\n");
		    	exit(EXIT_FAILURE);
		    }
		    strncpy(ip_range->ip_prefix, (char*)str, strlen((char*)str)+1);
		    xmlFree(str);
		    res++;
 	    } else if ((!xmlStrcmp(cur->name, (const xmlChar *)"start"))) {
 	    	str = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
 	    	ip_range->start = atoi((char*)str);
 	    	xmlFree(str);
 	    	res++;
 	    } else if ((!xmlStrcmp(cur->name, (const xmlChar *)"end"))) {
 	    	str = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
 	    	ip_range->end = atoi((char*)str);
 	    	xmlFree(str);
 	    	res++;
 	    }

	    cur = cur->next;
	}

	/* Res is incremented above each time a valid child node is found. Only when this number corresponds to IP_RANGE_CHILDREN this is considered a valid ip_range node */
    res = (res == IP_RANGE_CHILDREN ? 1 : 0);

    return res;
}

/* Constructs an area from the ranges and area_id */
void construct_area(int area_id, struct ip_range* ranges, struct area* area, char* local_ip, int select_parent) {
	struct ip_range* range;

	area->area_id 		= area_id;
	area->select_parent = select_parent;

	/* Create a router struct for each ip found in the ranges */
	DL_FOREACH(ranges, range) {
		int i;

		for (i = range->start; i <= range->end; i++) {
			struct router* 	router = (struct router*) malloc(sizeof(struct router));

			initialize_router(router);

			sprintf(router->ip_string, "%s%d", range->ip_prefix, i);					/* Concatenate the prefix and the suffix into router->ip_string */

			if (strncmp(router->ip_string, local_ip, strlen(local_ip)) == 0) {			/* Check if this router structure refers to this router */
				area->this_router = router;
			}

			DL_APPEND(area->routers, router);
		}
	}
}

struct area* parse_area(xmlDocPtr doc, xmlNodePtr cur, char* local_ip, struct area* areas) {
	struct ip_range* 	ranges	= NULL;						/* Needs to be initialized to NULL for use with utlist.h */
	int	   				area_id = -1;						/* Initialized to -1. If the value is still <0 after parsing it means that no valid area_id was specified */
	int					select_parent = 0;


	cur = cur->xmlChildrenNode;								/* Cur now points to the first child of the area node */

	while (cur != NULL) {
		int res = 0;

		if (!xmlStrcmp(cur->name, (const xmlChar *) "area_id")) {
			xmlChar *str;
 	    	str = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
 	    	area_id = atoi((char*)str);
 	    	xmlFree(str);
		} else if (!xmlStrcmp(cur->name, (const xmlChar *) "ip_range")) {
			struct ip_range* ip_range = (struct ip_range*) malloc(sizeof(struct ip_range));

			res = parse_range(doc, cur, ip_range);			/* Parse the range */

			if (res == 1) {									/* Only add the range if it was valid */
				DL_APPEND(ranges, ip_range);
			} else {										/* Else free the allocated memory */
				free(ip_range);
			}
		} else if (!xmlStrcmp(cur->name, (const xmlChar *) "select_parent")) {
			xmlChar *str;
			str = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			select_parent = atoi((char*)str);
			xmlFree(str);
		}

		cur = cur->next;
	}

	if (contains_ip(ranges, local_ip) && area_id >= 0) {
		struct area* new_area = (struct area*) malloc(sizeof(struct area));
		initialize_area(new_area);							/* Important. Initializes pointers to NULL for use with utlist.h */

		construct_area(area_id, ranges, new_area, local_ip, select_parent);

		DL_APPEND(areas, new_area);
	} else {
		dno_print("%s in ranges of this area?: %d. Area ID == %d", local_ip, contains_ip(ranges, local_ip), area_id);
	}

	free_ranges(ranges);

	return areas;
}

struct area* parse_topology(char* filename, char* local_ip) {
	struct	area* 	areas = NULL;						/* Important. Utlist.h needs this to be initialized to NULL. */
	xmlDocPtr 		doc;
	xmlNodePtr 		cur;

	if (areas != NULL) {
		dno_print("Parse_topology: invalid parameter: areas != NULL.");
		exit(EXIT_FAILURE);
	}

	xmlInitParser();

	doc = xmlParseFile(filename);

	if (doc == NULL ) {
		dno_print("Error parsing topology file.");
		exit(EXIT_FAILURE);
	}

	cur = xmlDocGetRootElement(doc);

	if (cur == NULL) {
		dno_print("Error parsing xml document: Empty document.");
		xmlFreeDoc(doc);
		exit(EXIT_FAILURE);
	}

	if (xmlStrcmp(cur->name, (const xmlChar *) "overlay_topology")) {
		dno_print("Error parsing xml document: document of the wrong type, root node != overlay_topology.");
		xmlFreeDoc(doc);
		exit(EXIT_FAILURE);
	}

	/* For each child of the root node check if it is called "area" and if so, parse it
	 */
	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if ((!xmlStrcmp(cur->name, (const xmlChar *) "area"))) {
			areas = parse_area(doc, cur, local_ip, areas);
		}

		cur = cur->next;
	}

	if (areas == NULL) {
		dno_print("%s: This node with IP: %s is not a member of an area defined in \"%s\"\n", hostname, local_ip, filename);
		xmlFreeDoc(doc);
		exit(EXIT_FAILURE);
	}

	/* Free the resources used by libxml */
	xmlFreeDoc(doc);
	xmlCleanupParser();

	return areas;
}
