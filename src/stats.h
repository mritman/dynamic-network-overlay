/*
 * stats.h
 *
 *  Created on: Jul 15, 2011
 *      Author: mritman
 *
 *      *      TODO
 *      - remove superfluous stat keeping in other modules.
 *      - allow for differentiation between requested LSAs and flooded LSAs
 */

#ifndef STATS_H_
#define STATS_H_

#include <stdint.h>

#define ACKREQS_SENT_PIGGYBACK		0
#define ACKREQS_SENT 				1
#define ACKS_SENT 					2
#define STOPS_SENT					3
#define RESUMES_SENT				4
#define ACKNOWLEDGES_SENT 			5
#define TERMINATIONS_SENT			6
#define PARENT_OFFERS_SENT			7
#define PARENT_ACCEPTS_SENT			8
#define PARENT_REJECTS_SENT			9

#define ACKREQS_RECEIVED_PIGGYBACK	10
#define ACKREQS_RECEIVED 			11
#define ACKS_RECEIVED 				12
#define STOPS_RECEIVED 				13
#define RESUMES_RECEIVED 			14
#define ACKNOWLEDGES_RECEIVED 		15
#define TERMINATIONS_RECEIVED 		16
#define PARENT_OFFERS_RECEIVED 		17
#define PARENT_ACCEPTS_RECEIVED 	18
#define PARENT_REJECTS_RECEIVED 	19

#define DD_PACKETS_SENT 			20
#define LSR_PACKETS_SENT 			21
#define LSU_PACKETS_SENT 			22

#define DD_PACKETS_RECEIVED 		23
#define LSR_PACKETS_RECEIVED 		24
#define LSU_PACKETS_RECEIVED 		25

#define DISCONNECTS_SENT 			26
#define DISCONNECTS_RECEIVED 		27

#define NUMBER_OF_LSAS_GENERATED 	28

#define DATA_PACKETS_FORWARDED 		29
#define DATA_PACKETS_RECEIVED 		30

#define DATA_PACKETS_ORIGINATED		31
#define DATA_PACKETS_DELIVERED 		32

extern int test_id;

struct stats {
	int termination_number;
	int termination_type;

	/* Termination detection stats. */
	uint32_t ackreqs_sent_piggyback;
	uint32_t ackreqs_sent;
	uint32_t acks_sent;
	uint32_t stops_sent;
	uint32_t resumes_sent;
	uint32_t acknowledges_sent;
	uint32_t terminations_sent;
	uint32_t parent_offers_sent;
	uint32_t parent_accepts_sent;
	uint32_t parent_rejects_sent;

	uint32_t ackreqs_received_piggyback;
	uint32_t ackreqs_received;
	uint32_t acks_received;
	uint32_t stops_received;
	uint32_t resumes_received;
	uint32_t acknowledges_received;
	uint32_t terminations_received;
	uint32_t parent_offers_received;
	uint32_t parent_accepts_received;
	uint32_t parent_rejects_received;

	/* Routing information stats. */
	uint32_t dd_packets_sent;
	uint32_t lsr_packets_sent;
	uint32_t lsu_packets_sent;
	//uint32_t lsas_sent; TODO differentiate between requested LSAs and flooded LSAs

	uint32_t dd_packets_received;
	uint32_t lsr_packets_received;
	uint32_t lsu_packets_received;
	//uint32_t lsas_received; TODO differentiate between requested LSAs and flooded LSAs

	uint32_t disconnects_sent;
	uint32_t disconnects_received;

	uint32_t number_of_lsas_generated;

	/* Data forwarding stats. */
	uint32_t data_packets_forwarded;
	uint32_t data_packets_received;
	uint32_t data_packets_originated;
	uint32_t data_packets_delivered;

	struct stats* prev;
	struct stats* next;
};

extern struct stats* stats;

void init_stats();

void free_stats();

void update_stats(int type);

void reset_stats();

void print_stats();

void print_stats_summary(struct timeval delay, struct timeval sync_delay, int startup, int disconnect);

#endif /* STATS_H_ */
