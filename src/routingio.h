/*
 * routingio.h
 *
 *  Created on: Aug 24, 2010
 *      Author: mritman
 */

#ifndef ROUTINGIO_H_
#define ROUTINGIO_H_

#include <netdb.h>
#include <stdint.h>

#include "uthash.h"
#include "utlist.h"

#define PACKET_SIZE 128000 					/* Bytes. */
#define BUFFER_SIZE 100						/* Packets. */
#define PACKET_HEADER						(sizeof(uint32_t) * 3 + sizeof(uint8_t) + sizeof(unsigned char))
#define FLAG_OFFSET							(sizeof(uint32_t) * 3 + sizeof(uint8_t))

#define write_lock_sets()					write_lock_sets_priv(__FILE__, __LINE__, __FUNCTION__)

extern struct routing_connection* full_set;

struct routing_connection {
	int 				fd;
	int					fd_info;			/* fd_info is only used for the termination detection algorithm by the routingio module. */
	uint32_t			peer;				/* Key for uthash. */
	char				neighbor_ip_string[INET6_ADDRSTRLEN];
	struct pckt* 		buffer;				/* List of packets to send out. */
	int 				buffer_count;		/* Number of packets in the buffer list. */
	struct pckt* 		recv_packet;		/* Packet struct to receive the next packet in. */
	struct queue_item* 	queue;				/* The list of routing_connections that are waiting to forward on this router_connection. */

	UT_hash_handle 		hh;					/* uthash handle for full set */
	UT_hash_handle 		hh_rs;				/* uthash handle for read set */
	UT_hash_handle 		hh_ws;				/* uthash handle for write set */
};

struct queue_item {
	struct routing_connection* 	waiting_connection;

	struct queue_item* 			next;
	struct queue_item* 			prev;
};

struct pckt {
	char* 			packet;
	uint32_t		dest;					/* Destination. */
	uint32_t		src;					/* Source. */
	uint32_t		size;					/* Size of the packet. */
	uint8_t			type;
	unsigned char	flags;					/* First bit is for term_turn: Switches each time termination occurs to distinguish packets from before and after termination. Second bit indicates an ACK should be sent. */

	uint32_t		pos;					/* The position in the packet of the last send/receive. */
	uint16_t		new;					/* Used when sending the packet to see if num_unack_message has to be incremented. True if new is 1. */

	struct 	pckt* 	prev;
	struct 	pckt* 	next;
};

void	routingio_init();

void 	unpack_packet_header(struct pckt* packet, char* buffer);

int 	write_packet_header(uint32_t dest, uint32_t src, uint32_t size, char* packet, uint8_t type);

struct 	pckt* init_packet();

struct 	routing_connection* initialize_routing_connection(int fd, int fd_info, char* ipstr, uint16_t their_port);

void 	add_to_full_and_read_set(struct routing_connection* connection);

struct 	routing_connection* find_connection_in_full_set(uint32_t peer);

int 	delete_routing_connection(uint32_t peer);

void 	free_rsws();

void 	free_receive_buffer();

void	free_send_buffer();

pthread_t create_routing_io_thread();

int		dno_recv(char* buffer, int size);

uint32_t 	dno_send(uint32_t dest, char* buffer, uint32_t size);

void	dno_send_debug(uint32_t destination, uint32_t size);

void 	print_routing_connections();

void 	print_data_sent();

void 	write_lock_sets_priv(const char *file, int line, const char *function);

void 	read_lock_sets();

void 	unlock_sets();

#endif /* ROUTINGIO_H_ */
