/*
 * xml.h
 *
 *  Created on: Aug 2, 2010
 *      Author: mritman
 */

#ifndef XML_H_
#define XML_H_

#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

#include "area.h"

struct area* parse_topology(char* filename, char* local_ip);

#endif /* XML_H_ */
