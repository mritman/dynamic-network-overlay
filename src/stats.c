/*
 * stats.c
 *
 *  Created on: Jul 15, 2011
 *      Author: mritman
 */

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

#include "stats.h"
#include "packettypes.h"
#include "utlist.h"
#include "utils.h"

int test_id;

int termination_number;
struct stats* stats;

pthread_rwlock_t stats_lock;

void create_stats_struct();

void init_stats() {
	int rv;

    rv = pthread_rwlock_init(&stats_lock, NULL);
	if (rv != 0) {
		dno_print_error("Failed to initialize stas_lock. Exiting.");
		exit(EXIT_FAILURE);
    }

	termination_number = 0;
	stats = NULL;
	reset_stats();
}

void read_lock_stats() {
    if (pthread_rwlock_rdlock(&stats_lock) != 0) {
    	dno_print("read_lock_stats: Can't acquire readlock.\n");
    	exit(EXIT_FAILURE);
    }
}

void write_lock_stats() {
    if (pthread_rwlock_wrlock(&stats_lock) != 0) {
    	dno_print("write_lock_stats: Can't acquire writelock.\n");
    	exit(EXIT_FAILURE);
    }
}

void unlock_stats() {
	if(pthread_rwlock_unlock(&stats_lock) != 0) {
		dno_print("Error unlocking stats_lock.\n");
		exit(EXIT_FAILURE);
	}
}

void reset_stats() {
	struct stats* new_stats = (struct stats*) malloc(sizeof(struct stats));

	new_stats->termination_number = termination_number++;

	/* Termination detection stats. */
	new_stats->ackreqs_sent_piggyback = 0;
	new_stats->ackreqs_sent = 0;
	new_stats->acks_sent = 0;
	new_stats->stops_sent = 0;
	new_stats->resumes_sent = 0;
	new_stats->acknowledges_sent = 0;
	new_stats->terminations_sent = 0;
	new_stats->parent_offers_sent = 0;
	new_stats->parent_accepts_sent = 0;
	new_stats->parent_rejects_sent = 0;

	new_stats->ackreqs_received_piggyback = 0;
	new_stats->ackreqs_received = 0;
	new_stats->acks_received = 0;
	new_stats->stops_received = 0;
	new_stats->resumes_received = 0;
	new_stats->acknowledges_received = 0;
	new_stats->terminations_received = 0;
	new_stats->parent_offers_received = 0;
	new_stats->parent_accepts_received = 0;
	new_stats->parent_rejects_received = 0;

	/* Routing information stats. */
	new_stats->dd_packets_sent = 0;
	new_stats->lsr_packets_sent = 0;
	new_stats->lsu_packets_sent = 0;
	//new_stats->lsas_sent = 0; TODO differentiate between requested LSAs and flooded LSAs

	new_stats->dd_packets_received = 0;
	new_stats->lsr_packets_received = 0;
	new_stats->lsu_packets_received = 0;
	//new_stats->lsas_received = 0; TODO differentiate between requested LSAs and flooded LSAs

	new_stats->disconnects_sent = 0;
	new_stats->disconnects_received = 0;

	new_stats->number_of_lsas_generated = 0;

	/* Data forwarding stats. */
	new_stats->data_packets_forwarded = 0;
	new_stats->data_packets_received = 0;
	new_stats->data_packets_originated = 0;
	new_stats->data_packets_delivered = 0;

	write_lock_stats();
	DL_PREPEND(stats, new_stats);
	unlock_stats();
}

void free_stats() {
	struct stats* current_stats, *tmp;

	write_lock_stats();

	DL_FOREACH_SAFE(stats, current_stats, tmp) {
		DL_DELETE(stats, current_stats);
		free(current_stats);
	}

	unlock_stats();
}

void update_stats(int type) {
	write_lock_stats();

	switch(type) {
		case ACKREQS_SENT_PIGGYBACK:
			stats->ackreqs_sent_piggyback++;
			break;
		case ACKREQS_SENT:
			stats->ackreqs_sent++;
			break;
		case ACKS_SENT:
			stats->acks_sent++;
			break;
		case STOPS_SENT:
			stats->stops_sent++;
			break;
		case RESUMES_SENT:
			stats->resumes_sent++;
			break;
		case ACKNOWLEDGES_SENT:
			stats->acknowledges_sent++;
			break;
		case TERMINATIONS_SENT:
			stats->terminations_sent++;
			break;
		case PARENT_OFFERS_SENT:
			stats->parent_offers_sent++;
			break;
		case PARENT_ACCEPTS_SENT:
			stats->parent_accepts_sent++;
			break;
		case PARENT_REJECTS_SENT:
			stats->parent_rejects_sent++;
			break;
		case ACKREQS_RECEIVED_PIGGYBACK:
			stats->ackreqs_received_piggyback++;
			break;
		case ACKREQS_RECEIVED:
			stats->ackreqs_received++;
			break;
		case ACKS_RECEIVED:
			stats->acks_received++;
			break;
		case STOPS_RECEIVED:
			stats->stops_received++;
			break;
		case RESUMES_RECEIVED:
			stats->resumes_received++;
			break;
		case ACKNOWLEDGES_RECEIVED:
			stats->acknowledges_received++;
			break;
		case TERMINATIONS_RECEIVED:
			stats->terminations_received++;
			break;
		case PARENT_OFFERS_RECEIVED:
			stats->parent_offers_received++;
			break;
		case PARENT_ACCEPTS_RECEIVED:
			stats->parent_accepts_received++;
			break;
		case PARENT_REJECTS_RECEIVED:
			stats->parent_rejects_received++;
			break;
		case DD_PACKETS_SENT:
			stats->dd_packets_sent++;
			break;
		case LSR_PACKETS_SENT:
			stats->lsr_packets_sent++;
			break;
		case LSU_PACKETS_SENT:
			stats->lsu_packets_sent++;
			break;
		case DD_PACKETS_RECEIVED:
			stats->dd_packets_received++;
			break;
		case LSR_PACKETS_RECEIVED:
			stats->lsr_packets_received++;
			break;
		case LSU_PACKETS_RECEIVED:
			stats->lsu_packets_received++;
			break;
		case DISCONNECTS_SENT:
			stats->disconnects_sent++;
			break;
		case DISCONNECTS_RECEIVED:
			stats->disconnects_received++;
			break;
		case NUMBER_OF_LSAS_GENERATED:
			stats->number_of_lsas_generated++;
			break;
		case DATA_PACKETS_FORWARDED:
			stats->data_packets_forwarded++;
			break;
		case DATA_PACKETS_RECEIVED:
			stats->data_packets_received++;
			break;
		case DATA_PACKETS_ORIGINATED:
			stats->data_packets_originated++;
			break;
		case DATA_PACKETS_DELIVERED:
			stats->data_packets_delivered++;
			break;
		default:
			break;
	}

	unlock_stats();
}

void print_stats_summary(struct timeval delay, struct timeval sync_delay, int startup, int disconnect) {
	struct stats* last_stats;

	if (gxp_id == 0 || 1) {
		if (startup && disconnect) {
			dno_print("Invalid arguments: (startup && disconnect) == 1");
			return;
		}

		printf("%d,", get_nr_of_gxp_processes());

		printf("\t%s,", hostname);

		if (startup) {
			printf("\tstartup,");
		} else if (disconnect) {
			printf("\tdisconnect,");
		} else {
			printf("\toverlaychange,");
		}

		read_lock_stats();

		if (stats->next != NULL) {
			last_stats = stats->next;
		} else {
			last_stats = stats;
		}

		printf("\t%d,\t%4ld.%06ld,\t%4ld.%06ld,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d\n",
				test_id,
				delay.tv_sec,
				delay.tv_usec,
				sync_delay.tv_sec,
				sync_delay.tv_usec,

				last_stats->termination_number,
				last_stats->termination_type,

				/* Termination detection stats. */
				last_stats->ackreqs_sent_piggyback,
				last_stats->ackreqs_sent,
				last_stats->acks_sent,
				last_stats->stops_sent,
				last_stats->resumes_sent,
				last_stats->acknowledges_sent,
				last_stats->terminations_sent,
				last_stats->parent_offers_sent,
				last_stats->parent_accepts_sent,
				last_stats->parent_rejects_sent,

				last_stats->ackreqs_received_piggyback,
				last_stats->ackreqs_received,
				last_stats->acks_received,
				last_stats->stops_received,
				last_stats->resumes_received,
				last_stats->acknowledges_received,
				last_stats->terminations_received,
				last_stats->parent_offers_received,
				last_stats->parent_accepts_received,
				last_stats->parent_rejects_received,

				/* Routing information stats. */
				last_stats->dd_packets_sent,
				last_stats->lsr_packets_sent,
				last_stats->lsu_packets_sent,
				//last_stats->lsas_sent, TODO differentiate between requested LSAs and flooded LSAs

				last_stats->dd_packets_received,
				last_stats->lsr_packets_received,
				last_stats->lsu_packets_received,
				//last_stats->lsas_received, TODO differentiate between requested LSAs and flooded LSAs

				last_stats->disconnects_sent,
				last_stats->disconnects_received,

				last_stats->number_of_lsas_generated,

				/* Data forwarding stats. */
				last_stats->data_packets_forwarded,
				last_stats->data_packets_received,
				last_stats->data_packets_originated,
				last_stats->data_packets_delivered);

		unlock_stats();

		fflush(stdout);
	}
}

void print_stats() {
	struct stats* last_stats;

	if (stats->next != NULL) {
		last_stats = stats->next;
	} else {
		last_stats = stats;
	}

	read_lock_stats();

	dno_printf(0, "========== Printing packet stats ==========");

	dno_printf(0, "Termination: %d\n\n", last_stats->termination_number);

	dno_printf(0, "Ackreqs sent (piggyback): %d\n", last_stats->ackreqs_sent_piggyback);
	dno_printf(0, "Ackreqs sent: %d\n", last_stats->ackreqs_sent);
	dno_printf(0, "Acks sent: %d\n", last_stats->acks_sent);
	dno_printf(0, "Stops sent: %d\n", last_stats->stops_sent);
	dno_printf(0, "Resumes sent: %d\n", last_stats->resumes_sent);
	dno_printf(0, "Acknowledges sent: %d\n", last_stats->acknowledges_sent);
	dno_printf(0, "Terminations sent: %d\n", last_stats->terminations_sent);
	dno_printf(0, "Parent offers sent: %d\n", last_stats->parent_offers_sent);
	dno_printf(0, "Parent accepts sent: %d\n", last_stats->parent_accepts_sent);
	dno_printf(0, "Parent rejects sent: %d\n", last_stats->parent_rejects_sent);

	dno_printf(0, "Ackreqs received (piggyback): %d\n", last_stats->ackreqs_received_piggyback);
	dno_printf(0, "Ackreqs received: %d\n", last_stats->ackreqs_received);
	dno_printf(0, "Acks received: %d\n", last_stats->acks_received);
	dno_printf(0, "Stops received: %d\n", last_stats->stops_received);
	dno_printf(0, "Resumes received: %d\n", last_stats->resumes_received);
	dno_printf(0, "Acknowledges received: %d\n", last_stats->acknowledges_received);
	dno_printf(0, "Terminations received: %d\n", last_stats->terminations_received);
	dno_printf(0, "Parent offers received: %d\n", last_stats->parent_offers_received);
	dno_printf(0, "Parent accepts received: %d\n", last_stats->parent_accepts_received);
	dno_printf(0, "Parent rejects received: %d\n", last_stats->parent_rejects_received);

	dno_printf(0, "DD packets sent: %d\n", last_stats->dd_packets_sent);
	dno_printf(0, "LSR packets sent: %d\n", last_stats->lsr_packets_sent);
	dno_printf(0, "LSU packets sent: %d\n", last_stats->lsu_packets_sent);

	dno_printf(0, "DD packets received: %d\n", last_stats->dd_packets_received);
	dno_printf(0, "LSR packets received: %d\n", last_stats->lsr_packets_received);
	dno_printf(0, "LSU packets received: %d\n", last_stats->lsu_packets_received);

	dno_printf(0, "Disconnects sent: %d\n", last_stats->disconnects_sent);
	dno_printf(0, "Disconnects received: %d\n", last_stats->disconnects_received);

	dno_printf(0, "Number of LSAs generated: %d\n", last_stats->number_of_lsas_generated);

	dno_printf(0, "Data packets forwarded: %d\n", last_stats->data_packets_forwarded);
	dno_printf(0, "Data packets received: %d\n", last_stats->data_packets_received);
	dno_printf(0, "Data packets originated: %d\n", last_stats->data_packets_originated);
	dno_printf(0, "Data packets delivered: %d\n", last_stats->data_packets_delivered);

	dno_printf(1, "===========================================");

	unlock_stats();
}
