/*
 * connections.c
 *
 *  Created on: May 24, 2010
 *      Author: mritman
 *
 *      TODO the ports that are exchanged when setting up connections in the establish_connection and serve_connection procedures are
 *      TODO actually the listening port the server is bound to
 *
 */
#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include <errno.h>

#include "utils.h"
#include "socksvr.h"
#include "connections.h"
#include "routinginfo.h"
#include "routingio.h"
#include "routingtable.h"
#include "area.h"
#include "packets.h"
#include "termination.h"

#define IS_IN_LIST 		3939
#define CONN_SUCCESS 	4000
#define CONN_ABORT		4001
//#define REQ_GTW_FLAG	4550 TODO remove

struct connection_args {
	uint32_t gxp_id;						/* The gxp_id is used to decide which router's connection request is aborted when two are trying to connect to each other at the same time. */
	uint32_t area_id;
};

struct connection* 	connections = NULL;
pthread_rwlock_t 	connections_lock;

char 				connecting_to_ip[INET6_ADDRSTRLEN];
int					connecting_to_port 	= 0;
pthread_mutex_t 	connecting_to_mutex = PTHREAD_MUTEX_INITIALIZER;

struct connection* initialize_connection(char* ipstr, uint16_t port, int fd, int fd_info, int area_id, int has_parent);
void read_lock_connections();
void write_lock_connections();
void unlock_connections();

void lock_connecting_to();
void unlock_connecting_to();

void reset_connecting_to() {
	lock_connecting_to();
	memset(connecting_to_ip, '\0', strlen(connecting_to_ip));
	connecting_to_port = 0;
	unlock_connecting_to();
}

void connections_init() {
	int rv;

    rv = pthread_rwlock_init(&connections_lock,NULL);							/* Initialize the lock used for shared variable connections */
	if (rv != 0) {
		dno_print_error("Lock init failed on connections_lock");
		exit(EXIT_FAILURE);
    }

	reset_connecting_to();
}

/* Only supports IP4. */
int calculate_key(char* ipstr, uint16_t port) {
	struct sockaddr_in sa;

	inet_pton(AF_INET, ipstr, &(sa.sin_addr));

	return sa.sin_addr.s_addr;
}

struct connection* find_connection(struct connection* connections_table, int key) {
    struct connection* connection;

  	HASH_FIND_INT(connections_table, &key, connection);

    return connection;
}

int is_in_table(struct connection* table, char* ipstr, uint16_t port) {
	struct connection* 	is_in_table;
	int 				key;
	int 				result;

	key = calculate_key(ipstr, port);

	is_in_table = find_connection(table, key);

	if (is_in_table == NULL) {
		result = 0;
	} else {
		result = 1;
	}

	return result;
}

int add_connection(char* ipstr, uint16_t port, int fd, int fd_info, int area_id, int has_parent) {
	int 				result = 0;
	struct connection* 	new_connection = initialize_connection(ipstr, port, fd, fd_info, area_id, has_parent);

	if (is_in_table(connections, ipstr, port)) {
		dno_print("Warning, new connection to %s already in connections table.", new_connection->neighbor_ip_string);
		free(new_connection);
		result = 0;
	} else {
		HASH_ADD_INT(connections, key, new_connection);
		result = 1;
	}

	return result;
}

int remove_connection(int key) {
	struct connection* 	connection;
	int 				result = 0;

	connection = find_connection(connections, key);

	if (connection != NULL) {
		HASH_DEL(connections, connection);
		close(connection->fd);

		free_lsa_headers(connection->database_summary_list); 						/* Free the database summary list. */
		free_lsa_keys(connection->link_state_request_list); 						/* Free LSR list. */

		if (connection->buffer != NULL) {
			free(connection->buffer);
		}

		free(connection);

	    result = 1;
	}

	return result;
}

int remove_connection_unlocked(int key) {
	int result = 0;

	write_lock_connections();

	result = remove_connection(key);

    unlock_connections();

    return result;
}

int try_different_port(struct addrinfo *p, int fd) {
	int rv;
	int i;
	int random_port;

    if (p->ai_family == AF_INET) { 																						/* IPv4. */
    	struct sockaddr_in *ipv4 = (struct sockaddr_in *) p->ai_addr;

    	for (i = 1; i < MAX_PORT_RETRYS; i++) {
    		random_port = getrandomint(MIN_TCP_PORT, MAX_TCP_PORT);														/* Random port between MIN_TCP_PORT and MAX_TCP_PORT. */

    		ipv4->sin_port = htons(random_port);

        	rv = bind(fd, (struct sockaddr *) ipv4, sizeof(struct sockaddr_in));
        	if (rv == 0) {
        		return ntohs(ipv4->sin_port);
        	}
        }
    } else { 																											/* IPv6. */
        struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)p->ai_addr;
        for (i = 1; i <= MAX_PORT_RETRYS; i++) {
        	random_port = getrandomint(MIN_TCP_PORT, MAX_TCP_PORT);

        	ipv6->sin6_port = htons(random_port);

        	rv = bind(fd, (struct sockaddr *) ipv6, sizeof(struct sockaddr_in));
        	if (rv == 0) {
        		return ntohs(ipv6->sin6_port);
        	}
        }
    }

    return -1;
}

int create_info_connection(int fd) {
	int 					rv;
	int 					yes = 1;
	struct addrinfo 		hints, *servinfo, *p;
	int						random_port;
	char 					portstring[5];
	int						fd_listen;
	int 					fd_info;
	uint16_t				port;
	int						port_int;
	uint16_t				port_nbo;
	struct sockaddr_storage their_addr;
	socklen_t 				sin_size;

	random_port = getrandomint(MIN_TCP_PORT, MAX_TCP_PORT);									/* Get a random port to listen on for the new TCP connection. */
	sprintf(portstring, "%d", random_port);

	memset(&hints, 0, sizeof hints); 														/* Make sure the struct is empty. */
	hints.ai_family 	= AF_UNSPEC;														/* Don't care IPv4 or IPv6 */
	hints.ai_socktype 	= SOCK_STREAM; 														/* TCP stream sockets. */
	hints.ai_flags 		= AI_PASSIVE;    													/* Fill in my IP for me. */

	rv = getaddrinfo(hostname, portstring, &hints, &servinfo);
	if (rv != 0) {
	    fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(rv));
	    exit(EXIT_FAILURE);
	}

    for(p = servinfo; p != NULL; p = p->ai_next) {											/* Loop through all the results and bind to the first we can. */
    	fd_listen = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
    	if (fd_listen == -1) {
            dno_print_error("create_info_connection: socket");
            continue;
        }

		rv = setsockopt(fd_listen, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
        if (rv == -1) {
            dno_print_error("create_info_connection: setsockopt");
            exit(EXIT_FAILURE);
        }

        rv = bind(fd_listen, p->ai_addr, p->ai_addrlen);
        if (rv == -1) {
        	if (errno == EADDRINUSE) {
        		rv = try_different_port(p, fd_listen);
        	}

        	if (rv == -1) {
        		close(fd_listen);
        		dno_print_error("bind");
				continue;
        	}
        }

        break;																				/* Break if the bind was successful. */
    }

    if (p == NULL)  {
        dno_print("create_info_connection: failed to bind\n");
        exit(EXIT_FAILURE);
    }

    get_ipstr_and_port_from_addrinfo(p, NULL, &port_int);									/* Get the actual port that the socket was bound to, so it can be sent to the router that will try to connect to this router. */
    port = port_int;

    freeaddrinfo(servinfo);

    rv = listen(fd_listen, BACKLOG);
    if (rv == -1) {
        dno_print_error("setup_sock_server_socket: listen");
        exit(EXIT_FAILURE);
    }

    port_nbo = htons(port);																	/* Send listening port on fd. */
	rv = send(fd, &port_nbo, sizeof(port_nbo), 0);
	if (rv == -1) {
		dno_print_error("create_info_connection: send");
		exit(EXIT_FAILURE);
	}

	sin_size 	= sizeof their_addr;														/* Call accept and wait until the other router has connected. */
	fd_info 	= accept(fd_listen, (struct sockaddr *) &their_addr, &sin_size);
	if (fd_info == -1) {
		dno_print_error("create_info_connection: accept");
		exit(EXIT_FAILURE);
	}

	close(fd_listen);																		/* Close listening_port because it is no longer needed. */

	return fd_info;																			/* Return the file descriptor of the established TCP connection. */
}

int connect_info_connection(int fd, char* ip_string) {
	int 				rv;
	int 				fd_info;
	struct addrinfo 	hints, *res;
	uint16_t			port;
	uint16_t			port_nbo;
	char 				portstring[5];

	rv = recv(fd, &port_nbo, sizeof(port_nbo), 0);												/* Receive the port that the other router is listening on for a new connection. */
	if (rv == -1) {
		dno_print_error("connect_info_connection: recv");
		exit(EXIT_FAILURE);
	}

	port = ntohs(port_nbo);

	sprintf(portstring, "%d", port);

	memset(&hints, 0, sizeof hints);
	hints.ai_family 	= AF_UNSPEC;
	hints.ai_socktype 	= SOCK_STREAM;

	getaddrinfo(ip_string, portstring, &hints, &res);

	fd_info = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (fd_info == -1) {
		dno_print_error("connect_info_connection: socket");
		close(fd_info);
		exit(EXIT_FAILURE);
	}

	rv = connect(fd_info, res->ai_addr, res->ai_addrlen);
	if (rv == -1) {
		dno_print_error("connect_info_connection: connect: to %s", ip_string);
		close(fd_info);
		exit(EXIT_FAILURE);
	}

	freeaddrinfo(res);

	return fd_info;
}

uint32_t setup_parent_child_relation(char* ipstr, uint16_t port, int fd, int fd_info, int area_id) {
	uint32_t 	has_parent;
	uint32_t 	has_parent_nbo;
	uint32_t 	has_parent_neighbor;
	int 		rv;

	write_lock_termination();

	if (parent == NULL) {
		has_parent = 0;
	} else {
		has_parent = 1;
	}

	has_parent_nbo = htonl(has_parent);
	rv = send(fd, &has_parent_nbo, sizeof(uint32_t), 0);
	if (rv == -1) {
		dno_print_error("setup_parent_child_relation: send: send has_parent.");
		exit(EXIT_FAILURE);
	}

	rv = recv(fd, &has_parent_neighbor, sizeof(uint32_t), 0);
	if (rv == -1) {
		dno_print_error("setup_parent_child_relation: recv: receive has_parent.");
		exit(EXIT_FAILURE);
	}
	has_parent_neighbor = ntohl(has_parent_neighbor);

	if (has_parent && !has_parent_neighbor) {
		uint32_t router_id = get_router_id(ipstr);
		add_child(router_id);
	} else if (!has_parent && has_parent_neighbor) {
		parent = initialize_connection(ipstr, port, fd, fd_info, area_id, has_parent_neighbor);
	}

	unlock_termination();

	return has_parent_neighbor;
}

void establish_connections() {
	struct area* 			area;
	struct router*			router;
	int 					exists;
	struct connection_args 	conn_args;

	conn_args.gxp_id = htonl(get_gxp_id());

	/* For each router in each area struct that has the connect flag set:
	 * Check if it has not been added by the accept thread.
	 * If it is, move on to next router.
	 * If it is not, try to make a connection and a new connection structure and add it to the list of connections.
	 * If the connection fails do not add it and do not try to connect to it again.
	 */
	read_lock_areas();
	DL_FOREACH(areas, area) {
		conn_args.area_id = area->area_id;

		DL_FOREACH(area->routers, router) {
			if (router->connect == 0) {																/* Only connect if this router was selected. */
				continue;
			}

			lock_connecting_to();																	/* Lock access to connection table and connecting_to information. */
			read_lock_connections();																/* The order is very important to prevent the possibility of deadlock with the serve_connection routine. */

			exists = is_in_table(connections, router->ip_string, SOCK_SVR_PORT);					/* Currently the only possible port is SOCK_SVR_PORT */
			if (!exists) {																			/* If a connection to this router does not yet exist, set connection_to information for the serve_connection routine. */
				strncpy(connecting_to_ip, router->ip_string, INET6_ADDRSTRLEN);
				connecting_to_port = SOCK_SVR_PORT;
			}

			unlock_connections();																	/* Unlock access to connection table and connecting_to information */
			unlock_connecting_to();

			if (!exists) {																			/* If a connection does not yet exist, connect to the router. */
				int 		rv;
				int			sockfd;
				uint16_t 	reply;

				sockfd = connect_to_router(router, REQUEST_CONN);
				if (sockfd < 0) {
					reset_connecting_to();
					continue;
				}

				rv = send(sockfd, &conn_args, sizeof(struct connection_args), 0);					/* Send connection arguments. */
				if (rv == -1) {
					dno_print_error("establish connections: send: send connection arguments.");
					close(sockfd);
					reset_connecting_to();
					continue;
				}

				rv = recv(sockfd, &reply, sizeof(reply), 0);										/* Check the reply of the server to see if the connection was successful. */
				if (rv == -1) {
					dno_print_error("establish connections: recv");
					close(sockfd);
					reset_connecting_to();
					continue;
				}
				reply = ntohs(reply);

				if (reply == CONN_SUCCESS) {														/* If successful. */
					int fd_info = create_info_connection(sockfd);

					uint32_t has_parent = setup_parent_child_relation(router->ip_string, SOCK_SVR_PORT, sockfd, fd_info, area->area_id);

					write_lock_connections();														/* Get write access to connection table */

					add_connection(router->ip_string, SOCK_SVR_PORT, sockfd, fd_info, area->area_id, has_parent);	/* Add connection. */

					unlock_connections();															/* Unlock access to connection table */
				} else {
					close(sockfd);																	/* If the connection was unsuccessful, close the socket. */
				}
			}

			reset_connecting_to();
		}
	}
	unlock_areas();
}

void *serve_connection(void *arg) {
	int 					rv;
	int 					sockfd;
	int 					exists = 0;
	struct arg_holder* 		arguments;
	struct sockaddr_storage their_addr;
	struct connection_args 	conn_args;
	uint32_t				their_gxp_id;
	uint16_t 				their_port;
	char 					ipstr[INET6_ADDRSTRLEN];
	uint16_t 				reply_nbo;
	int						fd_info;

	arguments 	= (struct arg_holder*) arg;
	sockfd 		= arguments->fd;
	their_addr 	= arguments->their_addr;
	free(arguments);

	get_ipstr_and_port_from_sockaddr_storage(&(their_addr), ipstr, NULL);

	rv = recv(sockfd, &conn_args, sizeof(struct connection_args), 0);								/* Receive the connection arguments. */
	if (rv == -1) {
		dno_print_error("serve_connections: recv");
		close(sockfd);
		pthread_exit(NULL);
	}

	their_gxp_id 	= ntohl(conn_args.gxp_id);
	their_port 		= SOCK_SVR_PORT;

    lock_connecting_to();																			/* Lock connecting_to. */

    if (strcmp(connecting_to_ip, ipstr) == 0 && connecting_to_port == their_port && their_gxp_id > get_gxp_id()) {	/* If the local router is trying to connect to this router abort if their gxp_id is higher. */
		reply_nbo = htons(CONN_ABORT);
    	rv = send(sockfd, &reply_nbo, sizeof(reply_nbo), 0);
		if (rv == -1) {
			dno_print_error("serve_connection: send");
		}
		close(sockfd);
	    unlock_connecting_to();
		pthread_exit(NULL);
    } else  {																						/* Else: */
    	write_lock_connections(); 																	/* Get write access to connection table. */
    }

    unlock_connecting_to();																			/* Unlock connecting_to. */

	/* Check if there is already a connection to this endpoint in the connection table
	 * If this endpoint is already in the connection table do not add it and tell the client,
	 * else add it to the connections table and tell the client.
	 */
    exists = is_in_table(connections, ipstr, their_port);

	if(exists) {																					/* If the connection already exists in the connection table. */
		reply_nbo = htons(CONN_ABORT);																/* Tell the other end to abort this connection setup. */
		rv = send(sockfd, &reply_nbo, sizeof(reply_nbo), 0);
		if (rv == -1) {
			dno_print_error("serve_connection: send: IS_IN_LIST");
			close(sockfd);
			unlock_connections();
			pthread_exit(NULL);
		}
		close(sockfd);
	} else {																						/* Else: */
		reply_nbo = htons(CONN_SUCCESS);
		rv = send(sockfd, &reply_nbo, sizeof(reply_nbo), 0);										/* Tell it the connection was set up successfully. */
		if (rv == -1) {
			dno_print_error("serve_connection: send: CONN_SUCCESS");
			close(sockfd);
			unlock_connections();
			pthread_exit(NULL);
		}

		fd_info = connect_info_connection(sockfd, ipstr);

		uint32_t has_parent = setup_parent_child_relation(ipstr, their_port, sockfd, fd_info, conn_args.area_id);

		add_connection(ipstr, their_port, sockfd, fd_info, conn_args.area_id, has_parent);			/* Add connection to the connection table. */
	}

	unlock_connections();																			/* Release write access to connection table. */

	pthread_exit(NULL);
}

void switch_on_indices(int min, int max, int* selected_node_indices, int d) {
	int i;
	int random;
	int range 						= (max-min)+1;
	int nr_of_selected_indices 		= 0;
	int nr_of_prev_selected_indices = 0;

	for (i = min; i <= max; i++) {
		if (selected_node_indices[i-1] == 1) {
			nr_of_prev_selected_indices++;
		}
	}

	while (nr_of_selected_indices < d && ((nr_of_prev_selected_indices + nr_of_selected_indices) < range)) {
		random = getrandomint(min, max);
		if (selected_node_indices[random-1] != 1) {
			selected_node_indices[random-1] = 1;
			nr_of_selected_indices++;
		}
	}
}

/* See section 6.1 of the paper High Performance Wide-area Overlay using Deadlock-free Routing by Ken Hironaka, Hideo Saito and Kenjiro Taura
 * for an explanation of the selection procedure below.
 *
 * The ordered list of routers is divided into consecutive ranges. The ranges get larger with each selection step.
 * From each range, d routers are uniform randomly selected.
 * The ranges increase in size each time i is incremented, up to a maximum of k = log(n) / log(d).
 * This is what causes many nearby nodes and fewer far away nodes to be selected.
 *
 * For d == 2, the ranges look as follows:
 * 		k == 0: [2,2]
 * 		k == 1: [3,4]
 * 		k == 2: [5,8]
 * 		k == 3: [9,16]
 * 		k == 4: [17,32]
 * 		k == 5: ...
 */
void select_connections(double d) {
	struct area* area;

	write_lock_areas();
	DL_FOREACH(areas, area) {
		int 			i;
		struct router* 	router;
		int 			router_count;
		double 			k;

		router_count 	= count_routers(area->routers);
		k 				= logbase((double) router_count, d);

		int selected_router_indices[router_count];
		memset(selected_router_indices, 0, sizeof(int) * router_count);

		for(i = 0; i < d; i++) {																		/* Select the d closest routers. */
			selected_router_indices[i] = 1;
		}

		setrandseed();																					/* Set the seed for getrandomint(). */

		for (i = 0; i < (int)k; i++) {
			int min = (int) (pow(d, i) + 1);
			int max = (int) pow(d, (i+1));
			switch_on_indices(min, max, selected_router_indices, (int) d);								/* Uniform randomly select d routers from the current [min,max] range. */
		}

		if (i < k) {																					/* Because in the for-loop above k is cast to an int, it is possible that there was another range to select from, if k is bigger than (int) k. */
			int min = (int) (pow(d, i) + 1);															/* Take the lower limit of the last range to be the lower limit for this range. */
			int max = router_count;																		/* Take n as the upper limit of the range. */
			switch_on_indices(min, max, selected_router_indices, (int) d);								/* Uniform randomly select d routers from the current [min,max] range. */
		}

		router = area->routers;
		for(i = 0; i < router_count; i++) {																/* Set the connect flags for the selected routers. */
			if (selected_router_indices[i] == 1 && router != area->this_router) {						/* Do not select if the router is the local router. */
				router->connect = 1;
			}

			router = router->next;
		}
	}
	unlock_areas();
}

struct connection* initialize_connection(char* ipstr, uint16_t port, int fd, int fd_info, int area_id, int has_parent) {
	struct connection* new_connection = malloc(sizeof(struct connection));

	strncpy(new_connection->neighbor_ip_string, ipstr, sizeof new_connection->neighbor_ip_string);
	new_connection->key 					= get_router_id(ipstr);
	new_connection->neighbor_id				= get_router_id(ipstr);
	new_connection->neighbor_port 			= port;
	new_connection->fd 						= fd;
	new_connection->fd_info					= fd_info;
	new_connection->area_id 				= area_id;
	new_connection->cost					= DEFAULT_COST;
	new_connection->state					= STATE_EXSTART;

	new_connection->dd_done					= 0;

	new_connection->database_summary_list 	= NULL;
	new_connection->last_dd					= 0;

	new_connection->link_state_request_list = NULL;
	new_connection->lsrq_list_pos			= NULL;

	new_connection->disconnected			= 0;

	new_connection->buffer					= NULL;									/* Needs to be set to NULL so that reset_packet_fields() can check if it points to allocated memory or not. */
	reset_packet_fields(new_connection);

	new_connection->has_parent				= has_parent;

	return new_connection;
}

void close_connections() {
	struct connection *connection, *tmp;

	write_lock_connections();

	HASH_ITER(hh, connections, connection, tmp) {
		HASH_DEL(connections, connection);
		close(connection->fd);
		close(connection->fd_info);

		free_lsa_headers(connection->database_summary_list); 						/* Free the database summary list. */
		free_lsa_keys(connection->link_state_request_list); 						/* Free LSR list. */

		if (connection->buffer != NULL) {
			free(connection->buffer);
		}

		free(connection);
	}

	unlock_connections();
}

int connection_sort(struct connection* a, struct connection* b) {
	char* astr = a->neighbor_ip_string;
	char* bstr = b->neighbor_ip_string;
	int n;

	n = strlen(astr) < strlen(bstr) ? strlen(bstr) : strlen(astr);

    return strncmp(astr, bstr, strlen(astr));
}

void print_connections()  {
    struct connection* connection;

    write_lock_connections();

    HASH_SORT(connections, connection_sort);

    dno_printf(0, "========== Printing connections ==========");

    dno_printf(0, "Nr. of connections: %d\n", HASH_COUNT(connections));
    dno_printf(0, "Connections:\n");

    for(connection = connections; connection != NULL; connection = connection->hh.next) {
    	char state[15];

    	get_state_string(state, connection->state, 15);

    	dno_printf(0, "\tTo:%15s\tarea_id:%2d\tcost:%2d\tstate:%9s\treadable_fd: %d\treadable_fd_info: %d\n", connection->neighbor_ip_string, connection->area_id, connection->cost, state, is_readable(connection->fd), is_readable(connection->fd_info));
    }

    dno_printf(1, "==========================================");

    unlock_connections();
}

void print_lsr(struct connection* connection) {
	struct lsa_key_element* key;
	int lsa_nr = 1;

	if (connection->link_state_request_list == NULL) {
		dno_printf(0, "\t\tEmpty link-state request list.\n");
	}

	DL_FOREACH(connection->link_state_request_list, key) {
		char adv_router[INET6_ADDRSTRLEN];
		char link_state_id[INET6_ADDRSTRLEN];

		inet_ntop(AF_INET, &(key->key.advertising_router), adv_router, INET6_ADDRSTRLEN);
		inet_ntop(AF_INET, &(key->key.link_state_id), link_state_id, INET6_ADDRSTRLEN);

		dno_printf(0, "\t\tlsa%3d:\tadv. router:%15s\tlink-state id:%15s\tlink-state type: %1d\n", lsa_nr, adv_router, link_state_id, key->key.link_state_type);
		lsa_nr++;
	}
}

void print_lsrs() {
	struct connection* connection;

	write_lock_connections();

	HASH_SORT(connections, connection_sort);

	dno_printf(0, "========== Printing link-state request lists ==========");

	dno_printf(0, "Nr. of connections: %d\n", HASH_COUNT(connections));
	dno_printf(0, "Connections:\n");

	for(connection = connections; connection != NULL; connection = connection->hh.next) {
    	char state[15];
    	get_state_string(state, connection->state, 15);

    	dno_printf(0, "\tTo:%15s\tarea_id:%2d\tcost:%2d\tstate:%15s\n", connection->neighbor_ip_string, connection->area_id, connection->cost, state);
		dno_printf(0, "\t\tdd_done: %1d\n", connection->dd_done);

		print_lsr(connection);
	}

	dno_printf(0, "=======================================================");

	unlock_connections();
}

void read_lock_connections() {
    if (pthread_rwlock_rdlock(&connections_lock) != 0) {
    	dno_print("read_lock_connections: Can't acquire readlock.\n");
    	exit(EXIT_FAILURE);
    }
}

void write_lock_connections() {
    if (pthread_rwlock_wrlock(&connections_lock) != 0) {
    	dno_print("write_lock_connections: Can't acquire writelock.\n");
    	exit(EXIT_FAILURE);
    }
}

void unlock_connections() {
	if(pthread_rwlock_unlock(&connections_lock) != 0) {
		dno_print("Error unlocking connections_lock.\n");
		exit(EXIT_FAILURE);
	}
}

void lock_connecting_to() {
	if (pthread_mutex_lock(&connecting_to_mutex) != 0) {
		dno_print("lock_connecting_to: Can't acquire lock to connecting_to_mutex.\n");
		exit(EXIT_FAILURE);
	}
}

void unlock_connecting_to() {
	if(pthread_mutex_unlock(&connecting_to_mutex) != 0) {
		dno_print("Error unlocking connecting_to_mutex.\n");
		exit(EXIT_FAILURE);
	}
}

