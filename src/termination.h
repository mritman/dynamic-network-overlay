/*
 * termination.h
 *
 *  Created on: Mar 1, 2011
 *      Author: mritman
 */

#ifndef TERMINATION_H_
#define TERMINATION_H_

#include "routingio.h"
#include "connections.h"

#define write_lock_termination()			write_lock_termination_priv(__FILE__, __LINE__, __FUNCTION__)

//extern int idle;
extern int terminate;
extern int is_free;
extern int inactive;
extern int num_unack_msgs;
extern int offer_count;
extern int dc;
extern pthread_rwlock_t termination_lock;

extern struct connection* parent;
extern struct child* 	children;
extern uint32_t root;

extern struct timeval term_start;
extern struct timeval sync_end;

extern int first_termination;
extern int times_terminated;

struct child {
	uint32_t 	router_id;
	int 		inactive;

	struct child* next;
	struct child* prev;
};

struct dc_element {
	uint32_t router_id;

	struct dc_element* next;
	struct dc_element* prev;
};

void add_child(uint32_t router_id);
void remove_child(uint32_t router_id);
void free_children();
void add_dc_neighbor(uint32_t dc_router_id);
void termination_init();
int  is_inactive();
int is_disconnecting();
void detect_termination();
void start_termination();
void increment_unack_msgs();
void decrement_unack_msgs();
void set_ackreq_bit_locked(char* buffer);
void set_ackreq_bit_routinginfo(struct connection* connection, char* buffer);
void ackreq_received_routingio(struct routing_connection* r_connection, int piggyback);
void ack_received_routingio(struct routing_connection* r_connection);
void received_primary_msg_routinginfo(struct connection* connection, unsigned char flags);
void received_primary_msg_routingio(struct routing_connection* connection, unsigned char term_turn);
void insert_term_packet(struct routing_connection* r_connection, uint8_t type);
int  is_termination_packet(uint8_t type);
struct connection*  process_termination_packet(struct termination_packet* term_packet, struct connection* connection);
void send_parent_offers_unlocked();
void select_parents(struct area* areas);
void print_parent_and_children();
void print_termination_status();
void read_lock_termination();
void write_lock_termination_priv(const char *file, int line, const char *function);
void unlock_termination();
void free_dc_list();

#endif /* TERMINATION_H_ */
