/*
 * packets.c
 *
 *  Created on: Dec 28, 2010
 *      Author: mritman
 */

#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>

#include "packets.h"
#include "utlist.h"
#include "linkstatedatabase.h"
#include "utils.h"
#include "routinginfo.h"
#include "termination.h"

unsigned char termination_turn = 1;

int calculate_nr_of_lsa_headers(int packet_length);
int calculate_nr_of_keys(int packet_length);
int calculate_nr_of_lsa_links(int lsa_packet_length);

int pack_int16_t(uint16_t int16, char* buffer, int offset) {
	int16 = htons(int16);
	memcpy(buffer+offset, &int16, sizeof(uint16_t));
	offset += sizeof(uint16_t);

	return offset;
}

uint16_t unpack_int16_t(char* buffer, int* offset) {
	uint16_t int16;

	memcpy(&int16, buffer+*offset, sizeof(uint16_t));
	*offset += sizeof(uint16_t);
	int16 = ntohs(int16);

	return int16;
}

int pack_int32_t(uint32_t int32, char* buffer, int offset) {
	int32 = htonl(int32);
	memcpy(buffer+offset, &int32, sizeof(uint32_t));
	offset += sizeof(uint32_t);

	return offset;
}

uint32_t unpack_int32_t(char* buffer, int* offset) {
	uint32_t int32;

	memcpy(&int32, buffer+*offset, sizeof(uint32_t));
	*offset += sizeof(uint32_t);
	int32 = ntohl(int32);

	return int32;
}

int pack_common_header(struct common_header* header, char* buffer, int offset) {
	unsigned char flags;

	memcpy(buffer+offset, &(header->type), sizeof(uint8_t));
	offset += sizeof(uint8_t);

	offset = pack_int16_t(header->length, buffer, offset);
	offset = pack_int32_t(header->router_id, buffer, offset);
	offset = pack_int32_t(header->area_id, buffer, offset);

	CLEAR_BITS(flags);
	if (termination_turn) SET_TERM_TURN_UP(flags);

	memcpy(buffer+offset, &flags, sizeof(unsigned char));
	offset += sizeof(unsigned char);

	return offset;
}

int unpack_common_header(struct common_header* header, char* buffer, int offset) {
	memcpy(&(header->type), buffer+offset, sizeof(uint8_t));
	offset += sizeof(uint8_t);

	header->length 		= unpack_int16_t(buffer, &offset);
	header->router_id 	= unpack_int32_t(buffer, &offset);
	header->area_id		= unpack_int32_t(buffer, &offset);
	memcpy(&(header->flags), buffer+offset, sizeof(unsigned char));
	offset += sizeof(unsigned char);

	return offset;
}

int pack_dd_header(struct dd_header* dd_header, char* buffer, int offset) {
	memcpy(buffer+offset, &(dd_header->flags), sizeof(unsigned char));
	offset += sizeof(unsigned char);

	return offset;
}

int unpack_dd_header(struct dd_header* dd_header, char* buffer, int offset) {
	memcpy(&(dd_header->flags), buffer+offset, sizeof(unsigned char));
	offset += sizeof(unsigned char);

	return offset;
}

int pack_lsdb_entry_key(lsdb_entry_key* key, char* buffer, int offset) {
	offset = pack_int32_t(key->link_state_type, buffer, offset);
	offset = pack_int32_t(key->link_state_id, buffer, offset);
	offset = pack_int32_t(key->advertising_router, buffer, offset);

	return offset;
}

int unpack_lsdb_entry_key(lsdb_entry_key* key, char* buffer, int offset) {
	key->link_state_type 	= unpack_int32_t(buffer, &offset);
	key->link_state_id		= unpack_int32_t(buffer, &offset);
	key->advertising_router	= unpack_int32_t(buffer, &offset);

	return offset;
}

int pack_lsa_keys(struct lsa_key_element* lsa_keys, char* buffer, int offset) {
	struct lsa_key_element* lsa_key_elt;

	DL_FOREACH(lsa_keys, lsa_key_elt) {
		offset = pack_lsdb_entry_key(&(lsa_key_elt->key), buffer, offset);
	}

	return offset;
}

struct lsa_key_element* unpack_lsa_keys(int nr_of_keys, char* buffer, int* offset) {
	int i;
	struct lsa_key_element* lsa_keys = NULL;
	struct lsa_key_element* new_key_element;

	for(i = 0; i < nr_of_keys; i++) {
		new_key_element = (struct lsa_key_element*) malloc(sizeof(struct lsa_key_element));

		*offset = unpack_lsdb_entry_key(&(new_key_element->key), buffer, *offset);

		DL_APPEND(lsa_keys, new_key_element);
	}

	return lsa_keys;
}

int pack_lsa_header(struct lsa_header* lsa_header, char* buffer, int offset) {
	offset = pack_int16_t(lsa_header->age, buffer, offset);
	offset = pack_int32_t(lsa_header->sequence_number, buffer, offset);
	offset = pack_int16_t(lsa_header->length, buffer, offset);
	offset = pack_int16_t(lsa_header->number_of_links, buffer, offset);

	return offset;
}

int unpack_lsa_header(struct lsa_header* lsa_header, char* buffer, int offset) {
	lsa_header->age 			= unpack_int16_t(buffer, &offset);
	lsa_header->sequence_number = unpack_int32_t(buffer, &offset);
	lsa_header->length			= unpack_int16_t(buffer, &offset);
	lsa_header->number_of_links = unpack_int16_t(buffer, &offset);

	return offset;
}

int pack_lsa_headers(struct lsa_header_element* lsa_headers, char* buffer, int offset) {
	struct lsa_header_element* lsa_header_elt;

	DL_FOREACH(lsa_headers, lsa_header_elt) {
		offset = pack_lsdb_entry_key(&(lsa_header_elt->key), buffer, offset);
		offset = pack_lsa_header(&(lsa_header_elt->header), buffer, offset);
	}

	return offset;
}

struct lsa_header_element* unpack_lsa_headers(int nr_of_headers, char* buffer, int* offset) {
	int i;
	struct lsa_header_element* lsa_headers = NULL;
	struct lsa_header_element* new_header_elt;

	for(i = 0; i < nr_of_headers; i++) {
		new_header_elt = (struct lsa_header_element*) malloc(sizeof(struct lsa_header_element));

		*offset = unpack_lsdb_entry_key(&(new_header_elt->key), buffer, *offset);
		*offset = unpack_lsa_header(&(new_header_elt->header), buffer, *offset);

		DL_APPEND(lsa_headers, new_header_elt);
	}

	return lsa_headers;
}

int pack_lsa_type1_link(struct lsa_type1_link* link, char* buffer, int offset) {
	offset = pack_int32_t(link->link_id, buffer, offset);
	offset = pack_int32_t(link->metric, buffer, offset);

	return offset;
}

int unpack_lsa_type1_link(struct lsa_type1_link* link, char* buffer, int offset) {
	link->link_id 	= unpack_int32_t(buffer, &offset);
	link->metric	= unpack_int32_t(buffer, &offset);

	return offset;
}

int pack_lsa_type1_links(struct lsa_type1_link* links, char* buffer, int offset) {
	struct lsa_type1_link* link;

	DL_FOREACH(links, link) {
		offset = pack_lsa_type1_link(link, buffer, offset);
	}

	return offset;
}

struct lsa_type1_link* unpack_lsa_type1_links(int nr_of_links, char* buffer, int* offset) {
	struct lsa_type1_link* lsa_links = NULL;
	struct lsa_type1_link* new_link;
	int i;

	for (i = 0; i < nr_of_links; i++) {
		new_link = (struct lsa_type1_link*) malloc(sizeof(struct lsa_type1_link));

		*offset = unpack_lsa_type1_link(new_link, buffer, *offset);

		DL_APPEND(lsa_links, new_link);
	}

	return lsa_links;
}

int pack_lsas_type1(struct lsa_type1* lsas_type1, char* buffer, int offset) {
	struct lsa_type1* lsa_type1;

	DL_FOREACH(lsas_type1, lsa_type1) {
		offset = pack_lsdb_entry_key(&(lsa_type1->key), buffer, offset);
		offset = pack_lsa_header(&(lsa_type1->header), buffer, offset);
		offset = pack_lsa_type1_links(lsa_type1->links, buffer, offset);
	}

	return offset;
}

struct lsa_type1* unpack_lsas_type1(int nr_of_lsas, char* buffer, int* offset) {
	struct lsa_type1* lsas = NULL;
	struct lsa_type1* new_lsa;
	int i;

	for(i = 0; i < nr_of_lsas; i++) {
		int nr_of_links;

		new_lsa = (struct lsa_type1*) malloc(sizeof(struct lsa_type1));

		*offset = unpack_lsdb_entry_key(&(new_lsa->key), buffer, *offset);
		*offset = unpack_lsa_header(&(new_lsa->header), buffer, *offset);

		nr_of_links = calculate_nr_of_lsa_links(new_lsa->header.length);

		new_lsa->links = unpack_lsa_type1_links(nr_of_links, buffer, offset);

		DL_APPEND(lsas, new_lsa);
	}

	return lsas;
}

char* pack_dd_packet(struct dd_packet* dd_packet) {
	char* buffer = (char*) malloc(dd_packet->common_header.length);
	int offset = 0;

	offset = pack_common_header(&(dd_packet->common_header), buffer, offset);
	offset = pack_dd_header(&(dd_packet->dd_header), buffer, offset);
	offset = pack_lsa_headers(dd_packet->lsa_headers, buffer, offset);

	return buffer;
}

struct dd_packet* unpack_dd_packet(char* buffer) {
	struct dd_packet* 	dd_packet = (struct dd_packet*) malloc(sizeof(struct dd_packet));
	int 				nr_of_headers;
	int					offset = 0;

	offset 						= unpack_common_header(&(dd_packet->common_header), buffer, 0);
	offset 						= unpack_dd_header(&(dd_packet->dd_header), buffer, offset);
	nr_of_headers 				= calculate_nr_of_lsa_headers(dd_packet->common_header.length);
	dd_packet->lsa_headers 		= unpack_lsa_headers(nr_of_headers, buffer, &offset);

	return dd_packet;
}

char* pack_lsr_packet(struct lsr_packet* lsr_packet) {
	char* buffer = (char*) malloc(lsr_packet->common_header.length);
	int offset = 0;

	offset = pack_common_header(&(lsr_packet->common_header), buffer, 0);
	offset = pack_lsa_keys(lsr_packet->lsa_keys, buffer, offset);

	return buffer;
}

struct lsr_packet* unpack_lsr_packet(char* buffer) {
	struct lsr_packet* 	lsr_packet = (struct lsr_packet*) malloc(sizeof(struct lsr_packet));
	int					nr_of_keys;
	int					offset = 0;

	offset 						= unpack_common_header(&(lsr_packet->common_header), buffer, 0);
	nr_of_keys 					= calculate_nr_of_keys(lsr_packet->common_header.length);
	lsr_packet->lsa_keys 		= unpack_lsa_keys(nr_of_keys, buffer, &offset);

	return lsr_packet;
}

char* pack_lsu_packet(struct lsu_packet* lsu_packet) {
	char* buffer = (char*) malloc(lsu_packet->common_header.length);
	int offset = 0;

	offset = pack_common_header(&(lsu_packet->common_header), buffer, 0);
	offset = pack_int32_t(lsu_packet->number_of_lsas, buffer, offset);
	offset = pack_lsas_type1(lsu_packet->lsas_type1, buffer, offset);

	return buffer;
}

struct lsu_packet* unpack_lsu_packet(char* buffer) {
	struct lsu_packet* lsu_packet = (struct lsu_packet*) malloc(sizeof(struct lsu_packet));
	int offset = 0;

	offset						= unpack_common_header(&(lsu_packet->common_header), buffer, 0);
	lsu_packet->number_of_lsas 	= unpack_int32_t(buffer, &offset);
	lsu_packet->lsas_type1 		= unpack_lsas_type1(lsu_packet->number_of_lsas, buffer, &offset);

	return lsu_packet;
}

char* pack_dc_packet(struct dc_packet* dc_packet) {
	char* buffer = (char*) malloc(dc_packet->common_header.length);

	pack_common_header(&(dc_packet->common_header), buffer, 0);

	return buffer;
}

struct dc_packet* unpack_dc_packet(char* buffer) {
	struct dc_packet* dc_packet = (struct dc_packet*) malloc(sizeof(struct dc_packet));

	unpack_common_header(&(dc_packet->common_header), buffer, 0);

	return dc_packet;
}

int calculate_nr_of_lsa_headers(int packet_length) {
	return (packet_length - COMMON_HEADER_SIZE - DD_HEADER_SIZE) / (LSDB_ENTRY_KEY_SIZE + LSA_HEADER_SIZE);
}

int calculate_nr_of_keys(int packet_length) {
	return (packet_length - COMMON_HEADER_SIZE) / LSDB_ENTRY_KEY_SIZE;
}

int calculate_nr_of_lsa_links(int lsa_packet_length) {
	return (lsa_packet_length - LSA_HEADER_SIZE - LSDB_ENTRY_KEY_SIZE) / LSA_TYPE1_LINK_SIZE;
}

void free_lsa_headers(struct lsa_header_element* lsa_headers) {
	struct lsa_header_element* elt, *tmp;

	DL_FOREACH_SAFE(lsa_headers, elt, tmp) {
		DL_DELETE(lsa_headers, elt);
		free(elt);
	}
}

void free_lsa_keys(struct lsa_key_element* lsa_keys) {
	struct lsa_key_element* elt, *tmp;

	DL_FOREACH_SAFE(lsa_keys, elt, tmp) {
		DL_DELETE(lsa_keys, elt);
		free(elt);
	}
}

void free_lsa_type1_links(struct lsa_type1_link* links) {
	struct lsa_type1_link* elt, *tmp;

	DL_FOREACH_SAFE(links, elt, tmp) {
		DL_DELETE(links, elt);
		free(elt);
	}
}

void free_lsa_type1(struct lsa_type1* lsa) {
	free_lsa_type1_links(lsa->links);
	free(lsa);
}

void free_lsas_type1(struct lsa_type1* lsas) {
	struct lsa_type1* elt, *tmp;

	DL_FOREACH_SAFE(lsas, elt, tmp) {
		DL_DELETE(lsas, elt);
		free_lsa_type1(elt);
	}
}

void free_dd_packet(struct dd_packet* dd_packet) {
	free_lsa_headers(dd_packet->lsa_headers);
	free(dd_packet);
}

void free_lsr_packet(struct lsr_packet* lsr_packet) {
	free_lsa_keys(lsr_packet->lsa_keys);
	free(lsr_packet);
}

void free_lsu_packet(struct lsu_packet* lsu_packet) {
	free_lsas_type1(lsu_packet->lsas_type1);
	free(lsu_packet);
}

struct lsa_type1* create_lsa_type1(lsdb_entry_key key) {
	struct lsa_type1* 		new_lsa = (struct lsa_type1*) malloc(sizeof(struct lsa_type1));						/* Create a new lsa type1 packet. */
	struct lsdb_entry*		lsdb_entry;
	struct lsa_type1_link*	current_link;

	lsdb_entry = find_lsdb_entry(key);																			/* Find the LSA in the link-state database. */

	if (lsdb_entry == NULL) {
		return NULL;																							/* The LSA might have expired in the time between sending the dd_packet and receiving the lsr_packet from the neighbor. */
	}

	write_lock_lsdb();																							/* The entry is part of the link-state database so acquire a lock. We need a write-lock because we are updating the age field. */

	age_lsdb_entry(lsdb_entry);

	new_lsa->key 	= lsdb_entry->key;
	new_lsa->header = lsdb_entry->header;
	new_lsa->links	= NULL;

	if (new_lsa->header.age < MAX_AGE) new_lsa->header.age += INF_TRANS_DELAY;									/* LSAs' age needs to be incremented when in transit. */
	if (new_lsa->header.age > MAX_AGE) new_lsa->header.age = MAX_AGE;

	current_link = lsdb_entry->links;

	while(current_link != NULL) {
		struct lsa_type1_link* link = (struct lsa_type1_link*) malloc(sizeof(struct lsa_type1_link));

		memcpy(link, current_link, sizeof(struct lsa_type1_link));

		DL_APPEND(new_lsa->links, link);

		current_link = current_link->next;
	}

	unlock_lsdb();																								/* Unlock the link-state database. */

	return new_lsa;
}

struct dd_packet* create_dd_packet(struct lsa_header_element** summary_list, uint32_t router_id, uint32_t area_id) {
	struct dd_packet* 			dd_packet 			= (struct dd_packet*) malloc(sizeof(struct dd_packet));
	struct lsa_header_element* 	summary_list_head	= *summary_list;
	struct lsa_header_element* 	current_header;

	dd_packet->common_header.type 		= DATABASE_DESCRIPTION;
	dd_packet->common_header.router_id 	= router_id;
	dd_packet->common_header.area_id 	= area_id;
	dd_packet->common_header.length 	= COMMON_HEADER_SIZE + DD_HEADER_SIZE;
	dd_packet->dd_header.flags			= CLEAR;
	dd_packet->lsa_headers				= NULL;

	while ((dd_packet->common_header.length + LSA_HEADER_SIZE) < MAX_PACKET_LENGTH && summary_list_head != NULL) {
		current_header = summary_list_head;
		DL_DELETE(summary_list_head, current_header);								/* Remove current_header from summary list. */
		DL_APPEND(dd_packet->lsa_headers, current_header);							/* Add it to the dd_packet->lsa_headers. */
		dd_packet->common_header.length += LSA_HEADER_SIZE + LSDB_ENTRY_KEY_SIZE;	/* Update the packet length. */
	}

	*summary_list = summary_list_head;												/* The caller needs to know the new head of the database summary list. */

	if (summary_list_head != NULL) {												/* If there are more headers to be sent, set the more flag. */
		SET_MORE(dd_packet->dd_header.flags);
	}

	return dd_packet;
}

struct lsr_packet* create_lsr_packet(struct lsa_key_element** request_list, uint32_t router_id, uint32_t area_id) {
	struct lsr_packet* 		lsr_packet	= (struct lsr_packet*) malloc(sizeof(struct lsr_packet));
	struct lsa_key_element* current_key = *request_list;

	lsr_packet->common_header.type		= LINK_STATE_REQUEST;
	lsr_packet->common_header.router_id = router_id;
	lsr_packet->common_header.area_id 	= area_id;
	lsr_packet->common_header.length	= COMMON_HEADER_SIZE;
	lsr_packet->lsa_keys 				= NULL;

	while((lsr_packet->common_header.length + LSDB_ENTRY_KEY_SIZE) < MAX_PACKET_LENGTH && current_key != NULL) {
		struct lsa_key_element* new_key_elt = (struct lsa_key_element*) malloc(sizeof(struct lsa_key_element));

		new_key_elt->key 	= current_key->key;
		new_key_elt->next 	= NULL;
		new_key_elt->prev	= NULL;

		DL_APPEND(lsr_packet->lsa_keys, new_key_elt);
		lsr_packet->common_header.length += LSDB_ENTRY_KEY_SIZE;

		current_key = current_key->next;
	}

	*request_list = current_key;

	return lsr_packet;
}

struct lsu_packet* create_lsu_packet(struct lsa_key_element** lsa_keys, uint32_t router_id, uint32_t area_id) {
	struct lsu_packet* 		lsu_packet 	= (struct lsu_packet*) malloc(sizeof(struct lsu_packet));
	struct lsa_key_element*	current_key = *lsa_keys;

	lsu_packet->common_header.type 		= LINK_STATE_UPDATE;
	lsu_packet->common_header.router_id	= router_id;
	lsu_packet->common_header.area_id	= area_id;
	lsu_packet->common_header.length	= COMMON_HEADER_SIZE;
	lsu_packet->number_of_lsas			= 0;
	lsu_packet->lsas_type1				= NULL;
	lsu_packet->common_header.length	+= sizeof(lsu_packet->number_of_lsas);					/* Add the size of the 'number_of_lsas' field to the packet length. */

	while(current_key != NULL) {
		struct lsa_type1* new_lsa = create_lsa_type1(current_key->key);

		current_key = current_key->next;

		if (new_lsa == NULL) continue;

		if (lsu_packet->common_header.length + new_lsa->header.length < MAX_PACKET_LENGTH) {
			DL_APPEND(lsu_packet->lsas_type1, new_lsa);											/* Add it to the lsu_packet. */
			lsu_packet->number_of_lsas++;														/* Increment number_of_lsas. */
			lsu_packet->common_header.length += new_lsa->header.length;							/* Increase length of packet by the size of the new lsa. */
		} else {
			free_lsa_type1(new_lsa);
			break;
		}
	}

	*lsa_keys = current_key;

	return lsu_packet;
}

struct dc_packet* create_dc_packet(uint32_t router_id, uint32_t area_id) {
	struct dc_packet* dc_packet	= (struct dc_packet*) malloc(sizeof(struct dc_packet));

	dc_packet->common_header.type 		= DISCONNECT;
	dc_packet->common_header.router_id 	= router_id;
	dc_packet->common_header.area_id 	= area_id;
	dc_packet->common_header.length 	= COMMON_HEADER_SIZE;

	return dc_packet;
}

#include <errno.h>
int read_bytes(int fd, char* buffer, int pos, int length) {
	int rv;

	errno = 0;
	rv = recv(fd, buffer + pos, length - pos, MSG_DONTWAIT);
	if (rv == -1) {
		switch(errno) {
		case EAGAIN:
			rv = 0;
			break;
		default:
			dno_print_error("read_bytes: recv:");
			exit(EXIT_FAILURE);
		}
	}

	return rv;

}

void read_header(struct connection* connection) {
	int pos = 0;

	memset(connection->header_buffer, 0, COMMON_HEADER_SIZE);

	while(pos != COMMON_HEADER_SIZE) {
		pos += read_bytes(connection->fd_info, connection->header_buffer, pos, COMMON_HEADER_SIZE);
	}
}

//TODO busy looping on the fd_info socket is done on the assumption that data is incoming.
//However it may be the case that select return a socket as readable even is the number of bytes that can be read is 0.
void read_packet(struct connection* connection) {
	if (!connection->in_progress) {   																		/* Start of a new packet. */
		read_header(connection);																			/* Read the header. */
		unpack_common_header(&(connection->header), connection->header_buffer, 0);							/* Unpack the common_header, ignore the return value. */

		connection->buffer = (char*) malloc(connection->header.length);										/* Allocate the buffer for the packet based on length field of the header. */

		memcpy(connection->buffer, connection->header_buffer, COMMON_HEADER_SIZE);							/* Copy the header to the beginning of the packet buffer. */
		connection->pos += COMMON_HEADER_SIZE;																/* Increment position by COMMON_HEADER_SIZE. */

		connection->in_progress = 1;
	}

	connection->pos += read_bytes(connection->fd_info, connection->buffer, connection->pos, connection->header.length);	/* Read the bytes of the rest of the packet currently available. */
	if (connection->pos == connection->header.length) {
		connection->packet_ready = 1;
	}
}

void reset_packet_fields(struct connection* connection) {
	if (connection == NULL) return;

	connection->in_progress 	= 0;
	connection->packet_ready	= 0;
	memset(&(connection->header), 0, sizeof(struct common_header));
	memset(connection->header_buffer, 0, COMMON_HEADER_SIZE);
	if(connection->buffer != NULL) {
		free(connection->buffer);
	}
	connection->buffer			= NULL;
	connection->pos				= 0;
}

void send_packet(struct connection* connection, char* buffer, int length) {
	int pos = 0;
	int rv;

	while(pos != length) {
		rv = send(connection->fd_info, buffer+pos, length-pos, 0);
		if (rv == -1) {
			dno_print_error("send_packet");
			exit(EXIT_FAILURE);
		}

		pos += rv;
	}
}

void print_dd_packet(struct dd_packet* dd_packet) {
	struct lsa_header_element* 	current_header;
	int							header_count = 1;
   	char router_id[INET6_ADDRSTRLEN];
    char area_id[INET6_ADDRSTRLEN];

	dno_printf(0, "============ Printing dd_packet ============");

    if(link_state_database == NULL) {
    	dno_printf(0, "%\tempty\n");
    	dno_printf(1, "============================================");
    	return;
    }

    current_header = dd_packet->lsa_headers;

	inet_ntop(AF_INET, &(dd_packet->common_header.router_id), router_id, INET6_ADDRSTRLEN);
	inet_ntop(AF_INET, &(dd_packet->common_header.area_id), area_id, INET6_ADDRSTRLEN);

    dno_printf(0, "dd_packet->common_header.type = %d\n", (int) dd_packet->common_header.type);
    dno_printf(0, "dd_packet->common_header.length = %d\n", (int) dd_packet->common_header.length);
    dno_printf(0, "dd_packet->common_header.router_id = %s\n", router_id);
    dno_printf(0, "dd_packet->common_header.area_id = %s\n", area_id);
    if(HAS_MORE(dd_packet->dd_header.flags)) {
    	dno_printf(0, "There are more LSA headers in the database summary list.\n");
    } else {
    	dno_printf(0, "There are no more LSA headers in the database summary list.\n");
    }

    while(current_header != NULL) {
    	char link_state_id[INET6_ADDRSTRLEN];
    	char advertising_router[INET6_ADDRSTRLEN];

    	inet_ntop(AF_INET, &(current_header->key.link_state_id), link_state_id, INET6_ADDRSTRLEN);
    	inet_ntop(AF_INET, &(current_header->key.advertising_router), advertising_router, INET6_ADDRSTRLEN);

    	dno_printf(0, "\t header%3d->key.link_state_type\t\t = %d\n", header_count, (int) current_header->key.link_state_type);
    	dno_printf(0, "\t header%3d->key.link_state_id\t\t = %s\n", header_count, link_state_id);
    	dno_printf(0, "\t header%3d->key.advertising_router\t = %s\n", header_count, advertising_router);

    	dno_printf(0, "\t header%3d->header.length\t\t = %d\n", header_count, (int) current_header->header.length);
    	dno_printf(0, "\t header%3d->header.sequence_number\t = %d\n", header_count, (int) current_header->header.sequence_number);
    	dno_printf(0, "\t header%3d->header.age\t\t\t = %d\n", header_count, (int) current_header->header.age);
    	dno_printf(0, "\t header%3d->header.number_of_links\t = %d\n", header_count, (int) current_header->header.number_of_links);

    	dno_printf(0, "\n");

    	current_header = current_header->next;
    	header_count++;
    }

    dno_printf(1, "============================================");
}

void print_lsr_packet(struct lsr_packet* lsr_packet) {
	struct lsa_key_element* 	current_key;
	int							key_count = 1;
   	char router_id[INET6_ADDRSTRLEN];
    char area_id[INET6_ADDRSTRLEN];

	dno_printf(0, "============ Printing lsr_packet ============");

    if(lsr_packet == NULL) {
    	dno_printf(0, "%\tempty\n");
    	dno_printf(1, "=============================================");
    	return;
    }

    current_key = lsr_packet->lsa_keys;

	inet_ntop(AF_INET, &(lsr_packet->common_header.router_id), router_id, INET6_ADDRSTRLEN);
	inet_ntop(AF_INET, &(lsr_packet->common_header.area_id), area_id, INET6_ADDRSTRLEN);

	dno_printf(0, "lsr_packet->common_header.type = %d\n", (int) lsr_packet->common_header.type);
	dno_printf(0, "lsr_packet->common_header.length = %d\n", (int) lsr_packet->common_header.length);
	dno_printf(0, "lsr_packet->common_header.router_id = %s\n", router_id);
	dno_printf(0, "lsr_packet->common_header.area_id = %s\n", area_id);

	while(current_key != NULL) {
    	char link_state_id[INET6_ADDRSTRLEN];
    	char advertising_router[INET6_ADDRSTRLEN];

    	inet_ntop(AF_INET, &(current_key->key.link_state_id), link_state_id, INET6_ADDRSTRLEN);
    	inet_ntop(AF_INET, &(current_key->key.advertising_router), advertising_router, INET6_ADDRSTRLEN);

    	dno_printf(0, "\t lsa_key%3d->link_state_type\t\t = %d\n", key_count, (int) current_key->key.link_state_type);
    	dno_printf(0, "\t lsa_key%3d->link_state_id\t\t = %s\n", key_count, link_state_id);
    	dno_printf(0, "\t lsa_key%3d->advertising_router\t\t = %s\n", key_count, advertising_router);

    	dno_printf(0, "\n");

    	current_key = current_key->next;
    	key_count++;
	}

	dno_printf(1, "============================================");
}

void print_lsu_packet(struct lsu_packet* lsu_packet) {
	struct lsa_type1* 	current_lsa;
	int					lsa_count = 1;
	char router_id[INET6_ADDRSTRLEN];
	char area_id[INET6_ADDRSTRLEN];

	dno_printf(0, "============ Printing lsu_packet ============");

	if(lsu_packet == NULL) {
		dno_printf(0, "%\tempty\n");
		dno_printf(1, "=============================================");
		return;
	}

	current_lsa = lsu_packet->lsas_type1;

	inet_ntop(AF_INET, &(lsu_packet->common_header.router_id), router_id, INET6_ADDRSTRLEN);
	inet_ntop(AF_INET, &(lsu_packet->common_header.area_id), area_id, INET6_ADDRSTRLEN);

	dno_printf(0, "lsu_packet->common_header.type = %d\n", (int) lsu_packet->common_header.type);
	dno_printf(0, "lsu_packet->common_header.length = %d\n", (int) lsu_packet->common_header.length);
	dno_printf(0, "lsu_packet->common_header.router_id = %s\n", router_id);
	dno_printf(0, "lsu_packet->common_header.area_id = %s\n", area_id);
	dno_printf(0, "lsu_packet->number_of_lsas = %d\n", lsu_packet->number_of_lsas);

	while(current_lsa != NULL) {
		char 	link_state_id[INET6_ADDRSTRLEN];
		char 	advertising_router[INET6_ADDRSTRLEN];
		int		link_count = 1;
		struct	lsa_type1_link* current_link;

		inet_ntop(AF_INET, &(current_lsa->key.link_state_id), link_state_id, INET6_ADDRSTRLEN);
		inet_ntop(AF_INET, &(current_lsa->key.advertising_router), advertising_router, INET6_ADDRSTRLEN);

		dno_printf(0, "\t lsa%3d->key.link_state_type\t = %d\n", lsa_count, (int) current_lsa->key.link_state_type);
		dno_printf(0, "\t lsa%3d->key.link_state_id\t = %s\n", lsa_count, link_state_id);
		dno_printf(0, "\t lsa%3d->key.advertising_router\t = %s\n", lsa_count, advertising_router);

		dno_printf(0, "\t lsa%3d->header.length\t\t = %d\n", lsa_count, (int) current_lsa->header.length);
		dno_printf(0, "\t lsa%3d->header.sequence_number\t = %d\n", lsa_count, (int) current_lsa->header.sequence_number);
		dno_printf(0, "\t lsa%3d->header.age\t\t = %d\n", lsa_count, (int) current_lsa->header.age);
		dno_printf(0, "\t lsa%3d->header.number_of_links\t = %d\n", lsa_count, (int) current_lsa->header.number_of_links);

		current_link = current_lsa->links;

		while(current_link != NULL) {
			char link_id[INET6_ADDRSTRLEN];

			inet_ntop(AF_INET, &(current_link->link_id), link_id, INET6_ADDRSTRLEN);

			dno_printf(0, "\t lsa%3d->link%3d->link_id\t = %s\n", lsa_count, link_count, link_id);
			dno_printf(0, "\t lsa%3d->link%3d->metric\t = %d\n", lsa_count, link_count, current_link->metric);

			current_link = current_link->next;
			link_count++;
		}

		dno_printf(0, "\n");

    	current_lsa = current_lsa->next;
    	lsa_count++;
	}

	dno_printf(1, "============================================");
}

/* Termination detection packet related functions. */

struct termination_packet* create_termination_packet(uint32_t router_id, uint32_t area_id, uint8_t type, uint32_t router_id_1, uint32_t router_id_2) {
	struct termination_packet* termination_packet = (struct termination_packet*) malloc(sizeof(struct termination_packet));

	termination_packet->common_header.router_id = router_id;
	termination_packet->common_header.area_id	= area_id;
	termination_packet->common_header.type 		= type;
	termination_packet->common_header.length	= COMMON_HEADER_SIZE;

	if (type == RESUME || type == ACKNOWLEDGE) {
		termination_packet->common_header.length += sizeof(uint32_t) * 2;
		termination_packet->router_id_1 = router_id_1;
		termination_packet->router_id_2 = router_id_2;
	}

	return termination_packet;
}

char* pack_termination_packet(struct termination_packet* termination_packet) {
	char* buffer = (char*) malloc(termination_packet->common_header.length);
	int offset = 0;

	offset = pack_common_header(&(termination_packet->common_header), buffer, offset);
	if (termination_packet->common_header.type == RESUME || termination_packet->common_header.type == ACKNOWLEDGE) {
		offset = pack_int32_t(termination_packet->router_id_1, buffer, offset);
		offset = pack_int32_t(termination_packet->router_id_2, buffer, offset);
	}

	return buffer;
}

struct termination_packet* unpack_termination_packet(char* buffer) {
	struct termination_packet* termination_packet = (struct termination_packet*) malloc(sizeof(struct termination_packet));
	int	offset = 0;

	offset = unpack_common_header(&(termination_packet->common_header), buffer, 0);
	if (termination_packet->common_header.type == RESUME || termination_packet->common_header.type == ACKNOWLEDGE) {
		termination_packet->router_id_1 = unpack_int32_t(buffer, &offset);
		termination_packet->router_id_2 = unpack_int32_t(buffer, &offset);
	}

	return termination_packet;
}
