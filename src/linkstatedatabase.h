/*
 * linkstatedatabase.h
 *
 *  Created on: Dec 28, 2010
 *      Author: mritman
 */

#include "uthash.h"
#include "utlist.h"
#include "packets.h"

extern struct lsdb_entry* 	link_state_database;
extern pthread_rwlock_t 	lsdb_lock;

struct lsdb_entry {
	lsdb_entry_key			key;				/* The hash key uniquely identifying this LSA. */

	struct lsa_header 		header;				/* The LSA header. */
	struct lsa_type1_link*	links;				/* Links of the LSA. */

	struct timeval			timestamp;			/* Used for aging the LSA. */

	UT_hash_handle 			hh;					/* Required for uthash. */
};

void 	lsdb_init();

struct 	lsdb_entry* new_lsdb_entry();

void 	add_lsdb_entry(struct lsdb_entry* new_entry);

struct 	lsdb_entry* find_lsdb_entry(lsdb_entry_key key);

int 	in_lsdb(uint32_t router_id);

void 	age_lsdb_entry(struct lsdb_entry* entry);

int 	is_newer(lsdb_entry_key key, struct lsa_header header);

int 	update_lsdb(struct lsa_type1* lsa);

void 	delete_lsdb_entry(struct lsdb_entry* entry);

void 	purge_expired_lsas();

void 	free_link_state_database();

void 	print_link_state_database(int full);

int 	database_sort(struct lsdb_entry* a, struct lsdb_entry* b);

struct 	lsa_header_element* get_database_summary_list();

void 	read_lock_lsdb();

void 	write_lock_lsdb();

void 	unlock_lsdb();
