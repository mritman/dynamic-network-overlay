/*
 * packettypes.h
 *
 *  Created on: Feb 7, 2011
 *      Author: mritman
 */

#ifndef PACKETTYPES_H_
#define PACKETTYPES_H_

#include <stdint.h>

#define MAX_PACKET_LENGTH			1024																/* Maximum packet size in bytes. */
#define COMMON_HEADER_SIZE			12																	/* Size in octets. */
#define COMMON_HEADER_FLAG_OFFSET	11																	/* Size in octets. */
#define DD_HEADER_SIZE				1																	/* Size in octets. */
#define LSDB_ENTRY_KEY_SIZE			12																	/* Size in octets. */
#define LSA_HEADER_SIZE				10																	/* Size in octets. */
#define LSA_TYPE1_LINK_SIZE			8																	/* Size in octets. */

/* OSPF packet types. */
#define DATABASE_DESCRIPTION 		0x02
#define LINK_STATE_REQUEST			0x03
#define LINK_STATE_UPDATE			0x04
//#define LINK_STATE_ACKNOWLEDGEMENT	0x05 TODO remove

/* DNO packet types. */
#define DISCONNECT					0x05

/* Termination detection packets types */
#define STOP						0x06
#define RESUME						0x07
#define TERMINATION					0x08
#define ACKNOWLEDGE					0x09
#define PARENT_OFFER				0x0A
#define PARENT_ACCEPT				0x0B
#define PARENT_REJECT				0x0C
#define ACKREQ						0x0D
#define ACK							0x0E

/* LSA packet types. */
#define ROUTER_LSA					0x01

/* Bit mask to clear the flags. */
#define CLEAR						0x00

/* Database description packet bit flags. */
#define MASTER						0x01
#define MORE						0x02
#define INITIAL		 				0x04

/* Macros to set the flags. */
#define CLEAR_BITS(x)				(x = CLEAR)
#define SET_MASTER(x)				(x |= MASTER)
#define SET_MORE(x)					(x |= MORE)
#define SET_INITIAL(x)				(x |= INITIAL)

/* Macros to evaluate the flags. */
#define IS_MASTER(x)				(x & MASTER)
#define HAS_MORE(x)					(x & MORE)
#define IS_INITIAL(x)				(x & INITIAL)

/* Common header packet bit flags. */
#define TERM_TURN_UP				0x01
#define ACKREQ_UP					0x02

/* Macros to set the flags. */
#define SET_TERM_TURN_UP(x)			(x |= TERM_TURN_UP)
#define SET_ACKREQ_UP(x)			(x |= ACKREQ_UP)

/* Macros to evaluate the flags. */
#define IS_TERM_TURN_UP(x)			(x & TERM_TURN_UP)
#define HAS_ACKREQ(x)				(x & ACKREQ_UP)

/* The common header used in all packet types. */
struct common_header {
	 uint8_t 		type;									/* One of the OSPF packet types. */
	 uint16_t 		length;									/* The length of the OSPF packet, in octets, including the header. */
	 uint32_t		router_id;								/* Originating router. */
	 uint32_t		area_id;								/* Originating area. */
	 unsigned char 	flags;									/* Either 0 or 1. Switches each time termination occurs to distinguish packets from before and after termination. */
};

/* The database description packet header. */
struct dd_header {
	unsigned char 	flags;								/* The I, M and MS bit flags. */
//	uint32_t 		sequence_number;					/* The database description packet sequence number. */
	//TODO might not be needed, if so remove the sequence number field
};

/* The key is logically a part of the lsa header but it is separated here so it is easier to use with uthash. */
typedef struct {
	uint32_t link_state_type;							/* One of the two LSA types applicable in the overlay. Router LSAs or Summary LSAs. */
	uint32_t link_state_id;								/* If type is Router LSA: the originating router's Router ID. If type is summary: the Router ID of the router being advertised. */
	uint32_t advertising_router;						/* The router_id of the router that originated the LSA. */
} lsdb_entry_key;

/* The link-state request packet consist of keys, each identifying a single LSA. The key is defined in linkstatedatabase.h. */
struct lsa_key_element {
	lsdb_entry_key key;

	struct lsa_key_element* prev;
	struct lsa_key_element* next;
};

/* The link-state advertisement header. */
struct lsa_header {
	uint16_t 	age;									/* Age of the LSA. */

//	uint8_t		type;									/* LSA Type. Router or Summary. */
//	uint32_t	link_state_id;
//	uint32_t	advertising_router;						/* The originating router of the LSA. */

	int32_t		sequence_number;						/* The sequence number of the LSA. */
	uint16_t	length;									/* The number of octets of the LSA. Including the header. */
	uint16_t	number_of_links;						/* The number of links the LSA contains. */
};

/* The link-state advertisement header wrapping the LSA header into a struct that can be used as an element in a list. */
struct lsa_header_element {
	lsdb_entry_key		key;
	struct lsa_header 	header;

	struct lsa_header_element* prev;
	struct lsa_header_element* next;
};

/* The data making up the type1 link-state advertisements. */
struct lsa_type1_link {
	uint32_t link_id;									/* Identifies the router to which this link connects. */
	uint32_t metric;									/* The cost associated with the link. */

	struct lsa_type1_link* prev;
	struct lsa_type1_link* next;
};

/* The type1 link-state advertisement. */
struct lsa_type1 {
	lsdb_entry_key				key;
	struct lsa_header		 	header;
	struct lsa_type1_link* 		links;

	struct lsa_type1* prev;
	struct lsa_type1* next;
};

/* The database description packet. */
struct dd_packet {
	struct common_header 		common_header;
	struct dd_header 			dd_header;
	struct lsa_header_element*	lsa_headers;
};

/* The link-state request packet. */
struct lsr_packet {
	struct common_header	common_header;
	struct lsa_key_element*	lsa_keys;
};

/* The link-state update packet. */
struct lsu_packet {
	struct common_header 	common_header;
	uint32_t				number_of_lsas;
	struct lsa_type1*		lsas_type1;

	//struct lsa_type2*		type2; TODO To be added later.
};

struct dc_packet {
	struct common_header common_header;
};

struct termination_packet {
	struct common_header common_header;
	uint32_t router_id_1;
	uint32_t router_id_2;
};

#endif /* PACKETTYPES_H_ */
