/*
 * routingtable.c
 *
 *  Created on: Aug 15, 2010
 *      Author: mritman
 */
#include <pthread.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netdb.h>

#include "routingtable.h"
#include "utils.h"
#include "graph.h"
#include "utlist.h"
#include "dno.h"

struct routing_table_entry* routing_table = NULL;
pthread_rwlock_t routing_table_lock;

void read_lock_table();
void write_lock_table();
void unlock_table();

void add_routing_entry(struct routing_table_entry* new_entry) {
    write_lock_table();
    HASH_ADD_INT(routing_table, destination, new_entry);
    unlock_table();
}

struct routing_table_entry* find_routing_entry(int destination) {
	struct routing_table_entry* result;

    read_lock_table();
    HASH_FIND_INT(routing_table, &destination, result);
    unlock_table();

    return result;
}

void delete_routing_entry(struct routing_table_entry* entry) {
    write_lock_table();
    HASH_DEL(routing_table, entry);
    unlock_table();

    free(entry);
}

uint32_t find_next_hop(uint32_t dest) {
	struct 	routing_table_entry* entry;
	int		next_hop;

    read_lock_table();
    HASH_FIND_INT(routing_table, &dest, entry);
    if (entry != NULL) {
    	next_hop = entry->next_hop;
    } else {
    	next_hop = 0;
    }
    unlock_table();

    return next_hop;
}

void free_routing_table() {
	struct routing_table_entry *current_entry, *tmp;

    write_lock_table();
    HASH_ITER(hh, routing_table, current_entry, tmp) {
		HASH_DEL(routing_table, current_entry);
		free(current_entry);
	}
    unlock_table();
}

uint32_t calculate_next_hop(struct node* nodes, struct node* node) {
	struct node* clone;

	clone = find_clone(nodes, node);

	if (clone != NULL) {
		if (clone->dist < node->dist) {												/* If the rank2 version of this node has a shorter path than the rank 1 version use that to calculate the next hop for. */
			node = clone;
		}
	}

	while(1) {
		if (node->previous == 0) {
			char ip_string[INET6_ADDRSTRLEN];
			convert_ip_ntop(node->router_id, ip_string);

			dno_print("Warning: unreachable node in network: %s", ip_string);
			break;
		}
		if (node->previous->router_id == router_id) {
			return node->router_id;
		} else {
			node = node->previous;
		}
	}

	return 0;
}

void calculate_routing_table(struct graph* graph) {
	struct node* node;

	free_routing_table();

	DL_FOREACH(graph->nodes, node) {
		struct routing_table_entry* entry;

		if(node->router_id == router_id || node->rank == 2) continue;

		entry = (struct routing_table_entry*) malloc(sizeof(struct routing_table_entry));
		entry->destination 	= node->router_id;
		entry->next_hop		= calculate_next_hop(graph->nodes, node);

		add_routing_entry(entry);
	}
}

int table_sort(struct routing_table_entry* a, struct routing_table_entry* b) {
	char astr[INET6_ADDRSTRLEN];
	char bstr[INET6_ADDRSTRLEN];
	int n;

	convert_ip_ntop(a->destination, astr);
	convert_ip_ntop(b->destination, bstr);

	n = strlen(astr) < strlen(bstr) ? strlen(bstr) : strlen(astr);

    return strncmp(astr, bstr, strlen(astr));
}

void truncate_hostname(char* host) {
	char* first_dot;

	first_dot = strstr(host, ".");
	strncpy(first_dot, "\0", 1);
}

void print_routing_table() {
	struct routing_table_entry* current_entry;
	int 						entry_nr = 1;

	write_lock_table();
	dno_printf(0, "========== Printing routing table ==========");

    if(routing_table == NULL) {
    	dno_printf(0, "%\tempty\n");
    	dno_printf(1, "============================================");
    	pthread_rwlock_unlock(&routing_table_lock);
    	return;
    }

    HASH_SORT(routing_table, table_sort);

    for(current_entry = routing_table; current_entry != NULL; current_entry = current_entry->hh.next) {
    	char 			dest[INET6_ADDRSTRLEN];
    	char 			next_hop[INET6_ADDRSTRLEN];

    	inet_ntop(AF_INET, &(current_entry->destination), dest, INET6_ADDRSTRLEN);
    	inet_ntop(AF_INET, &(current_entry->next_hop), next_hop, INET6_ADDRSTRLEN);

    	//getnameinfo((struct sockaddr*)&sa, sizeof sa, host, sizeof host, service, sizeof service, NI_NOFQDN); 	/* Can be used to convert ip to hostname but may make printing slower. */
    	//truncate_hostname(host);																					/* Truncate the FQDN to just the the hostname. */

    	dno_printf(0, "%4d: \tDestination: %15s \tNext hop: %15s\n", entry_nr, dest, next_hop);

        entry_nr++;
    }

    dno_printf(1, "============================================");

    unlock_table();
}

void read_lock_table() {
    if (pthread_rwlock_rdlock(&routing_table_lock) != 0) {
    	dno_print("read_lock_table: Can't acquire readlock.\n");
    	exit(EXIT_FAILURE);
    }
}

void write_lock_table() {
    if (pthread_rwlock_wrlock(&routing_table_lock) != 0) {
    	dno_print("write_lock_table: Can't acquire writelock.\n");
    	exit(EXIT_FAILURE);
    }
}

void unlock_table() {
	if(pthread_rwlock_unlock(&routing_table_lock) != 0) {
		dno_print("Error unlocking routing_table_lock.\n");
		exit(EXIT_FAILURE);
	}
}
