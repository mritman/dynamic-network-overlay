/*
 * utils.c
 *
 *  Created on: September 16, 2010
 *      Author: mritman
 */

#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <stdarg.h>

#include "utils.h"
#include "socksvr.h"
#include "routinginfo.h"

int gxp_id;
char hostname[MAX_HOSTNAME_LENGTH + 1];

struct timeval start_time;
pthread_mutex_t start_time_mutex = PTHREAD_MUTEX_INITIALIZER;

void get_hostname(char* hostname, int max_hostname_length) {
	int rv;

	hostname[max_hostname_length] = '\0';
	rv = gethostname(hostname, max_hostname_length);
	if (rv < 0) {
		struct timeval time_log;
		get_time_elapsed(&time_log);
		printf("hostname not know: [%ld.%06ld] gethostname: %s (%d)\n",
				time_log.tv_sec, time_log.tv_usec, strerror(errno), errno);
		exit(EXIT_FAILURE);
	}
}

void init_utils() {
	gxp_id = get_gxp_id();
	get_hostname(hostname, MAX_HOSTNAME_LENGTH); /* Retrieve the hostname. The hostname is used for debugging purposes. */
	set_start_time(); /* Initialize the start_time variable needed by the utils.c module. */
	setrandseed(); /* Set the random seed for the rand method used by getrandomint in utils.c. */
}

int get_nr_of_gxp_processes() {
	char* nrofprocs; /* The string value of GXP_NUM_EXECS */
	int n_procs; /* The integer value of GXP_NUM_EXECS */

	nrofprocs = getenv(GXP_NUM_EXECS);
	if (nrofprocs == NULL) {
		dno_print("Environment variable GXP_NUM_EXECS could not be found. Ensure that GXP is running.\n");
		exit(EXIT_FAILURE);
	}
	n_procs = atoi(nrofprocs);
	if (n_procs == 0) {
		dno_print("No running processes or invalid value of GXP_NUM_EXECS (n_procs == 0).\n");
		exit(EXIT_FAILURE);
	}

	return n_procs;
}

int get_gxp_id() {
	char* gxp_proc_id; /* The string value of GXP_EXEC_IDX */
	int gxp_process_id; /* The integer value of GXP_EXEC_IDX */

	gxp_proc_id = getenv(GXP_EXEC_IDX);
	if (gxp_proc_id == NULL) {
		dno_print("Environment variable GXP_EXEC_IDS could not be found. Ensure that GXP is running.\n");
		exit(EXIT_FAILURE);
	}
	gxp_process_id = atoi(gxp_proc_id);

	return gxp_process_id;
}

void synchronize() {
	int n_procs = get_nr_of_gxp_processes();
	int rv;
	int i;
	char buff[2];

	rv = write(GXP_WRITE_FD, "\n", 1);
	if (rv < 0) {
		dno_print_error("Error writing to GXP_WRITE_FD");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < n_procs; i++) {
		rv = read(GXP_READ_FD, buff, 1);
		if (rv < 0) {
			dno_print_error("Error reading from GXP_WRITE_FD");
			exit(EXIT_FAILURE);
		}
	}
}

void write_position(int position) {
	int rv;
	char buffer[sizeof(uint16_t) + 1]; //+1 for '\n' needed by GXP write
	uint16_t pos_nbo;

	buffer[sizeof(uint16_t)] = '\n';

	position++;
	pos_nbo = (uint16_t) htons(position);

	memcpy(buffer, &pos_nbo, sizeof(uint16_t));

	rv = write(GXP_WRITE_FD, buffer, sizeof(uint16_t) + 1);
	if (rv < 0) {
		dno_print_error("write_position: write");
		exit(EXIT_FAILURE);
	}
}

int read_position() {
	int rv;
	char buffer[sizeof(uint16_t) + 1]; //+1 for '\n' needed by GXP write
	uint16_t result;

	rv = read(GXP_READ_FD, buffer, sizeof(uint16_t) + 1);
	if (rv < 0) {
		dno_print_error("Error reading from GXP_WRITE_FD");
		exit(EXIT_FAILURE);
	}

	memcpy(&result, buffer, sizeof(uint16_t));

	result = ntohs(result);

	return result;
}

void setrandseed() {
	int rv;
	struct timeval time;

	rv = gettimeofday(&time, NULL);
	if (rv != 0) {
		dno_print_error("setrandseed: gettimeofday");
		exit(EXIT_FAILURE);
	}

	srand(time.tv_sec + time.tv_usec);
}

int getrandomint(int min, int max) {
	return min + (int) ((max - min + 1) * (rand() / (RAND_MAX + 1.0)));
}

double logbase(double a, double base) {
	return log(a) / log(base);
}

int check_timer(struct timeval* timestamp, int timeout) {
	int rv;
	struct timeval current_time;
	struct timeval interval;
	double time_elapsed = 0;

	rv = gettimeofday(&current_time, NULL);
	if (rv != 0) {
		dno_print_error("check_timer: gettimeofday");
		exit(EXIT_FAILURE);
	}

	get_interval(timestamp, &current_time, &interval);

	time_elapsed = interval.tv_sec + ((double) interval.tv_usec / 1000000);

	if (time_elapsed >= timeout) {
		return 1;
	}

	return 0;
}

int check_timer_sec(int timestamp_sec, int timeout) {
	struct timeval current_time;
	int time_elapsed;

	gettimeofday(&current_time, NULL);

	time_elapsed = current_time.tv_sec - timestamp_sec;

	if (time_elapsed >= timeout) {
		return 1;
	}

	return 0;
}

void set_start_time() {
	int rv;

	rv = pthread_mutex_lock(&start_time_mutex);
	if (rv != 0) {
		printf("%s: set_start_time: error acquiring lock on start_time_mutex\n", hostname);
		exit(EXIT_FAILURE);
	}

	rv = gettimeofday(&start_time, NULL);
	if (rv != 0) {
		printf("%s: set_start_time: gettimeofday: %s (%d)\n", hostname,	strerror(errno), errno);
		exit(EXIT_FAILURE);
	}

	rv = pthread_mutex_unlock(&start_time_mutex);
	if (rv != 0) {
		printf("%s: set_start_time: error releasing lock on start_time_mutex\n", hostname);
		exit(EXIT_FAILURE);
	}
}

void get_time_elapsed(struct timeval* time) {
	int rv;
	struct timeval current_time;

	rv = gettimeofday(&current_time, NULL);
	if (rv != 0) {
		printf("%s: get_time_elapsed: gettimeofday: %s (%d)\n", hostname, strerror(errno), errno);
		exit(EXIT_FAILURE);
	}

	rv = pthread_mutex_lock(&start_time_mutex);
	if (rv != 0) {
		printf("%s: get_time_elapsed: error acquiring lock on start_time_mutex\n", hostname);
		exit(EXIT_FAILURE);
	}

	if (start_time.tv_usec > current_time.tv_usec) {
		time->tv_usec = 1000000 - (start_time.tv_usec - current_time.tv_usec);
		time->tv_sec = current_time.tv_sec - start_time.tv_sec;
		time->tv_sec--;
	} else {
		time->tv_sec = current_time.tv_sec - start_time.tv_sec;
		time->tv_usec = current_time.tv_usec - start_time.tv_usec;
	}

	rv = pthread_mutex_unlock(&start_time_mutex);
	if (rv != 0) {
		printf("%s: get_time_elapsed: error releasing lock on start_time_mutex\n", hostname);
		exit(EXIT_FAILURE);
	}
}

void get_interval(struct timeval* time_start, struct timeval* time_stop, struct timeval* result) {
	if (time_start->tv_usec > time_stop->tv_usec) {
		result->tv_usec = 1000000 - (time_start->tv_usec - time_stop->tv_usec);
		result->tv_sec = time_stop->tv_sec - time_start->tv_sec;
		result->tv_sec--;
	} else {
		result->tv_sec = time_stop->tv_sec - time_start->tv_sec;
		result->tv_usec = time_stop->tv_usec - time_start->tv_usec;
	}
}

void get_ipstr_and_port_from_addrinfo(struct addrinfo* p, char* ipstr, int* port) {
	void* addr;
	int temp_port;

	if (p->ai_family == AF_INET) { // IPv4
		struct sockaddr_in* ipv4 = (struct sockaddr_in *) p->ai_addr;
		addr = &(ipv4->sin_addr);
		temp_port = ntohs(ipv4->sin_port);
	} else { // IPv6
		struct sockaddr_in6* ipv6 = (struct sockaddr_in6 *) p->ai_addr;
		addr = &(ipv6->sin6_addr);
		temp_port = ntohs(ipv6->sin6_port);
	}

	if (ipstr != NULL) {
		inet_ntop(p->ai_family, addr, ipstr, INET6_ADDRSTRLEN);
		if (ipstr == NULL) {
			dno_print_error("get_ipstr_and_port_from_addrinfo: inet_ntop");
		}
	}
	if (port != NULL) {
		*port = temp_port;
	}
}

void get_ipstr_and_port_from_sockaddr_storage(struct sockaddr_storage* p,
		char* ipstr, int* port) {
	void* addr;
	int temp_port;

	if (p->ss_family == AF_INET) { // IPv4
		struct sockaddr_in* ipv4 = (struct sockaddr_in *) p;
		addr = &(ipv4->sin_addr);
		temp_port = ntohs(ipv4->sin_port);
	} else { // IPv6
		struct sockaddr_in6* ipv6 = (struct sockaddr_in6 *) p;
		addr = &(ipv6->sin6_addr);
		temp_port = ntohs(ipv6->sin6_port);
	}

	if (ipstr != NULL) {
		inet_ntop(p->ss_family, addr, ipstr, INET6_ADDRSTRLEN);
		if (ipstr == NULL) {
			dno_print_error("get_ipstr_and_port_from_addrinfo: inet_ntop");
		}
	}
	if (port != NULL) {
		*port = temp_port;
	}
}

void convert_ip_ntop(int ip, char* ipstr) {
	// IPv4:
	struct sockaddr_in sa; // pretend this is loaded with something

	sa.sin_addr.s_addr = ip;

	inet_ntop(AF_INET, &(sa.sin_addr), ipstr, INET6_ADDRSTRLEN);
}

void dno_printf(int flush, const char* format, ...) {
	va_list args;
	struct timeval time_log;

	get_time_elapsed(&time_log);

	printf("%s : [ %4ld.%06ld ] ", hostname, time_log.tv_sec, time_log.tv_usec);

	va_start(args, format); /* Initialize the argument list. */
	vprintf(format, args); /* Print the caller's string */
	va_end(args); /* Clean up the arguments list */

	if (strchr(format, '\n') == NULL) { /* If the format string did not contain a newline character print one anyway. Gxpc needs a newline character to return when writing to its stdout file descripter */
		printf("\n");
	}

	if (flush) {
		fflush(stdout);
	}
}

void dno_print_error(const char* format, ...) {
	va_list args;
	struct timeval time_log;

	get_time_elapsed(&time_log);

	printf("%s: [%4ld.%06ld] ", hostname, time_log.tv_sec, time_log.tv_usec);

	va_start(args, format); /* Initialize the argument list. */
	vprintf(format, args); /* Print the caller's string */
	va_end(args); /* Clean up the arguments list */

	printf(": %s (%d)\n", strerror(errno), errno); /* Print the error message and error number */

	fflush(stdout);
}

void dno_print_dbg(const char *file, int line, const char *function, int flush,
		const char* format, ...) {
	va_list args;
	struct timeval time_log;

	get_time_elapsed(&time_log);

	printf("%s: [%4ld.%06ld] ", hostname, time_log.tv_sec, time_log.tv_usec);
	printf("%s(%d):%s() Called dno_print_debug\n", file, line, function);

	printf("%s: [%4ld.%06ld] ", hostname, time_log.tv_sec, time_log.tv_usec);

	va_start(args, format); /* Initialize the argument list. */
	vprintf(format, args); /* Print the caller's string */
	va_end(args); /* Clean up the arguments list */

	if (strchr(format, '\n') == NULL) { /* If the format string did not contain a newline character print one anyway. Gxpc needs a newline character to return when writing to its stdout file descripter */
		printf("\n");
	}

	if (flush) {
		fflush(stdout);
	}
}

void dno_log_priv(const char* format, ...) {
	va_list args;
	struct timeval time_log;

	if (!LOGGING)
		return;

	char filename[MAX_HOSTNAME_LENGTH + 1];
	sprintf(filename, "%s.log", hostname);

	FILE* logfile = fopen(filename, "a");

	if (logfile == NULL) {
		dno_print_error("Error opening logfile");
		return;
	}

	get_time_elapsed(&time_log);

	fprintf(logfile, "%s : [ %4ld.%06ld ] ", hostname, time_log.tv_sec,
			time_log.tv_usec);

	va_start(args, format); /* Initialize the argument list. */
	vfprintf(logfile, format, args); /* Print the caller's string */
	va_end(args); /* Clean up the arguments list */

	fflush(logfile);

	fclose(logfile);
}

int connect_to_router(struct router* router, int type) {
	int rv;
	int sockfd;
	uint16_t request_nbo;
	struct addrinfo hints, *res;
	char sock_svr_portstring[5];

	sprintf(sock_svr_portstring, "%d", SOCK_SVR_PORT);

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	getaddrinfo(router->ip_string, sock_svr_portstring, &hints, &res);

	sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (sockfd == -1) {
		dno_print_error("connect_to_router: socket");
		return -1;
	}

	/* Connect to router */
	rv = connect(sockfd, res->ai_addr, res->ai_addrlen);
	if (rv == -1) {
		dno_print_error("connect_to_router: connect: to %s", router->ip_string);
		close(sockfd);
		return -1;
	}

	freeaddrinfo(res);

	/* Tell the other end what type of connection is wanted */
	request_nbo = htons(type);
	rv = send(sockfd, &request_nbo, sizeof(request_nbo), 0);
	if (rv == -1) {
		dno_print_error("connect_to_router: send.");
		close(sockfd);
		return -1;
	}

	return sockfd;
}

uint32_t get_router_id(char* ipstr) {
	struct sockaddr_in sa;

	inet_pton(AF_INET, ipstr, &(sa.sin_addr));

	return sa.sin_addr.s_addr;
}

int is_readable(int fd) {
	int rv;
	fd_set read_set;
	struct timeval timeout;

	timeout.tv_sec = 0;
	timeout.tv_usec = 0;

	FD_ZERO(&read_set);
	FD_SET(fd, &read_set);

	rv = select(fd + 1, &read_set, NULL, NULL, &timeout);
	if (rv < 0) {
		dno_print_error("is_readable: select failed");
		exit(EXIT_FAILURE);
	}

	if (FD_ISSET(fd, &read_set)) {
		return 1;
	}

	return 0;
}
