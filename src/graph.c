/*
 * graph.c
 *
 *  Created on: Mar 1, 2011
 *      Author: mritman
 */

#include <stdlib.h>
#include <inttypes.h>

#include "graph.h"
#include "utlist.h"
#include "uthash.h"
#include "packettypes.h"
#include "utils.h"
#include "linkstatedatabase.h"
#include "dno.h"
#include "termination.h"

#define INFINITE_DISTANCE 0xFFFFFFFF									/* The maximum value of uint32_t. */

void print_graph(struct graph* graph);

int find_nodes_router_id_rank(struct node* a, struct node* b) {
	if (a->router_id == b->router_id && a->rank == b->rank) {
		return 0;
	} else {
		return 1;
	}
}

int sort_nodes_distance(struct node* a, struct node* b) {
	if (a->dist == b->dist) {
		return 0;
	} else if (a->dist < b->dist) {
		return -1;
	} else {
		return 1;
	}
}

struct node* find_clone(struct node* nodes, struct node* node) {
	struct node* current_node;

	DL_FOREACH(nodes, current_node) {
		if (current_node->router_id == node->router_id && current_node->rank != node->rank) {
			return current_node;
		}
	}

	return NULL;
}

void free_node(struct node* node) {
	struct edge* edge, *tmp;

	DL_FOREACH_SAFE(node->edges, edge, tmp) {
		DL_DELETE(node->edges, edge);
		free(edge);
	}

	free(node);
}

void free_graph(struct graph* graph) {
	struct node* node, *tmp;

	DL_FOREACH_SAFE(graph->nodes, node, tmp) {
		struct node* clone = NULL;

		DL_DELETE(graph->nodes, node);

		clone = find_clone(graph->nodes, node);
		if (clone != NULL) {
			clone->edges = NULL;
		}

		free_node(node);
	}

	free(graph);
}

void traverse(struct graph* graph, struct node* node, int* id_counter) {
	struct edge* edge;

	if(node->id != -1) {
		return;
	}

	node->id = *id_counter;
	*id_counter += 1;

	DL_FOREACH(node->edges, edge) {
		struct node* destination_node = NULL;

		DL_SEARCH_SCALAR(graph->nodes, destination_node, router_id, edge->dst);

		if (destination_node == NULL) {
			dno_print("traverse: destination_node not found, exiting.\n");
			print_graph(graph);
			//exit(EXIT_FAILURE);
		}

		if (destination_node->id == -1 || destination_node->id > node->id) {
			edge->label = DOWN;
		} else {
			edge->label = UP;
		}

		traverse(graph, destination_node, id_counter);
	}
}

void assign_edge_labels(struct graph* graph) {
	int id_counter = 0;
	traverse(graph, graph->nodes, &id_counter);
}

struct node* smallest_distance(struct node* nodes) {
	struct node* node, *smallest;
	smallest = NULL;

	DL_FOREACH(nodes, node) {
		if (!node->done && (smallest == NULL || smallest->dist > node->dist) && node->dist != INFINITE_DISTANCE) {
			smallest = node;
		}
	}

	return smallest;
}

struct node* find_destination(struct node* nodes, uint32_t m, int k) {
	struct node like;
	struct node* destination = NULL;

	like.router_id 	= m;
	like.rank		= k;

	DL_SEARCH(nodes, destination, &like, find_nodes_router_id_rank);
	if (destination == NULL) {
		dno_print("find_destination: destination == NULL, exiting.\n");
		exit(EXIT_FAILURE);
	}

	return destination;
}

void reset_graph(struct graph* graph) {
	struct node* node;

	DL_FOREACH(graph->nodes, node) {
		node->dist 		= INFINITE_DISTANCE;
		node->done 		= 0;
		node->previous 	= NULL;
	}
}

void dijkstra(struct graph* graph, uint32_t root) {
	struct node* node;																/* Only accounting for up/down routing. */

	DL_FOREACH(graph->nodes, node) {
		if (node->router_id == root) {
			node->dist = 0;
		}
	}

	while(1) {
		struct edge* edge;

		node = smallest_distance(graph->nodes); 											/* Get the node with the smallest distance. */
		if (node == NULL) {
			break;																			/* If node == NULL the algorithm is done. */
		}
		node->done = 1;																		/* Make sure this node will not be considered during next iteration. */

		DL_FOREACH(node->edges, edge) {
			struct node* destination = NULL;
			uint32_t d;

			destination = find_destination(graph->nodes, edge->dst, 1);
			d 			= node->dist + edge->dist;

			if (d < destination->dist) {
				destination->dist = d;
				destination->previous = node;
			}
		}
	}
}

/* See:
 * Ken Hironaka, Hideo Saito, Kenjiro Taura. High Performance Wide-area Overlay using Deadlock-free Routing.
 * 2009 International ACM Symposium on High Performance Distributed Computing (HPDC2009). pp.81-90, June 2009.
 *
 * http://www.logos.t.u-tokyo.ac.jp/wiki-en/index.php?Publications
 */
void leveled_dijkstra(struct graph* graph, uint32_t root, int r) {
	struct node* node;																/* Only accounting for up/down routing. */

	DL_FOREACH(graph->nodes, node) {
		if (node->router_id == root) {
			node->dist = 0;
		}
	}

	while(1) {
		struct edge* edge;

		node = smallest_distance(graph->nodes); 											/* Get the node with the smallest distance. */
		if (node == NULL) {
			break;																			/* If node == NULL the algorithm is done. */
		}
		node->done = 1;																		/* Make sure this node will not be considered during next iteration. */

		DL_FOREACH(node->edges, edge) {
			struct node* destination = NULL;
			int k;
			int i = node->rank;

			uint32_t 	m = edge->dst;
			int 		j = edge->label;

			for (k = i; k <= r; k++) {
				if (i <= j && j <= k) {
					destination = find_destination(graph->nodes, m, k);
					uint32_t d = node->dist + edge->dist;

					if (d < destination->dist) {
						destination->dist = d;
						destination->previous = node;
					}
				}
			}
		}
	}
}

/* Dijkstra's algorithm is run on the graph with the root set to the first node in the graph (which should have id==0).
 * The first node corresponds to the first lsa in the link-state database which is sorted before creating the graph.
 * See the sort method used for the link-state database to see how to order is determined.
 */
void set_parent_and_children(struct graph* graph) {
	struct node* self, *node;
	struct connection* tmp;

	free(parent);																		/* Clear old parent. */
	free_children();																	/* Clear old children. */
	children = NULL;

	root = graph->nodes->router_id;

	dijkstra(graph, root);

	DL_SEARCH_SCALAR(graph->nodes, self, router_id, router_id);

	parent = (struct connection*) malloc(sizeof(struct connection));

	if (self->previous != NULL) {
		tmp = find_connection(connections, self->previous->router_id);
	} else {
		tmp = NULL;
	}

	if (tmp != NULL) {
		memcpy(parent, tmp, sizeof(struct connection));									/* The parent connection should only be used for sending termination detection packets so it doesn't matter that just the fields of the connection struct are copied and not what they reference. */
	} else {
		parent->neighbor_id = 0;														/* There is no parent nor a connection to a parent if the router is the root of the tree. */
	}

	DL_FOREACH(graph->nodes, node) {													/* Find all the nodes that have node->previous->router_id == router_id. */
		if (node->previous != NULL && node->previous->router_id == router_id) {
			add_child(node->router_id);													/* Add them to the child list. */
		}
	}

	reset_graph(graph);																	/* Reset the graph so that it can be used again to calculate the shortest-path tree for this router. */
}

struct graph* construct_graph() {
	struct graph* graph = (struct graph*) malloc(sizeof(struct graph));
	struct lsdb_entry* lsdb_entry, *tmp;
	struct node* current_node;
	struct node* temporary_nodelist, *tmp_node;

	temporary_nodelist = NULL;
	graph->nodes = NULL;

	write_lock_lsdb();

	HASH_SORT(link_state_database, database_sort);									/* Sort the database so that the nodes of each routers graph have the same order. This will ensure that all routers will assign the same IDs to the nodes. */

	/* First create a graph with one node per router in the area. */
	HASH_ITER(hh, link_state_database, lsdb_entry, tmp) {
		if (lsdb_entry->key.link_state_type != ROUTER_LSA) {						/* Only ROUTER_LSAs are used to calculate routes. TODO check this when adding support for multiple areas. */
			continue;
		}

		struct lsa_type1_link* link;

		struct node* node = (struct node*) malloc(sizeof(struct node));
		node->dist 		= INFINITE_DISTANCE;
		node->id		= -1;
		node->previous 	= NULL;
		node->rank		= 1;
		node->router_id = lsdb_entry->key.link_state_id;
		node->edges 	= NULL;
		node->done		= 0;

		DL_FOREACH(lsdb_entry->links, link) {
			struct edge* edge = (struct edge*) malloc(sizeof(struct edge));
			edge->dist 	= link->metric;
			edge->dst 	= link->link_id;
			edge->label = -1;

			DL_APPEND(node->edges, edge);
		}

		DL_APPEND(graph->nodes, node);
	}

	unlock_lsdb();

	assign_edge_labels(graph);														/* Assigns up and down directions to all edges. */

	set_parent_and_children(graph);													/* The termination code calls construct_graph only and always has a lock on termination_lock. */

	DL_FOREACH(graph->nodes, current_node) {										/* Make a copy of each node in the graph. */
		struct node* new_node = (struct node*) malloc(sizeof(struct node));
		memcpy(new_node, current_node, sizeof(struct node));
		new_node->rank = 2;
		DL_APPEND(temporary_nodelist, new_node);
	}

	DL_FOREACH_SAFE(temporary_nodelist, current_node, tmp_node) {					/* Add the copies to the graph. */
		DL_DELETE(temporary_nodelist, current_node);
		DL_APPEND(graph->nodes, current_node);
	}

	leveled_dijkstra(graph, router_id, 2);											/* Calculate the shortest path values. */

	return graph;
}

void print_graph(struct graph* graph) {
	struct node* node;

	dno_printf(0, "========== Printing graph ==========");

    if(graph == NULL || graph->nodes == NULL) {
    	dno_printf(0, "\tEmpty graph.\n");
    	dno_printf(1, "==================================================");
    	return;
    }

    DL_FOREACH(graph->nodes, node) {
    	struct edge* edge;
    	char router_id[INET6_ADDRSTRLEN];
    	char previous_router_id[INET6_ADDRSTRLEN];
    	int previous_rank;

    	inet_ntop(AF_INET, &(node->router_id), router_id, INET6_ADDRSTRLEN);
    	if (node->previous != NULL) {
    		inet_ntop(AF_INET, &(node->previous->router_id), previous_router_id, INET6_ADDRSTRLEN);
    		previous_rank = node->previous->rank;
    	} else {
    		previous_router_id[0] = '\0';
    		previous_rank = 0;
    	}

    	dno_printf(0, "Node: router_id: %15s\t rank: %5d\t id: %5d distance: %5"PRIu32" previous: (%s, %d)\n", router_id, node->rank, node->id, node->dist, previous_router_id, previous_rank);
    	DL_FOREACH(node->edges, edge) {
    		char destination[INET6_ADDRSTRLEN];

    		inet_ntop(AF_INET, &(edge->dst), destination, INET6_ADDRSTRLEN);

    		dno_printf(0, "\tEdge: %15s\t label: %5d\t distance: %5d\n", destination, edge->label, edge->dist);
    	}
    }

    dno_printf(1, "==================================================");
}
