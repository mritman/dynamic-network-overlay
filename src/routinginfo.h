/*
 * routinginfo.h
 *
 *  Created on: Feb 1, 2011
 *      Author: mritman
 */

#ifndef ROUTINGINFO_H_
#define ROUTINGINFO_H_

#include <stdio.h>
#include <unistd.h>
#include <stdint.h>

//#define STATE_INIT		0
#define	STATE_EXSTART		1
#define STATE_EXCHANGE 		2
#define STATE_LOADING		3
#define STATE_FULL			4

#define STATE_EXSTART_STR	"EXSTART"
#define STATE_EXCHANGE_STR	"EXCHANGE"
#define STATE_LOADING_STR	"LOADING"
#define STATE_FULL_STR		"FULL"

#define INITIAL_AGE					0
#define MAX_AGE						3600						/* Maximum age of an LSA in seconds. */
#define MAX_AGE_DIFF				900
#define INF_TRANS_DELAY				1
#define AGE_UPDATE_INTERVAL			5
#define INITIAL_SEQUENCE_NUMBER		0x80000001
#define MAX_SEQUENCE_NUMBER			0x7fffffff
#define LS_REFRESH_TIME				3500
#define LS_REFRESH_TIME_MAX_OFFSET	60							/* The maximum random offset to the LS_REFRESH_TIME. To prevent many LSAs to be refreshed at the same time. */

void 	routinginfo_init();

pthread_t create_routing_information_thread();

void 	get_state_string(char* string, int state, int len);

void 	add_self_to_lsdb();

void	disconnect();

#endif /* ROUTINGINFO_H_ */
