/*
 * routinginfo.c
 *
 *  Created on: Jan 4, 2011
 *      Author: mritman
 */

#include <pthread.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>

#include "routinginfo.h"
#include "dno.h"
#include "connections.h"
#include "packets.h"
#include "utlist.h"
#include "linkstatedatabase.h"
#include "routingio.h"
#include "utils.h"
#include "termination.h"
#include "stats.h"

int32_t next_sequence_number = INITIAL_SEQUENCE_NUMBER;
struct timeval age_update_time;
struct timeval lsa_refresh_time;
int ls_refresh_offset;

pthread_rwlock_t 	dc_lock;
int					dc_msg = 0;

int all_neighbors_synchronized();
struct lsdb_entry* create_new_lsa(uint32_t area_id);
void process_dd_packet(struct dd_packet* dd_packet, struct connection* connection);
void process_lsr_packet(struct lsr_packet* lsr_packet, struct connection* connection);
void process_lsu_packet(struct lsu_packet* lsu_packet, struct connection* connection);
void process_dc_packet(struct dc_packet* dc_packet, struct connection* connection);
void send_dd_packet(struct connection* connection);
void send_lsr_packet(struct connection* connection);
void send_dc_packet(struct connection* connection);
void originate_lsa(uint32_t area_id);
void flood_lsu(struct lsu_packet* lsu_packet, uint32_t area_id, struct connection* incoming_connection);
void age_lsas();
void refresh_lsa();
void process_dc();
void read_lock_dc();
void write_lock_dc();
void unlock_dc();

/* Connections are added to two datastructures. One is the table of connections of the connections.c module,
 * the other is the table(s) of routing_connections used by the routingio.c module.
 * The connections table is used by the part of the program that is responsible for calculating the routing information.
 * The tables from the routingio module are used by the part of the program that does the actual packet forwarding.
 * Each of these modules maintains their own collections of connections so that they don't get in each other's way.
 * They would have had to 'compete' for access if there was one shared resource for all the connections.
 */
void *routing_information_thread(void *arg) {
	struct connection* connection, *tmp;
	int break_loop = 0;

	gettimeofday(&age_update_time, NULL);
	gettimeofday(&lsa_refresh_time, NULL);
	ls_refresh_offset = getrandomint(1, LS_REFRESH_TIME_MAX_OFFSET);

	gettimeofday(&term_start, NULL);

	//TODO LSA sequence number wrapping

	while(run) {
		write_lock_connections();

		process_dc();

		detect_termination();

		HASH_ITER(hh, connections, connection, tmp) {											/* For each connection the router has. */

			if (connection->state == STATE_EXSTART) {
				/* If the router is inactive, do not process the new connection.
				 * In the context of the termination detection process,
				 * accepting a new connection is part of the primary computation.
				 * The primary computation can not be restarted while the router is inactive
				 * for the termination detection algorithm to work correctly.
				 *
				 * If the router on the other end of this connection
				 * is already connected to another member of the overlay that is still active it will eventually generate a new lsa
				 * that will reach this router and thus wake up this router, after which this router can be added.
				 * When this LSA is processed the router is woken up, but can go back to sleep at the beginning of the iteration through the connections.
				 * If the waiting connection comes before the connection from which a new lsa was received it is possible that the router goes inactive again
				 * before the waiting router is served. -> deadlock
				 *
				 * The new router however will also generate a new lsa that will reach this router through the active router it connected to.
				 * The prevent the deadlock described above a check is also made if there exists and LSA for the new router in the database.
				 * If this is the case it means that it is ok to be woken up by it because it is part of the termination detection tree already and it and its parent are still active.
				 *
				 * NOTE:
				 * To explain why there would be a deadlock otherwise.
				 * The newly connected router would end up in a busy loop while trying to send its database description (receive the buffers are getting full at the inactive router it is try to synch with).
				 * This means it will never be able to send its packet fully (and packets over the routing info connection will busy loop until there are fully sent).
				 * This means it will never terminate and the new routers parent will neither -> deadlock in overlay
				 */
				if (is_inactive() && !in_lsdb(connection->neighbor_id) && connection->neighbor_id != parent->neighbor_id) {
					continue;
				}
				connection->database_summary_list = get_database_summary_list();				/* The database summary list is a list of LSA headers that are in the link state database when this connection goes into the EXCHANGE state. */
				connection->state = STATE_EXCHANGE;
			}

			if (is_readable(connection->fd_info) && connection->packet_ready != 1) {			/* If the control connection has bytes to be read and the buffer is not full with a previous packet. */
				read_packet(connection);
			}
			if (connection->packet_ready) {															/* If a packet has been fully read. */
				if (is_termination_packet(connection->header.type)) {
					struct termination_packet* term_packet = unpack_termination_packet(connection->buffer);

					connection = process_termination_packet(term_packet, connection);

					if (term_packet->common_header.type == TERMINATION) break_loop = 1;				/* If connection where removed during because neighbors were leaving break from the loop because tmp might be invalid if it pointed to one of the connections that was removed. */
					free(term_packet);

					if (connection == NULL) {														/* If connection was removed during termination (because it disconnected) break the loop. */
						break;
					}
				} else {
					received_primary_msg_routinginfo(connection, connection->header.flags);

					if (connection->header.type == DATABASE_DESCRIPTION) {
						struct dd_packet* dd_packet = unpack_dd_packet(connection->buffer);			/* Unpack the database description packet. */

						dno_log("Received DATABASE_DESCRIPTION from %s\n", connection->neighbor_ip_string);
						process_dd_packet(dd_packet, connection);									/* Put the LSA headers in the dd packet that are new or newer than the copy this router has, in the link state request list. */

						free_dd_packet(dd_packet);													/* Free the database description packet. */

						update_stats(DD_PACKETS_RECEIVED);
					} else if (connection->header.type == LINK_STATE_REQUEST) {
						struct lsr_packet* lsr_packet = unpack_lsr_packet(connection->buffer);		/* Unpack the link state request packet. */

						dno_log("Received LINK_STATE_REQUEST from %s\n", connection->neighbor_ip_string);
						process_lsr_packet(lsr_packet, connection);

						free_lsr_packet(lsr_packet);

						update_stats(LSR_PACKETS_RECEIVED);
					} else if (connection->header.type == LINK_STATE_UPDATE) {
						struct lsu_packet* 	lsu_packet = unpack_lsu_packet(connection->buffer);

						dno_log("Received LINK_STATE_UPDATE from %s\n", connection->neighbor_ip_string);
						process_lsu_packet(lsu_packet, connection);

						free_lsu_packet(lsu_packet);

						update_stats(LSU_PACKETS_RECEIVED);
					} else if (connection->header.type == DISCONNECT) {
						struct dc_packet* dc_packet = unpack_dc_packet(connection->buffer);
						uint32_t area_id = connection->area_id;										/* process_dc_packet() will free the connection struct so store the area_id. */

						dno_log("Received DISCONNECT from %s\n", connection->neighbor_ip_string);
						process_dc_packet(dc_packet, connection);

						free(dc_packet);

						if (!is_disconnecting()) originate_lsa(area_id);							/* This router's lsa has changed, reflood it but only if this router is not disconnecting itself. This will cause this newly generated lsa to be flooded after its previous LSA was flushed from the overlay resulting in corrupt LSDBs. */
						start_termination();														/* Start the termination process. */

						update_stats(DISCONNECTS_RECEIVED);
					}
				}

				reset_packet_fields(connection);													/* Free header_buffer and reset other packet fields so they can be used to read the next packet on the control connection. */
				if (break_loop) {
					break_loop = 0;
					break;
				}
			}

			if (connection->state == STATE_EXCHANGE || connection->state == STATE_LOADING) {
				if (connection->lsrq_list_pos != NULL) {										/* If lsrq_list_pos != null not all LSAs in the LSR list have been sent. */
					send_lsr_packet(connection);
				}

				if (!connection->last_dd) {														/* If the summary list is not empty, the connection is still in the exchange state. */
					send_dd_packet(connection);
				} else if (connection->database_summary_list == NULL && connection->dd_done) {	/* Else if the summary list is empty and if all the DD packets from the neighbor have been received the database description process is done. */
					if (connection->link_state_request_list != NULL) {							/* If there are still packets in the LSR list then not all LSRs have been satisfied yet. */
						connection->state = STATE_LOADING;										/* Transition to the loading state. */
					} else {
						connection->state = STATE_FULL;											/* Transition to the full state. */

						struct routing_connection* routing_connection = initialize_routing_connection(connection->fd, connection->fd_info, connection->neighbor_ip_string, connection->neighbor_port);
						add_to_full_and_read_set(routing_connection);							/* The data packet forwarding thread that does the actual forwarding can now use this connection. */

						if (all_neighbors_synchronized()) {
							gettimeofday(&sync_end, NULL);										/* Measure start of termination. */
							originate_lsa(connection->area_id);									/* This router needs to generate a new LSA and put this in its own database as well. */
							start_termination();
						}
					}
				}
			}
		}

		age_lsas();
		//purge_expired_lsas();
		refresh_lsa();

		unlock_connections();
	}

	pthread_exit(NULL);
}

int all_neighbors_synchronized() {
	struct connection* connection, *tmp;

	HASH_ITER(hh, connections, connection, tmp) {
		if (connection->state != STATE_FULL) {
			return 0;
		}
	}

	return 1;
}

void routinginfo_init() {
	int rv;

	rv = pthread_rwlock_init(&dc_lock,NULL);
	if (rv != 0) {
		dno_print_error("Lock init failed on dc_Lock");
		exit(EXIT_FAILURE);
	}
}

void process_dc() {
	struct connection* connection, *tmp;

	write_lock_dc();

	if (dc_msg) {

		write_lock_termination();

		if (!inactive) {
			dc = 1;
			dc_msg = 0;

			HASH_ITER(hh, connections, connection, tmp) {
				send_dc_packet(connection);
			}
		}

		unlock_termination();

		if (!dc_msg) start_termination();						/* Only start termination if the disconnect has been processed. */
	}

	unlock_dc();
}

void add_self_to_lsdb() {
	struct lsdb_entry* lsa;

	lsa	= create_new_lsa(0);		//TODO The area_id argument is static here but needs to be changed when support for multiple areas is implemented
	add_lsdb_entry(lsa);
}

void process_dd_packet(struct dd_packet* dd_packet, struct connection* connection) {
	struct lsa_header_element* elt, *tmp;

	DL_FOREACH_SAFE(dd_packet->lsa_headers, elt, tmp) {
		if (is_newer(elt->key, elt->header)) {
			struct lsa_key_element* lsa_key_element = (struct lsa_key_element*) malloc(sizeof(struct lsa_key_element));

			lsa_key_element->key	= elt->key;
			lsa_key_element->next 	= NULL;
			lsa_key_element->prev 	= NULL;

			DL_APPEND(connection->link_state_request_list, lsa_key_element);
			if (connection->lsrq_list_pos == NULL) {
				connection->lsrq_list_pos = lsa_key_element;
			}
		}
	}

	if (!HAS_MORE(dd_packet->dd_header.flags)) {
		connection->dd_done = 1;																			/* If the more bit was zero in the dd packet the other side is done with sending its dd_done packets so set done to true. */
	}
}

void process_lsr_packet(struct lsr_packet* lsr_packet, struct connection* connection) {
	struct lsa_key_element* current_key = lsr_packet->lsa_keys;

	while (current_key != NULL) {																			/* While not all requested LSAs are sent. */
		struct lsu_packet* lsu_packet = create_lsu_packet(&current_key, router_id, connection->area_id);	/* Create an LSU packet. */
		char* buffer = pack_lsu_packet(lsu_packet);															/* Pack the LSU packet in a buffer. */

		set_ackreq_bit_routinginfo(connection, buffer);

		send_packet(connection, buffer, lsu_packet->common_header.length);									/* Send the packet. */
		dno_log("Sent LINK_STATE_UPDATE to %s (requested)\n", connection->neighbor_ip_string);
		update_stats(LSU_PACKETS_SENT);

		//sent_primary_msg_routinginfo(connection); TODO remove, replaced by ackreq bit

		free(buffer);
		free_lsu_packet(lsu_packet);
	}
}

int lsr_cmp(struct lsa_key_element* a, struct lsa_key_element* b) {
	if (a->key.advertising_router == b->key.advertising_router) {
		if (a->key.link_state_id == b->key.link_state_id) {
			if (a->key.link_state_type == b->key.link_state_type) {
				return 0;
			}
		}
	}

	return 1;
}

void process_lsu_packet(struct lsu_packet* lsu_packet, struct connection* connection) {
	struct lsu_packet* 		new_lsu_packet;
	struct lsa_type1* 		lsa, *tmp;
	struct lsa_key_element* lsa_keys 		= NULL;
	struct lsa_key_element* lsa_keys_pos 	= NULL;
	int 					requested_lsu	= 0;

	DL_FOREACH_SAFE(lsu_packet->lsas_type1, lsa, tmp) {
		struct lsa_key_element* lsr = NULL;
		struct lsa_key_element  like;

		like.key = lsa->key;

		DL_SEARCH(connection->link_state_request_list, lsr, &like, lsr_cmp);								/* Try to find the LSA in the link-state request list. */

		if (lsr) {																							/* If it is in the link-state request list. */
			DL_DELETE(connection->link_state_request_list, lsr); 											/* Remove the satisfied LSA request from the link-state request list and free it. */
			free(lsr);
			requested_lsu = 1;
		} else {
			requested_lsu = 0;
		}

		//See section 13, 13.1, 13.2 for installation of a new LSA in the database.
		if (is_newer(lsa->key, lsa->header)) {
			struct lsa_key_element* lsa_key = (struct lsa_key_element*) malloc(sizeof(struct lsa_key_element));
			lsa_key->key 					= lsa->key;

			//if (!requested_lsu) {
				DL_APPEND(lsa_keys, lsa_key);																/* Add the LSA to the list of LSAs that will be flooded. Only if it wasn't requested as part of the synchronization procedure. */
			//}

			int changed = update_lsdb(lsa);																	/* Update the link state database with the newer LSA. */
			if (changed) {
				start_termination();
			}
		}
	}

	lsa_keys_pos = lsa_keys;

	if (lsa_keys_pos != NULL) {
		new_lsu_packet = create_lsu_packet(&lsa_keys_pos, router_id, connection->area_id);

		flood_lsu(new_lsu_packet, connection->area_id, connection);

		free_lsa_keys(lsa_keys);
		free_lsu_packet(new_lsu_packet);
	}

	/*
	 * TODO
	 * Do I need to make a distinction between lsu packets that are coming in because they were
	 * requested by the db synching procedure or because they were caused by a neighbors flooding procedure.
	 *
	 * More precisely, do lsu's that are part of a synching process need to be forwarded just like lsu's that are part
	 * of the flooding process. If they are, and a neighbor already has the newest lsa, it will just be ignored
	 */

	// Lsu's are either created as a response to a lsr or to another lsu.
	// Because of this a lsu will either only contain lsa's for the synching procedure or the flooding procedure, but not both.
}

void process_dc_packet(struct dc_packet* dc_packet, struct connection* connection) {
	lsdb_entry_key key;																										/* Create an lsdb_entry_key to find the LSA of the neighbor in the link-state database. */
	key.advertising_router = dc_packet->common_header.router_id;
	key.link_state_id	   = dc_packet->common_header.router_id;
	key.link_state_type	   = ROUTER_LSA;
	//TODO when implementing other type LSA also flush those from the lsdb

	struct lsa_key_element* lsa_key = NULL;
	struct lsa_key_element* lsa_key_2 = (struct lsa_key_element*) malloc(sizeof(struct lsa_key_element));					/* Create an LSA key from which to make an lsu_packet. */
	lsa_key_2->key 		= key;
	lsa_key_2->next 	= NULL;
	lsa_key_2->prev 	= NULL;
	DL_APPEND(lsa_key, lsa_key_2);

	struct lsa_key_element* lsa_key_tmp;																					/* Create a temporary copy of the pointer to the LSA key. */
	lsa_key_tmp = lsa_key;

	struct lsu_packet* new_lsu_packet = create_lsu_packet(&lsa_key_tmp, router_id, connection->area_id);					/* Create the lsu_packet. */

	struct lsdb_entry* lsdb_entry = find_lsdb_entry(key);																	/* Find the the LSA_TYPE1 of the neighbor that sent the dc_packet */
	delete_lsdb_entry(lsdb_entry);																							/* Remove it from the link-state database. Note, only do this after creating the LSU packet above because it reads this LSA from the database. */ //TODO when implementing termination detection removal of a link has to wait until termination has been detected

	if (new_lsu_packet->lsas_type1 != NULL) {																				/* Only flood the LSU if there was an entry in the link-state database for the neighbor that sent the dc_packet. */
		new_lsu_packet->lsas_type1->header.age = MAX_AGE;																	/* There is only one LSA in the packet. Set the age of the LSA to MAX_AGE. */
		flood_lsu(new_lsu_packet, connection->area_id, connection);															/* Flood the LSU so all the other routers will also remove the LSA from their database. */
	}

	add_dc_neighbor(connection->neighbor_id);																				/* Add the neighbor to the list of disconnecting neighbors. This will be processed after termination detection. */
	connection->disconnected = 1;

	free_lsa_keys(lsa_key);
	free_lsu_packet(new_lsu_packet);
}

void send_dd_packet(struct connection* connection) {
	struct dd_packet* dd_packet = create_dd_packet(&(connection->database_summary_list), router_id, connection->area_id);	/* Create a database description packet from the link state summary list. */
	char* buffer 				= pack_dd_packet(dd_packet);																/* Pack the DD packet. */

	set_ackreq_bit_routinginfo(connection, buffer);

	send_packet(connection, buffer, dd_packet->common_header.length);														/* Send it. */
	dno_log("Sent DATABASE_DESCRIPTION to %s\n", connection->neighbor_ip_string);
	update_stats(DD_PACKETS_SENT);

	//sent_primary_msg_routinginfo(connection); TODO remove, replaced by ackreq bit.

	if (!HAS_MORE(dd_packet->dd_header.flags)) {
		connection->last_dd = 1;
	}

	free(buffer);																											/* Free the packet buffer. */
	free_dd_packet(dd_packet);																								/* Free the packet. */
}

void send_lsr_packet(struct connection* connection) {
	struct lsr_packet* lsr_packet 	= create_lsr_packet(&(connection->lsrq_list_pos), router_id, connection->area_id);		/* Create an LSR packet. */
	char* buffer 					= pack_lsr_packet(lsr_packet);															/* Pack the LSR packet. */

	set_ackreq_bit_routinginfo(connection, buffer);

	send_packet(connection, buffer, lsr_packet->common_header.length);														/* Send it. */
	dno_log("Sent LINK_STATE_REQUEST to %s\n", connection->neighbor_ip_string);
	update_stats(LSR_PACKETS_SENT);

	//sent_primary_msg_routinginfo(connection); TODO remove, replaced by ackreq bit

	free(buffer);																											/* Free the packet buffer. */
	free_lsr_packet(lsr_packet);																							/* Free the packet. */
}

void send_dc_packet(struct connection* connection) {
	struct dc_packet* 	dc_packet 	= create_dc_packet(router_id, connection->area_id);
	lsdb_entry_key		key;
	char* 				buffer;

	buffer = pack_dc_packet(dc_packet);																						/* This will also set the term_turn bit. */

	set_ackreq_bit_locked(buffer);

	send_packet(connection, buffer, dc_packet->common_header.length);
	dno_log("Sent DISCONNECT to %s\n", connection->neighbor_ip_string);
	update_stats(DISCONNECTS_SENT);

	//sent_primary_msg_locked(connection); TODO remove replaced by ackreq bit.

	key.advertising_router 	= router_id;
	key.link_state_id 		= router_id;
	key.link_state_type 	= ROUTER_LSA;
	struct lsdb_entry* lsdb_entry = find_lsdb_entry(key);																	/* Remove own LSA from LSDB. */
	delete_lsdb_entry(lsdb_entry);

	free(buffer);
	free(dc_packet);
}

void originate_lsa(uint32_t area_id) {
	struct lsdb_entry* 		lsa 		= create_new_lsa(area_id);
	struct lsdb_entry* 		current		= find_lsdb_entry(lsa->key);
	struct lsa_key_element 	key_elt;
	struct lsa_key_element* key_elt_ptr	= &key_elt;
	struct lsu_packet* 		lsu_packet;

	if (current) {
		delete_lsdb_entry(current);
	}

	add_lsdb_entry(lsa);

	key_elt.key 	= lsa->key;
	key_elt.next 	= NULL;
	key_elt.prev 	= NULL;

	lsu_packet = create_lsu_packet(&(key_elt_ptr), router_id, area_id);

	flood_lsu(lsu_packet, area_id, NULL);

	free_lsu_packet(lsu_packet);

	update_stats(NUMBER_OF_LSAS_GENERATED);
}

int link_compare(struct lsa_type1_link* a, struct lsa_type1_link* b) {
	if (a->link_id < b->link_id) {
		return -1;
	} else if (a->link_id == b->link_id) {
		return 0;
	} else {
		return 1;
	}
}

struct lsdb_entry* create_new_lsa(uint32_t area_id) {
	struct lsdb_entry* lsa = (struct lsdb_entry*) malloc(sizeof(struct lsdb_entry));
	struct connection* connection, *tmp;

	lsa->key.advertising_router = router_id;
	lsa->key.link_state_id		= router_id;
	lsa->key.link_state_type 	= ROUTER_LSA;

	lsa->header.age 			= INITIAL_AGE;
	lsa->header.sequence_number = next_sequence_number;
	lsa->header.number_of_links = 0;
	lsa->header.length			= LSA_HEADER_SIZE + LSDB_ENTRY_KEY_SIZE;

	lsa->links					= NULL;

	next_sequence_number++;

	HASH_ITER(hh, connections, connection, tmp) {
		if (connection->state == STATE_FULL && connection->area_id == area_id && connection->disconnected != 1) {
			struct lsa_type1_link* link = (struct lsa_type1_link*) malloc(sizeof(struct lsa_type1_link));

			link->link_id = connection->neighbor_id;
			link->metric  = 1;

			DL_APPEND(lsa->links, link);

			lsa->header.length += LSA_TYPE1_LINK_SIZE;
			lsa->header.number_of_links++;
		}
	}

	DL_SORT(lsa->links, link_compare);

	return lsa;
}

void flood_lsu(struct lsu_packet* lsu_packet, uint32_t area_id, struct connection* incoming_connection) {
	//TODO
	struct connection* connection, *tmp;
	char* buffer = pack_lsu_packet(lsu_packet);

	HASH_ITER(hh, connections, connection, tmp) {
		if (connection->state >= STATE_EXCHANGE && connection->area_id == area_id && connection != incoming_connection) {
			set_ackreq_bit_routinginfo(connection, buffer);

			send_packet(connection, buffer, lsu_packet->common_header.length);
			dno_log("Sent LINK_STATE_UPDATE to %s\n", connection->neighbor_ip_string);
			update_stats(LSU_PACKETS_SENT);

			//sent_primary_msg_routinginfo(connection); TODO, remove, replaced by ackreq bit.
		}
	}


// This code only uses the termination detection tree to flood. This probably does not work and can be removed. TODO

//	struct child* child;
//	char* buffer = pack_lsu_packet(lsu_packet);
//
//	write_lock_termination();
//
//	if ((incoming_connection == NULL || incoming_connection->neighbor_id != parent->neighbor_id) && parent->neighbor_id != 0 && parent->area_id == area_id) {
//		send_packet(parent, buffer, lsu_packet->common_header.length);
//		dno_log("Sent LINK_STATE_UPDATE to %s\n", parent->neighbor_ip_string);
//		sent_primary_msg_locked(parent);
//	}
//
//	DL_FOREACH(children, child) {
//		if (incoming_connection == NULL || incoming_connection->neighbor_id != child->router_id) {
//			struct connection* child_connection = find_connection(connections, child->router_id);
//
//			if (child_connection == NULL) {
//				dno_print("flood_lsu: error: child_connection not found.");
//				exit(EXIT_FAILURE);
//			}
//
//			if (child_connection->state >= STATE_EXCHANGE && child_connection->area_id == area_id) {
//				send_packet(child_connection, buffer, lsu_packet->common_header.length);
//				dno_log("Sent LINK_STATE_UPDATE to %s\n", child_connection->neighbor_ip_string);
//				sent_primary_msg_locked(child_connection);
//			}
//		}
//	}
//
//	unlock_termination();

	free(buffer);
}

void age_lsas() {
	struct lsdb_entry* lsdb_entry, *tmp;

	if (check_timer(&age_update_time, AGE_UPDATE_INTERVAL)) {
		gettimeofday(&age_update_time, NULL);

		write_lock_lsdb();

		HASH_ITER(hh, link_state_database, lsdb_entry, tmp) {
			age_lsdb_entry(lsdb_entry);
		}

		unlock_lsdb();
	}
}

void refresh_lsa() {
	struct area* area;

	read_lock_areas();

	if (check_timer(&lsa_refresh_time, LS_REFRESH_TIME + ls_refresh_offset)) {
		DL_FOREACH(areas, area) {
			originate_lsa(area->area_id);
		}

		gettimeofday(&lsa_refresh_time, NULL);
		ls_refresh_offset = getrandomint(1, LS_REFRESH_TIME_MAX_OFFSET);
	}

	unlock_areas();
}

void get_state_string(char* string, int state, int len) {
	int stringlen;

	memset(string, '\0', len);

	if (state == STATE_EXSTART) {
		stringlen = MIN(len-1, strlen(STATE_EXSTART_STR));
		strncpy(string, STATE_EXSTART_STR, stringlen);
	}
	if (state == STATE_EXCHANGE) {
		stringlen = MIN(len-1, strlen(STATE_EXCHANGE_STR));
		strncpy(string, STATE_EXCHANGE_STR, stringlen);
	}
	if (state == STATE_LOADING) {
		stringlen = MIN(len-1, strlen(STATE_LOADING_STR));
		strncpy(string, STATE_LOADING_STR, stringlen);
	}
	if (state == STATE_FULL) {
		stringlen = MIN(len-1, strlen(STATE_FULL_STR));
		strncpy(string, STATE_FULL_STR, stringlen);
	}
}


void disconnect() {
	//dno_print("Disconnecting");

	write_lock_dc();

	dc_msg = 1;

	unlock_dc();
}

pthread_t create_routing_information_thread() {
	int 				rv;
	pthread_attr_t 		attr;
	pthread_t 			thread_id;

	rv = pthread_attr_init(&attr);													/* Initialize and set thread detached attribute. */
	if (rv != 0) {
		dno_print("Error initializing phtread attribute.");
		exit(EXIT_FAILURE);
	}
	rv = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	if (rv != 0) {
		dno_print("Error setting pthread attribute.");
		exit(EXIT_FAILURE);
	}

	rv = pthread_create(&thread_id, &attr, routing_information_thread, NULL);		/* Create thread. */
	if (rv != 0) {
		dno_print("Error creating socket server thread.");
		exit(EXIT_FAILURE);
	}

	rv = pthread_attr_destroy(&attr);
	if (rv != 0) {
		dno_print("Error destroying phtread attribute.");
		exit(EXIT_FAILURE);
	}

	return thread_id;
}

void read_lock_dc() {
    if (pthread_rwlock_rdlock(&dc_lock) != 0) {
    	dno_print("read_lock_dc: Can't acquire readlock.\n");
    	exit(EXIT_FAILURE);
    }
}

void write_lock_dc() {
    if (pthread_rwlock_wrlock(&dc_lock) != 0) {
    	dno_print("write_lock_dc: Can't acquire writelock.\n");
    	exit(EXIT_FAILURE);
    }
}

void unlock_dc() {
	if(pthread_rwlock_unlock(&dc_lock) != 0) {
		dno_print("Error unlocking dc_lock.\n");
		exit(EXIT_FAILURE);
	}
}
