/*
 * dno.h
 *
 *  Created on: Oct 3, 2010
 *      Author: mr
 */

#ifndef DNO_H_
#define DNO_H_

#include <signal.h>
#include <stdint.h>

#define	EXIT_PROGRAM			"exit"
#define HELP					"help"
#define ALL_ROUTERS				"all"
#define PRINT_AREAS 			"areas"
#define PRINT_CONNECTIONS 		"connections"
#define PRINT_R_CONNECTIONS		"rconnections"
#define PRINT_ROUTING_TABLE		"table"
#define PRINT_LSDB				"lsdb"
#define PRINT_LSRS				"lsrs"
#define PRINT_PARENT_AND_CHILDREN "pac"
#define PRINT_TERM_STATUS		"term"
#define SEND					"send"
#define PRINT_DATA_SENT			"data"
#define PRINT_STATS				"stats"

#define DENSITY				2															/* Dictates the density of the connections made within one area. */

#define HELP_STRING			"The following commands are available:\n"								\
							"\t<hostname> areas\t: print the areas of this host\n"				\
							"\t<hostname> connections\t: print the connections of this host used for routing algorithm\n" \
							"\t<hostname> rconnections\t: print the the connections used for forwarding data\n" \
							"\t<hostname> table\t: print the routing table of this host\n"			\
							"\t<hostname> lsdb\t: print the contents of the link-state database\n" \
							"\t<hostname> lsrs\t: print the link-state request list for each connection\n" \
							"\t<hostname> pac\t: print the parent and children\n" \
							"\t<hostname> term\t: print the variables related to the termination algorithm\n" \
							"\t<hostname> send\t: use "send <ip_address> <number_of_gbytes>" to send a number of GBs to another node in the overlay\n" \
							"\t<hostname> data\t: print statistics about data tranfers\n" \
							"\t<hostname> stats\t: print stats related to OSPF and the termination algorithm\n" \
							"\t<hostname> NOTE: all these commands have to be preceded by either "all" or a hostname to indicate by which nodes the command should be executed\n" \
							"\t<hostname> EXAMPLE: "node1 connections" will cause node1 to print its connections, "all connections" will cause all nodes to print their connections\n" \
							"\texit\t\t\t: exit program\n"

extern volatile sig_atomic_t 	run;
extern uint32_t 				router_id;												/* The ID uniquely identifying this router in the overlay. */

#endif /* DNO_H_ */
