/*
 * socksvr.c
 *
 *  Created on: May 10, 2010
 *      Author: mritman
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#include <pthread.h>

#include "socksvr.h"
#include "utils.h"
#include "connections.h"
#include "dno.h"
#include "latency.h"
#include "area.h"

int setup_sock_server_socket(char* ipstr, int* port) {
	int 				rv;
	int 				yes = 1;
	struct addrinfo 	hints, *servinfo, *p;
	char 				sock_svr_portstring[5];
	int 				sock_svr_fd;

	sprintf(sock_svr_portstring, "%d", SOCK_SVR_PORT);				/* Convert the SOCK_SVR_PORT to a string. */

	memset(&hints, 0, sizeof hints); 								/* Make sure the struct is empty. */
	hints.ai_family 	= AF_UNSPEC;     							/* Don't care IPv4 or IPv6 */
	hints.ai_socktype 	= SOCK_STREAM; 								/* TCP stream sockets. */
	hints.ai_flags 		= AI_PASSIVE;    							/* Fill in my IP for me. */

	rv = getaddrinfo(hostname, sock_svr_portstring, &hints, &servinfo);
	if (rv != 0) {
	    fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(rv));
	    exit(EXIT_FAILURE);
	}

    /* Loop through all the results and bind to the first we can. */
    for(p = servinfo; p != NULL; p = p->ai_next) {
    	sock_svr_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
    	if (sock_svr_fd == -1) {
            dno_print_error("setup_sock_server_socket: socket");
            continue;
        }

		rv = setsockopt(sock_svr_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
        if (rv == -1) {
            dno_print_error("setup_sock_server_socket: setsockopt");
            exit(EXIT_FAILURE);
        }

        rv = bind(sock_svr_fd, p->ai_addr, p->ai_addrlen);
        if (rv == -1) {
			close(sock_svr_fd);
			dno_print_error("bind");
			continue;
        }

        break;														/* Break if the bind was successful. */
    }

    if (p == NULL)  {
        dno_print("setup_sock_server_socket: failed to bind\n");
        exit(EXIT_FAILURE);
    }

    get_ipstr_and_port_from_addrinfo(p, ipstr, port);

    freeaddrinfo(servinfo);

    rv = listen(sock_svr_fd, BACKLOG);
    if (rv == -1) {
        dno_print_error("setup_sock_server_socket: listen");
        exit(EXIT_FAILURE);
    }

    router_id = get_router_id(ipstr);

    return sock_svr_fd;
}

void create_rtts_server_thread(int new_socket) {
	int 				rv;
	pthread_attr_t 		attr;
	pthread_t 			thread_id;
	struct arg_holder* 	arguments;

	rv = pthread_attr_init(&attr);																	/* Initialize and set thread detached attribute. */
	if (rv != 0) {
		dno_print("create_rtts_server_thread: Error initializing pthread attribute.");
		exit(EXIT_FAILURE);
	}
	rv = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	if (rv != 0) {
		dno_print("create_rtts_server_thread: Error setting pthread attribute.");
		exit(EXIT_FAILURE);
	}

	arguments = (struct arg_holder*) malloc(sizeof(struct arg_holder));								/* Set up thread arguments. */
	arguments->fd = new_socket;

	rv = pthread_create(&thread_id, &attr, serve_latencys, (void *) arguments);						/* Create thread. */
	if (rv != 0) {
		dno_print("Error creating socket server thread.");
		exit(EXIT_FAILURE);
	}
	rv = pthread_attr_destroy(&attr);
	if (rv != 0) {
		dno_print("create_rtts_server_thread: Error destroying phtread attribute.");
		exit(EXIT_FAILURE);
	}
}

void create_connection_accept_thread(int new_socket, struct sockaddr_storage their_addr) {
	int 				rv;
	pthread_attr_t 		attr;
	pthread_t 			thread_id;
	struct arg_holder* 	arguments;

	/* Initialize and set thread detached attribute */
	rv = pthread_attr_init(&attr);
	if (rv != 0) {
		dno_print("create_connection_accept_thread: Error initializing pthread attribute.");
		exit(EXIT_FAILURE);
	}
	rv = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	if (rv != 0) {
		dno_print("create_connection_accept_thread: Error setting pthread attribute.");
		exit(EXIT_FAILURE);
	}

	/* Create thread */
	arguments 				= (struct arg_holder*) malloc(sizeof(struct arg_holder));
	arguments->fd 			= new_socket;
	arguments->their_addr 	= their_addr;

	rv = pthread_create(&thread_id, &attr, serve_connection, (void *) arguments);
	if (rv != 0) {
		dno_print("create_connection_accept_thread: Error creating connection server thread.");
		exit(EXIT_FAILURE);
	}
	rv = pthread_attr_destroy(&attr);
	if (rv != 0) {
		dno_print("create_connection_accept_thread: Error destroying phtread attribute.");
		exit(EXIT_FAILURE);
	}
}

void *serve_sockets(void *args) {
	int 					rv;
	struct arg_holder*		arguments;
	int 					sock_svr_fd;
	int 					new_socket;
	int 					bytesread;
	struct sockaddr_storage their_addr;
	socklen_t 				sin_size;
	uint16_t 				request_nbo;
	uint16_t 				request;
	fd_set 					fd_read_set;
	struct timeval 			timeout;

	arguments 	= (struct arg_holder*) args;
	sock_svr_fd = arguments->fd;
	free(arguments);

	/* Accept connections while the program is running */
	while(run) {
		timeout.tv_sec 	= 1;
		timeout.tv_usec = 0;

		FD_ZERO(&fd_read_set); 												/* Clear the fd_read_set */
		FD_SET(sock_svr_fd, &fd_read_set);

		rv = select(sock_svr_fd+1, &fd_read_set, NULL, NULL, &timeout);
		if (rv < 0) {
			dno_print_error("serve_sockets: select failed");
			exit(EXIT_FAILURE);
		}

		if (!FD_ISSET(sock_svr_fd, &fd_read_set)) {
			continue;
		}

		/* Accept a new connection */
		sin_size 	= sizeof their_addr;
		new_socket 	= accept(sock_svr_fd, (struct sockaddr *) &their_addr, &sin_size);
		if (new_socket == -1) {
			dno_print_error("accept");
			break; // TODO change to continue and handle errors seperately
		}

	    bytesread = recv(new_socket, &request_nbo, sizeof(uint16_t), 0);
	    if (bytesread == -1) {
	        dno_print_error("serve_sockets: recv");
	        exit(EXIT_FAILURE);
	    }

	    request = ntohs(request_nbo);
	    if(request == REQUEST_RTTS) {
	    	create_rtts_server_thread(new_socket);
	    } else if (request == REQUEST_CONN) {
	    	create_connection_accept_thread(new_socket, their_addr);
	    } else {
	    	dno_print("serve_sockets: invalid request received");
	    	exit(EXIT_FAILURE);
	    }

		sched_yield();
	}

	close(sock_svr_fd);
	pthread_exit(NULL);
}

pthread_t create_socket_svr_thread(int sock_svr_fd) {
	int 				rv;
	pthread_attr_t 		attr;
	pthread_t 			thread_id;
	struct arg_holder* 	arguments;

	/* Initialize and set thread detached attribute */
	rv = pthread_attr_init(&attr);
	if (rv != 0) {
		dno_print("Error initializing phtread attribute.");
		exit(EXIT_FAILURE);
	}
	rv = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	if (rv != 0) {
		dno_print("Error setting pthread attribute.");
		exit(EXIT_FAILURE);
	}

	/* Create thread */
	arguments = (struct arg_holder*) malloc(sizeof(struct arg_holder));
	arguments->fd = sock_svr_fd;

	rv = pthread_create(&thread_id, &attr, serve_sockets, (void *) arguments);
	if (rv != 0) {
		dno_print("Error creating socket server thread.");
		exit(EXIT_FAILURE);
	}

	rv = pthread_attr_destroy(&attr);
	if (rv != 0) {
		dno_print("Error destroying phtread attribute.");
		exit(EXIT_FAILURE);
	}

	return thread_id;
}
