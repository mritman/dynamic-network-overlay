/*
 * connections.h
 *
 *  Created on: May 24, 2010
 *      Author: mritman
 */

#ifndef CONNECTIONS_H_
#define CONNECTIONS_H_

#include "packettypes.h"
#include "area.h"
#include "uthash.h"

extern struct connection* 	connections;
extern pthread_rwlock_t 	connections_lock;

#define DEFAULT_COST		1
#define MIN_TCP_PORT		49152
#define MAX_TCP_PORT		65535
#define MAX_PORT_RETRYS		10											/* The number of times a random port is tried if the default port SOCK_SVR_PORT is in use. */

/* Corresponds to the neighbor structure in OSPF. */
struct connection {
	int 						key;									/* Same as neighbor_id. */

	uint32_t					neighbor_id;							/* The router id of the neighbor. */
	char 						neighbor_ip_string[INET6_ADDRSTRLEN]; 	/* The ip string of the router on the other side of the connection. String big enough to hold either ip4 or ip6 address. */
	uint16_t 					neighbor_port;							/* The port that the router on the other side of the connection is listening on. */
	int 						fd;										/* The file descriptor for the data connection. */
	int							area_id;								/* The area this connection belongs to. */
	int							cost;									/* The outgoing cost for packets transmitted through this connection. */

	int							state;									/* The functional state of the connection. See a description of the OSPF interface state machine for more information. */
	int							fd_info;								/* The file descriptor used by the routing information algorithm. */

//	int							master;									/* 1 if the neighbor will control the database synchronization process. */
//	int							dd_seq_nr;								/* Database description packet sequence number. */

	int							dd_done;								/* Indicates whether the last dd packet has been received from the neighbor. */

	struct lsa_header_element* 	database_summary_list;					/* This component is the list of LSAs to be sent to the neighbor in Database Description packets during database synchronization. These LSAs are a snapshot of the database when the router goes into the exchange state. */
	int							last_dd;								/* Indicates whether the last dd_packet has been sent to the neighbor. */

	struct lsa_key_element*		link_state_request_list;				/* This list records LSAs from the neighbor's Database Description packets which are more recent than the LSAs in the link state database. */
	struct lsa_key_element*		lsrq_list_pos;							/* Points to the next lsa_key in the link-state request list from where the next lsr_packet will be built. */

	int 						disconnected;							/* Indicates whether this neighbor will disconnect after next termination. */

	/* Fields related to reading incoming packets. */
	int					 		in_progress;							/* 1 if the header of the packet has been read and the rest of the packet is being read. */
	int					 		packet_ready;							/* 1 if a packet is fully read into the buffer. */
	struct common_header 		header;									/* The header of the incoming packet. */
	char				 		header_buffer[COMMON_HEADER_SIZE];		/* The buffer to hold the header of the incoming packet. */
	char* 				 		buffer;									/* The buffer to read the incoming packet into. */
	int					 		pos;									/* The current position in the buffer. */

	/* Fields related to termination detection. */
	uint32_t					has_parent;								/* Indicates whether this router has a parent in the termination detection context. */

	UT_hash_handle				hh;										/* Required for uthash */
};

void	connections_init();

int 	calculate_key(char* ipstr, uint16_t port);

int 	remove_connection(int key);

int 	remove_connection_unlocked(int key);

struct connection* find_connection(struct connection* connections_table, int key);

void	establish_connections();

void* 	serve_connection(void *arg);

void 	select_connections(double d);

void 	close_connections();

void 	print_connections();

void 	print_lsrs();

void 	read_lock_connections();

void 	write_lock_connections();

void	unlock_connections();

#endif /* CONNECTIONS_H_ */
