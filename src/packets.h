/*
 * packets.h
 *
 *  Created on: Dec 16, 2010
 *      Author: mritman
 */

#ifndef PACKETS_H_
#define PACKETS_H_

#include <stdint.h>

#include "packettypes.h"
#include "connections.h"

extern unsigned char termination_turn;

int 				pack_int32_t(uint32_t int32, char* buffer, int offset);
uint32_t 			unpack_int32_t(char* buffer, int* offset);
int 				read_bytes(int fd, char* buffer, int pos, int length);

struct dd_packet* 	create_dd_packet(struct lsa_header_element** summary_list, uint32_t router_id, uint32_t area_id);
struct lsr_packet* 	create_lsr_packet(struct lsa_key_element** request_list, uint32_t router_id, uint32_t area_id);
struct lsu_packet* 	create_lsu_packet(struct lsa_key_element** lsa_keys, uint32_t router_id, uint32_t area_id);
struct dc_packet* 	create_dc_packet(uint32_t router_id, uint32_t area_id);

char* 				pack_dd_packet(struct dd_packet* dd_packet);
char* 				pack_lsr_packet(struct lsr_packet* lsr_packet);
char* 				pack_lsu_packet(struct lsu_packet* lsu_packet);
char* 				pack_dc_packet(struct dc_packet* dc_pacet);

int 				unpack_common_header(struct common_header* header, char* buffer, int offset);
struct dd_packet* 	unpack_dd_packet(char* buffer);
struct lsr_packet* 	unpack_lsr_packet(char* buffer);
struct lsu_packet* 	unpack_lsu_packet(char* buffer);
struct dc_packet* 	unpack_dc_packet(char* buffer);

void 				free_lsa_keys(struct lsa_key_element* lsa_keys);
void 				free_lsa_headers(struct lsa_header_element* lsa_headers);
void 				free_dd_packet(struct dd_packet* dd_packet);
void 				free_lsr_packet(struct lsr_packet* lsr_packet);
void 				free_lsu_packet(struct lsu_packet* lsu_packet);

void			 	read_packet(struct connection* connection);
void				reset_packet_fields(struct connection* connection);
void 				send_packet(struct connection* connection, char* buffer, int length);

void 				print_dd_packet(struct dd_packet* dd_packet);
void 				print_lsr_packet(struct lsr_packet* lsr_packet);
void 				print_lsu_packet(struct lsu_packet* lsu_packet);

struct termination_packet* 	create_termination_packet(uint32_t router_id, uint32_t area_id, uint8_t type, uint32_t router_id_1, uint32_t router_id_2);
char* 						pack_termination_packet(struct termination_packet* termination_packet);
struct termination_packet* 	unpack_termination_packet(char* buffer);

#endif /* PACKETS_H_ */
