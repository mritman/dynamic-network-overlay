/*
 * utils.h
 *
 *  Created on: May 10, 2010
 *      Author: mritman
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include "area.h"

#define MAX_HOSTNAME_LENGTH		1023

#define GXP_WRITE_FD        	3						/* This file descriptor is shared by all GXP processes */
#define GXP_READ_FD				4						/* This file descriptor is shared by all GXP processes */
#define GXP_NUM_EXECS			"GXP_NUM_EXECS"			/* The number of GXP processes */
#define GXP_EXEC_IDX			"GXP_EXEC_IDX"			/* The ID of this GXP process */

#define dno_print(...)			dno_printf(1, __VA_ARGS__)
#define dno_print_debug(...)	dno_print_dbg(__FILE__, __LINE__, __FUNCTION__, 1, __VA_ARGS__)
#define dno_log(...)			dno_log_priv(__VA_ARGS__)

#define MIN(X,Y) 				((X) < (Y) ? (X) : (Y))

#define LOG 					0						/* Log locking information. */
#define LOGGING					0						/* Log OSPF packet information. */

/* When creating a new thread, all arguments must be passed by reference and cast to (void *).
 * The arg_holder struct can be used to pass multiple parameters to a new thread.
 */
struct arg_holder {
	int 					fd;
	struct sockaddr_storage their_addr;
};

/* 'hostname' is used among multiple threads but not protected.
 * It should be written to only once at the beginning of the main function,
 * before any threads are created, by calling get_hostname.
 * After that, simultaneous reads by multiple threads should not lead to any errors.
 */
extern int	gxp_id;
extern char hostname[MAX_HOSTNAME_LENGTH+1];

void		init_utils();

void 		get_hostname(char* hostname, int max_hostname_length);

int 		get_nr_of_gxp_processes();

int 		get_gxp_id();

void 		synchronize();

void 		write_position(int position);

int 		read_position();

void 		setrandseed();

int 		getrandomint(int min, int max);

double 		logbase(double a, double base);

int 		check_timer(struct timeval* timestamp, int timeout);

int 		check_timer_sec(int timestamp_sec, int timeout);

void 		set_start_time();

void 		get_time_elapsed(struct timeval* time);

void 		get_interval(struct timeval* time_start, struct timeval* time_stop, struct timeval* result);

void 		get_ipstr_and_port_from_addrinfo(struct addrinfo *p, char* ipstr, int* port);

void 		get_ipstr_and_port_from_sockaddr_storage(struct sockaddr_storage* p, char* ipstr, int* port);

void 		convert_ip_ntop(int ip, char* ipstr);

void 		dno_printf(int flush, const char* format, ...);

void 		dno_print_error(const char* format, ...);

void 		dno_print_dbg(const char *file, int line, const char *function, int flush, const char* format, ...);

void 		dno_log_priv(const char* format, ...);

int 		connect_to_router(struct router* router, int type);

uint32_t 	get_router_id(char* ipstr);

int 		is_readable(int fd);

#endif /* UTILS_H_ */
