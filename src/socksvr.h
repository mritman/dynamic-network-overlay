/*
 * socksvr.h
 *
 *  Created on: May 10, 2010
 *      Author: mritman
 */

#ifndef SOCKSVR_H_
#define SOCKSVR_H_

#define REQUEST_CONN 		0
#define REQUEST_RTTS		1
#define SOCK_SVR_PORT		52710 				/* The default port the socket server will listen on. */
#define BACKLOG				50					/* The maximum number of connections queued by listen. */

int 		setup_sock_server_socket(char* ipstr, int* port);

pthread_t 	create_socket_svr_thread(int sock_svr_fd);

void		*serve_sockets(void *arg);

#endif /* SOCKSVR_H_ */
