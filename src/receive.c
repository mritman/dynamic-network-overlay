/*
 * receive.c
 *
 *  Created on: Apr 22, 2011
 *      Author: mritman
 */

#include <stdlib.h>
#include <sys/time.h>

#include "utils.h"
#include "receive.h"
#include "routingio.h"
#include "dno.h"

long 	bytes_received_total = 0;
int 	bytes_received_interval = 0;

void* receive_thread(void* args) {
	int 	rv;
	char 	buffer[PACKET_SIZE];
	struct	timeval 	measure_timer;
	struct 	timespec 	ts;

	ts.tv_sec  = 0;
	ts.tv_nsec = 10000;

	gettimeofday(&measure_timer, NULL);

	while (run) {
		rv = dno_recv(buffer, PACKET_SIZE);

		if (rv >= 1) {
			bytes_received_total += rv;
			bytes_received_interval += rv;
		} else if (rv == 0) {
			rv = nanosleep(&ts, NULL);
			if (rv == -1) {
				dno_print_error("receive_thread");
			}
			continue;
		}

		if (check_timer(&measure_timer, 1)) {
			char unit[1024];
			double total_bits = (double) bytes_received_interval * 8;
			double bandwidth;

			memset(unit, '\0', 1024);

			if (total_bits < 1000) {
				bandwidth = total_bits;
				strncpy(unit, "b/s\0", 4);
			} else if (total_bits >= 1000 && total_bits < 1000000) {
				bandwidth = total_bits / 1000;
				strncpy(unit, "kb/s\0", 5);
			} else if (total_bits >= 1000000 && total_bits < 1000000000) {
				bandwidth = total_bits / 1000000;
				strncpy(unit, "Mb/s\0", 5);
			} else if (total_bits >= 1000000000) {
				bandwidth = total_bits / 1000000000;
				strncpy(unit, "Gb/s\0", 5);
			}

			//dno_print("Receiving at: %6.2f %s\n", bandwidth, unit);

			gettimeofday(&measure_timer, NULL);
			bytes_received_interval = 0;
		}
	}

	pthread_exit(NULL);
}

//void fill_buffer(char* buffer) {
//	int pos = sizeof(int) * 2;
//	int count = 0;
//	int src;
//
//	while (pos != PACKET_SIZE) {
//		count++;
//		src = htonl(count);
//
//		memcpy(buffer+pos, &src, sizeof(int));
//		pos += sizeof(int);
//
//	}
//}

//int check_buffer(char* buffer) {
//	int pos = sizeof(int) * 2;
//	int count = 0;
//	int current;
//
//	while (pos != PACKET_SIZE) {
//		count++;
//		memcpy(&current, buffer+pos, sizeof(int));
//		pos += sizeof(int);
//
//		current = ntohl(current);
//		if (current != count) {
//			return 0;
//		}
//	}
//
//	return count;
//}

pthread_t create_receive_thread() {
	int 			rv;
	pthread_attr_t 	attr;
	pthread_t 		thread_id;

	/* Initialize and set thread detached attribute */
	rv = pthread_attr_init(&attr);
	if (rv != 0) {
		dno_print("create_receive_thread: Error initializing phtread attribute.");
		exit(EXIT_FAILURE);
	}
	rv = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	if (rv != 0) {
		dno_print("create_receive_thread: Error setting pthread attribute.");
		exit(EXIT_FAILURE);
	}

	/* Create thread */
	rv = pthread_create(&thread_id, &attr, receive_thread, NULL);
	if (rv != 0) {
		dno_print("create_receive_thread: Error creating routing_io thread.");
		exit(EXIT_FAILURE);
	}

	rv = pthread_attr_destroy(&attr);
	if (rv != 0) {
		dno_print("create_receive_thread: Error destroying phtread attribute.");
		exit(EXIT_FAILURE);
	}

	return thread_id;
}
