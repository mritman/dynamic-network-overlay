/*
 * graph.h
 *
 *  Created on: Mar 1, 2011
 *      Author: mritman
 */

#ifndef GRAPH_H_
#define GRAPH_H_

#include <stdint.h>

#define UP 		1
#define DOWN 	2

struct edge {
	uint32_t	dst;
	int 		label;
	int			dist;

	struct edge* prev;
	struct edge* next;
};

struct node {
	uint32_t		router_id;
	int				rank;

	int 			id;

	uint32_t 		dist;
	struct node*	previous;

	struct edge* 	edges;

	int 			done;

	struct node*	prev;
	struct node*	next;
};

struct graph {
	struct node* nodes;
};

struct graph* construct_graph();
struct node* find_clone(struct node* nodes, struct node* node);
void print_graph(struct graph* graph);
void free_graph(struct graph* graph);

#endif /* GRAPH_H_ */
