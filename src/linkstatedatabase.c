/*
 * linkstatedatabase.c
 *
 *  Created on: Dec 28, 2010
 *      Author: mritman
 */

#include <pthread.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <sys/time.h>

#include "linkstatedatabase.h"
#include "utils.h"
#include "routinginfo.h"

struct lsdb_entry* 	link_state_database = NULL;
pthread_rwlock_t 	lsdb_lock;

void lsdb_init() {
	int rv;

    rv = pthread_rwlock_init(&lsdb_lock,NULL);																/* Initialize the lock used for shared variable link_state_database. */
	if (rv != 0) {
		dno_print_error("Lock init failed on lsdb_lock");
		exit(EXIT_FAILURE);
    }
}

struct lsdb_entry* new_lsdb_entry() {
	struct lsdb_entry* new_entry = (struct lsdb_entry*) malloc(sizeof(struct lsdb_entry));
	memset(new_entry, 0, sizeof(struct lsdb_entry));														/* Required for structure keys used with uthash. */

	return new_entry;
}

void add_lsdb_entry(struct lsdb_entry* new_entry) {
    write_lock_lsdb();
    gettimeofday(&new_entry->timestamp, NULL);
    HASH_ADD(hh, link_state_database, key, sizeof(lsdb_entry_key), new_entry);
    unlock_lsdb();
}

int body_has_changed(struct lsdb_entry* current, struct lsa_type1* lsa) {
	struct lsa_type1_link* curr_link, *lsa_link;
	int curr_length = 0;
	int lsa_length = 0;
	int i;

	DL_FOREACH(current->links, curr_link) {
		curr_length++;
	}
	DL_FOREACH(lsa->links, lsa_link) {
		lsa_length++;
	}
	if (curr_length != lsa_length) {
		return 1;
	}

	curr_link = current->links;
	lsa_link = lsa->links;

	for(i = 0; i < curr_length; i++) {
		if (curr_link->link_id != lsa_link->link_id) {
			return 1;
		} else if (curr_link->metric != lsa_link->metric) {
			return 1;
		}

		curr_link = curr_link->next;
		lsa_link = lsa_link->next;
	}

	return 0;
}

int has_changed(struct lsdb_entry* current, struct lsa_type1* lsa) {
	int changed = 0;

	if (!current) {
		return 1;
	}

	read_lock_lsdb();
	if ((current->header.age == MAX_AGE && lsa->header.age != MAX_AGE) || (current->header.age != MAX_AGE && lsa->header.age == MAX_AGE)) {
		changed = 1;
	} else if (lsa->header.length != current->header.age) {
		changed = 1;
	} else if (body_has_changed(current, lsa)) {
		changed = 1;
	}
	unlock_lsdb();

	return changed;
}

int update_lsdb(struct lsa_type1* lsa) {
	struct lsdb_entry* current = find_lsdb_entry(lsa->key);
	struct lsa_type1_link* link;
	int changed = 0;

	changed = has_changed(current, lsa);

	if (current) {
		delete_lsdb_entry(current);
	}

	struct lsdb_entry* new_entry = (struct lsdb_entry*) malloc(sizeof(struct lsdb_entry)); //create lsdb_entry
	new_entry->key		= lsa->key;
	new_entry->header 	= lsa->header;
	new_entry->links	= NULL;

	DL_FOREACH(lsa->links, link) {
		struct lsa_type1_link* link_copy = (struct lsa_type1_link*) malloc(sizeof(struct lsa_type1_link));
		link_copy->link_id 	= link->link_id;
		link_copy->metric	= link->metric;

		DL_APPEND(new_entry->links, link_copy);
	}

	add_lsdb_entry(new_entry);

	return changed;
}

struct lsdb_entry* find_lsdb_entry(lsdb_entry_key key) {
	struct lsdb_entry* result;

    read_lock_lsdb();
    HASH_FIND(hh, link_state_database, &key, sizeof(lsdb_entry_key), result);
    unlock_lsdb();

    return result;
}

int in_lsdb(uint32_t router_id) {
	lsdb_entry_key key;
	key.advertising_router 	= router_id;
	key.link_state_id 		= router_id;
	key.link_state_type 	= ROUTER_LSA;

	struct lsdb_entry* entry = NULL;
	entry = find_lsdb_entry(key);

	if (entry != NULL) {
		return 1;
	} else {
		return 0;
	}
}

void age_lsdb_entry(struct lsdb_entry* entry) {
	struct timeval current_time;
	struct timeval interval;

	gettimeofday(&current_time, NULL);
	get_interval(&entry->timestamp, &current_time, &interval);

	entry->header.age += interval.tv_sec;
	if (entry->header.age > MAX_AGE) entry->header.age = MAX_AGE;

	gettimeofday(&entry->timestamp, NULL);
}

int is_newer(lsdb_entry_key key, struct lsa_header header) {
	struct lsdb_entry* current = find_lsdb_entry(key);
	int new_sequence_nr;
	int new_age;
	int current_sequence_nr;
	int current_age;

	if (current == NULL && header.age != MAX_AGE) {														/* If there is no instance of this LSA in the database the LSA the header_elt belongs to is newer. */
		return 1;
	} else if (current == NULL && header.age == MAX_AGE) {
		return 0;
	}

	read_lock_lsdb();																					/* Read-lock the link-state database. */
	current_sequence_nr = current->header.sequence_number;
	current_age			= current->header.age;
	unlock_lsdb();																						/* Unlock the link-state database. */

	new_sequence_nr = header.sequence_number;
	new_age			= header.age;

	if (new_sequence_nr > current_sequence_nr) {														/* If the header_elt's sequence number is higher it is newer. */
		return 1;
	} else if (current_sequence_nr > new_sequence_nr) {													/* Else if the current LSA's sequence number is higher, it is newer. */
		return 0;
	}

	/* If this section is reached the sequence number are the same. */

	if (new_age == MAX_AGE && current_age != MAX_AGE) {													/* If only header_elt's age is MAX_AGE it is considered newer. */
		return 1;
	} else if (new_age != MAX_AGE && current_age == MAX_AGE) {											/* If only the current LSA's age is MAX_AGE it is considered newer. */
		return 0;
	}

	/*
	 * TODO ospf seems to consider two lsas the same if the difference in age is not bigger than MAX_AGE_DIFF.
	 * This value is related to the possibility that the age of the exact same LSA may disperse as it is flooded throughout the network.
	 * Most of this time is accounted for by the LSAs sitting on router output queues (and therefore not aging) during the flooding process.
	 * It might be necessary to change this value because the overlay network has different properties from the networks OSPF is usually used in.
	 * However, normally a router will not send out two different instances with the same sequence number. This means that this situation will
	 * likely only occur when a router for some reason has lost track of the sequence number it was using and reuses it. In this case
	 * the LSA with the lower age is considered newer.
	 */


	if (abs(new_age - current_age) > MAX_AGE_DIFF) {													/* If the difference in age is bigger than MAX_AGE_DIFF the LSA with the lowest age is newer. */
		if (new_age < current_age) {
			return 1;
		} else {
			return 0;
		}
	}

	return 0;																						/* The LSA's are considered the same. */
}

void free_lsa_links(struct lsa_type1_link* links) {
	struct lsa_type1_link *elt, *tmp;

    DL_FOREACH_SAFE(links, elt, tmp) {
    	DL_DELETE(links, elt);
    	free(elt);
    }
}

void delete_lsdb_entry_priv(struct lsdb_entry* entry) {
	HASH_DEL(link_state_database, entry);
    free_lsa_links(entry->links);
    free(entry);
}

void delete_lsdb_entry(struct lsdb_entry* entry) {
	if (entry == NULL) return;

    write_lock_lsdb();
    delete_lsdb_entry_priv(entry);
    unlock_lsdb();
}

/* See section 14 of RFC 2328. */
/*
 *  TODO A MaxAge LSA must be removed immediately from the router's link
 *  state database as soon as both a) it is no longer contained on any
 *  neighbor Link state retransmission lists and b) none of the router's
 *  neighbors are in states Exchange or Loading.
 *
 */
void purge_expired_lsas() {
	struct lsdb_entry* lsdb_entry, *tmp;

	write_lock_lsdb();

	HASH_ITER(hh, link_state_database, lsdb_entry, tmp) {
		if (lsdb_entry->header.age >= MAX_AGE) {
			delete_lsdb_entry_priv(lsdb_entry);
		}
	}

	unlock_lsdb();
}

void free_link_state_database() {
	struct lsdb_entry *current_entry, *tmp;

    write_lock_lsdb();
    HASH_ITER(hh, link_state_database, current_entry, tmp) {
		HASH_DEL(link_state_database, current_entry);
		free_lsa_links(current_entry->links);
		free(current_entry);
	}

    link_state_database = NULL;

    unlock_lsdb();
}

/* Sorts the link state database by link state id. */
int database_sort(struct lsdb_entry* a, struct lsdb_entry* b) {
	char astr[INET6_ADDRSTRLEN];
	char bstr[INET6_ADDRSTRLEN];
	int n;

	convert_ip_ntop(a->key.link_state_id, astr);
	convert_ip_ntop(b->key.link_state_id, bstr);

	n = strlen(astr) < strlen(bstr) ? strlen(bstr) : strlen(astr);

    return strncmp(astr, bstr, strlen(astr));
}

void print_link_state_database(int full) {
	struct lsdb_entry* 	current_entry;
	struct lsa_header*	header;
	lsdb_entry_key*		key;
	int 				entry_nr = 1;

    write_lock_lsdb();

	dno_printf(0, "========== Printing link-state database ==========");

    if(link_state_database == NULL) {
    	dno_printf(0, "\tEmpty link-state database.\n");
    	dno_printf(1, "==================================================");
    	unlock_lsdb();
    	return;
    }

    HASH_SORT(link_state_database, database_sort);

    for(current_entry = link_state_database; current_entry != NULL; current_entry = current_entry->hh.next) {
       	char link_state_id[INET6_ADDRSTRLEN];
        char advertising_router[INET6_ADDRSTRLEN];

    	header 	= &(current_entry->header);
    	key		= &(current_entry->key);

    	inet_ntop(AF_INET, &(key->link_state_id), link_state_id, INET6_ADDRSTRLEN);
    	inet_ntop(AF_INET, &(key->advertising_router), advertising_router, INET6_ADDRSTRLEN);

    	dno_printf(0, "%4d: \tType: %1d \tLink-state ID: %15s \tAdvertising Router: %15s \tAge: %5d \tSequence number: %10d\n", entry_nr, key->link_state_type, link_state_id, advertising_router, header->age, header->sequence_number);

    	if (full) {
    		int 					link_nr = 1;
    		struct lsa_type1_link* 	link;

    		DL_FOREACH(current_entry->links, link) {
    			char link_id[INET6_ADDRSTRLEN];

    			inet_ntop(AF_INET, &(link->link_id), link_id, INET6_ADDRSTRLEN);

    			dno_printf(0, "\t\t%2d: \tLink-id: %15s \tCost: %5d\n", link_nr, link_id, link->metric);

    			link_nr++;
    		}
    	}

        entry_nr++;
    }

    dno_printf(1, "==================================================");

    unlock_lsdb();
}

struct lsa_header_element* get_database_summary_list() {
	struct lsa_header_element* 	database_summary_list = NULL;
	struct lsa_header_element*	new_element;
	struct lsdb_entry* 			current_entry;

	read_lock_lsdb();

	for(current_entry = link_state_database; current_entry != NULL; current_entry = current_entry->hh.next) {
		if (current_entry->header.age != MAX_AGE) {
			new_element = (struct lsa_header_element*) malloc(sizeof(struct lsa_header_element));

			new_element->key = current_entry->key;
			memcpy(&(new_element->header), &(current_entry->header), sizeof(struct lsa_header));

			DL_APPEND(database_summary_list, new_element);
		}
	}

    unlock_lsdb();

    return database_summary_list;
}

void read_lock_lsdb() {
    if (pthread_rwlock_rdlock(&lsdb_lock) != 0) {
    	dno_print("read_lock_lsdb: Can't acquire readlock.\n");
    	exit(EXIT_FAILURE);
    }
}

void write_lock_lsdb() {
    if (pthread_rwlock_wrlock(&lsdb_lock) != 0) {
    	dno_print("write_lock_lsdb: Can't acquire writelock.\n");
    	exit(EXIT_FAILURE);
    }
}

void unlock_lsdb() {
	if(pthread_rwlock_unlock(&lsdb_lock) != 0) {
		dno_print("Error unlocking lsdb_lock.\n");
		exit(EXIT_FAILURE);
	}
}
