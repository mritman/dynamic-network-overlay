/*
 * termination.c
 *
 *  Created on: Mar 1, 2011
 *      Author: mritman
 */

#include <pthread.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>

#include "dno.h"
#include "termination.h"
#include "utlist.h"
#include "utils.h"
#include "packets.h"
#include "connections.h"
#include "routingtable.h"
#include "routingio.h"
#include "routinginfo.h"
#include "linkstatedatabase.h"
#include "stats.h"

/* These variables should only be written to by the routinginfo thread.
 * This way it is not necessary to lock them when either thread wants to read,
 * or when the routinginfo thread wants to write to them.
 */
int terminate 		= 1;							/* Terminate is one after a change to the overlay has been detected and is set to 0 after termination has been received. It is initialized to one because a router can only start sending packets after it has connected and synched to all its peer routers. */
int inactive 		= 0;							/* A router is active if it has either not sent any STOP messages to its parent, or if it has not sent a STOP after the last RESUME, otherwise, it is inactive. The root router is active until it signals termination, after which it becomes inactive. */

/* These variable do need a lock to protect it. Both threads can read and write to it. */
int idle			= 0;							/* A router is idle only if all send and receive buffers of every routing_connection are empty and all connections are in the FULL or EXSTART state. */
int is_free 		= 0;							/* A router is free if and only if it is idle and all primary messages sent by it to other routers have been acknowledged, otherwise it is loaded. */
int num_unack_msgs 	= 0;
uint32_t resume_on  = 0;
int	num_queued_ackreqs_routinginfo 	= 0;
int	num_queued_ackreqs_routingio 	= 0;
int dc		 		= 0;							/* Disconnect */
struct dc_element* dc_list = NULL;					/* List of disconnecting neighbors. */

/* These variables are only used by the routinginfo thread. */
int offer_count 	= 0;

/* Parent needs to be locked when used. Can also be used by the routingio thread. */
struct connection* 	parent 		= NULL;
struct child* 		children 	= NULL;
uint32_t root;

pthread_rwlock_t 	termination_lock;

/* timeval structs to measure termination delay. */
struct timeval term_start;
struct timeval term_end;
struct timeval term_delay;

struct timeval sync_end;

int first_termination = 1;

int times_terminated = 0;							/* Used for debugging/testing. Can be removed. */

extern struct routing_connection* write_set;

struct ack_log* ack_logs = NULL;

struct ack_log {
	struct connection* connection;
	uint32_t router_id_1;
	uint32_t router_id_2;

	struct ack_log* prev;
	struct ack_log* next;
};

void send_parent_offers();
void send_ackreqs_routinginfo();
void send_ackreqs_routingio();
void ackreq_received(struct connection* connection, int piggyback);

void send_term_packet(uint8_t type, struct connection* connection, uint32_t router_id_1, uint32_t router_id_2) {
	struct termination_packet* term_packet = create_termination_packet(router_id, connection->area_id, type, router_id_1, router_id_2);
	char* buffer = pack_termination_packet(term_packet);

	send_packet(connection, buffer, term_packet->common_header.length);

	free(term_packet);
	free(buffer);
}

void send_resume(struct connection* connection, uint32_t router_id_1, uint32_t router_id2) {
	send_term_packet(RESUME, connection, router_id_1, router_id2);
	dno_log("Sent RESUME to %s\n", connection->neighbor_ip_string);
	update_stats(RESUMES_SENT);
}

void send_acknowledge(struct connection* connection, uint32_t router_id_1, uint32_t router_id2) {
	send_term_packet(ACKNOWLEDGE, connection, router_id_1, router_id2);
	dno_log("Sent ACKNOWLEDGE to %s\n", connection->neighbor_ip_string);
	update_stats(ACKNOWLEDGES_SENT);
}

void send_stop(struct connection* connection) {
	send_term_packet(STOP, connection, 0, 0);
	dno_log("Sent STOP to %s\n", connection->neighbor_ip_string);
	update_stats(STOPS_SENT);
}

void send_termination(struct connection* connection) {
	send_term_packet(TERMINATION, connection, 0, 0);
	dno_log("Sent TERMINATION to %s\n", connection->neighbor_ip_string);
	update_stats(TERMINATIONS_SENT);
}

void send_parent_offer(struct connection* connection) {
	send_term_packet(PARENT_OFFER, connection, 0, 0);
	dno_log("Sent PARENT_OFFER to %s\n", connection->neighbor_ip_string);
	update_stats(PARENT_OFFERS_SENT);
}

void send_parent_accept(struct connection* connection) {
	send_term_packet(PARENT_ACCEPT, connection, 0, 0);
	dno_log("Sent PARENT_ACCEPT to %s\n", connection->neighbor_ip_string);
	update_stats(PARENT_ACCEPTS_SENT);
}

void send_parent_reject(struct connection* connection) {
	send_term_packet(PARENT_REJECT, connection, 0, 0);
	dno_log("Sent PARENT_REJECT to %s\n", connection->neighbor_ip_string);
	update_stats(PARENT_REJECTS_SENT);
}

void send_ackreq(struct connection* connection) {
	send_term_packet(ACKREQ, connection, 0, 0);
	dno_log("Sent ACKREQ to %s\n", connection->neighbor_ip_string);
	update_stats(ACKREQS_SENT);
}

void send_ack(struct connection* connection) {
	send_term_packet(ACK, connection, 0, 0);
	dno_log("Sent ACK to %s\n", connection->neighbor_ip_string);
	update_stats(ACKS_SENT);
}

void log_ack_path(struct connection* connection, uint32_t router_id_1, uint32_t router_id_2) {
	struct ack_log* ack_log = (struct ack_log*) malloc(sizeof(struct ack_log));

	ack_log->connection = connection;
	ack_log->router_id_1 = router_id_1;
	ack_log->router_id_2 = router_id_2;

	DL_APPEND(ack_logs, ack_log);
}

int compare_logs(struct ack_log* a, struct ack_log* b) {
	if (a->router_id_1 == b->router_id_1 && a->router_id_2 == b->router_id_2) {
		return 0;
	} else {
		return -1;
	}
}

struct connection* find_next_child(uint32_t router_id_1, uint32_t router_id_2) {
	struct connection* child;
	struct ack_log* result = NULL;
	struct ack_log like;
	like.router_id_1 = router_id_1;
	like.router_id_2 = router_id_2;

	DL_SEARCH(ack_logs, result, &like, compare_logs);

	if (result == NULL) {
		dno_print("Error: could not find child to forward ACKNOWLEDGE packet to.");
		exit(EXIT_FAILURE);
	}

	child = result->connection;

	DL_DELETE(ack_logs, result);
	free(result);

	return child;
}

void reset() {
	terminate 	= 0;
	is_free 	= 0;
	inactive 	= 0;
	resume_on	= 0;
	termination_turn = !termination_turn;

	reset_stats();
}

void add_child(uint32_t router_id) {
	struct child* child = (struct child*) malloc(sizeof(struct child));
	child->inactive 	= 0;
	child->router_id 	= router_id;

	DL_APPEND(children, child);
}

void remove_child(uint32_t router_id) {
	struct child* child = NULL;

	DL_SEARCH_SCALAR(children, child, router_id, router_id);

	if (child != NULL) {
		DL_DELETE(children, child);
		free(child);
	}
}

void free_children() {
	struct child* child, *tmp;

	DL_FOREACH_SAFE(children, child, tmp) {
		DL_DELETE(children, child);
		free(child);
	}
}

void add_dc_neighbor(uint32_t dc_router_id) {
	write_lock_termination();

	struct dc_element* new_dc_elt = (struct dc_element*) malloc(sizeof(struct dc_element));

	new_dc_elt->router_id = dc_router_id;

	DL_APPEND(dc_list, new_dc_elt);

	unlock_termination();
}

void free_dc_list() {
	struct dc_element* dc_elt, *tmp;

	DL_FOREACH_SAFE(dc_list, dc_elt, tmp) {
		DL_DELETE(dc_list, dc_elt);
		free(dc_elt);
	}
}

void termination_init() {
	int rv;

    rv = pthread_rwlock_init(&termination_lock, NULL);
	if (rv != 0) {
		dno_print_error("Lock init failed on num_unack_msgs_lock");
		exit(EXIT_FAILURE);
    }
}

int is_idle() {
	struct connection* connection, *tmp;
	struct routing_connection* r_connection, *tmp2;
	int result = 1;

	HASH_ITER(hh, connections, connection, tmp) {										/* Check if all connections are in the FULL or EXSTART state. */
		if (connection->state != STATE_FULL && connection->state != STATE_EXSTART) {
			result = 0;
		}
	}

	read_lock_sets();
	HASH_ITER(hh, full_set, r_connection, tmp2) {										/* Check if all routing_connection buffers are empty. */
		if (r_connection->buffer != NULL || r_connection->recv_packet->pos != 0) {
			result = 0;
		}
	}
	unlock_sets();

	return result;
}

int is_inactive() {
	int result = 0;

	read_lock_termination();
	result = inactive;
	unlock_termination();

	return result;
}

int is_disconnecting() {
	int result = 0;

	read_lock_termination();
	if (dc) result = 1;
	unlock_termination();

	return result;
}

void start_termination() {
	write_lock_termination();
	if (terminate != 1) {
		//dno_print("Starting termination");
		if (times_terminated >= 1) {
			gettimeofday(&term_start, NULL);
		}
		
		terminate = 1;

		send_ackreqs_routinginfo();

		unlock_termination();					/* Necessary to prevent deadlock. */
		send_ackreqs_routingio();
		write_lock_termination();
	}
	unlock_termination();
}

void num_unack_msgs_minus() {
	num_unack_msgs--;
	if (num_unack_msgs < 0) {
		dno_print("Negative num_unack_msgs");
	}
}

void increment_unack_msgs() {
	write_lock_termination();
	num_unack_msgs++;
	is_free = 0;
	unlock_termination();
}

void decrement_unack_msgs() {
	write_lock_termination();
	num_unack_msgs_minus(); //TODO replace with --, this is for debugging
	unlock_termination();
}

void set_ackreq_bit(char* buffer) {
	unsigned char* flags;

	flags = (unsigned char*) buffer+COMMON_HEADER_FLAG_OFFSET;

	SET_ACKREQ_UP(*flags);
}

void set_ackreq_bit_locked(char* buffer) {
	if (terminate) {
		num_unack_msgs++;
		is_free = 0;

		set_ackreq_bit(buffer);
	}
}

void set_ackreq_bit_routinginfo(struct connection* connection, char* buffer) {
	if (terminate) {
		increment_unack_msgs();
		set_ackreq_bit(buffer);
		dno_log("Sent ACKREQ to %s (piggyback)\n", connection->neighbor_ip_string);
		update_stats(ACKREQS_SENT_PIGGYBACK);
	}
}

//void sent_primary_msg_locked(struct connection* connection) { TODO remove, replaced by ackreq bit
//	if (terminate) {
//		num_unack_msgs++;
//		is_free = 0;
//		send_ackreq(connection);
//	}
//}

//void sent_primary_msg_routinginfo(struct connection* connection) { TODO remove, replaced by ackreq bit
//	if (terminate) {
//		increment_unack_msgs();
//		send_ackreq(connection);
//	}
//}

//void sent_primary_msg_routingio(struct routing_connection* r_connection) {
//	if (terminate /* && r_connection->buffer_count == 1 */) {
//		char*	ackreq_packet;
//		int 	pos = 0;
//		int 	rv;
//
//		/* Construct ACKREQ packet. */
//		ackreq_packet = (char*) malloc(PACKET_HEADER);
//		write_packet_header(r_connection->peer, router_id, PACKET_HEADER, ackreq_packet, ACKREQ);
//
//		/* Send it to the neighbor. */
//		while(pos != PACKET_HEADER) {
//			rv = send(r_connection->fd, ackreq_packet+pos, PACKET_HEADER-pos, 0);
//			if (rv == -1) {
//				dno_print_error("sent_primary_msg_routingio");
//				exit(EXIT_FAILURE);
//			}
//
//			pos += rv;
//		}
//
//		free(ackreq_packet);
//
//		dno_log("Sent ACKREQ to %s (data_connection)\n", r_connection->neighbor_ip_string);
//	}
//}

void received_primary_msg(uint32_t neighbor_id, int term_turn) {
	if (terminate && term_turn == termination_turn) {
		write_lock_termination();

		idle = 0;
		is_free = 0;
		if (inactive) {
			send_resume(parent, neighbor_id, router_id);

			resume_on = neighbor_id;

			inactive = 0;
		}

		unlock_termination();
	}
}

/* It is assumed that when this function is called, connections is already locked, otherwise received_primary_msg_routingio has to be called. */
void received_primary_msg_routinginfo(struct connection* connection, unsigned char flags) {
	received_primary_msg(connection->neighbor_id, IS_TERM_TURN_UP(flags));

	if (HAS_ACKREQ(flags)) {
		write_lock_termination();
		ackreq_received(connection, 1);
		unlock_termination();
	}
}

void received_primary_msg_routingio(struct routing_connection* r_connection, unsigned char flags) {
	received_primary_msg(r_connection->peer, IS_TERM_TURN_UP(flags));

	if (HAS_ACKREQ(flags)) {
		ackreq_received_routingio(r_connection, 1);
	}

//	if (terminate) {
//		//unlock_sets(); 					/* Necessary to prevent deadlock. */
//
//		//read_lock_connections();
//
//		//struct connection* connection = find_connection(connections, r_connection->peer);
//		received_primary_msg_routinginfo(r_connection->peer, term_turn);
//
//		//unlock_connections();
//
//		//write_lock_sets();
//	} TODO remove
}

/* ACKs on data connections cannot be sent immediately but need to be queued, otherwise ACK packets can get mangled with data packets. */
void insert_term_packet(struct routing_connection* r_connection, uint8_t type) {
	struct pckt* packet = init_packet();
	packet->new 		= 0; 										/* This packet should not be handled as a primary message. */
	packet->dest 		= r_connection->peer;
	packet->src			= router_id;
	packet->size		= PACKET_HEADER;
	packet->type 		= type;

	packet->packet = (char*) malloc(PACKET_HEADER);
	write_packet_header(r_connection->peer, router_id, PACKET_HEADER, packet->packet, type);

	if (r_connection->buffer != NULL) {
		struct pckt* aux = r_connection->buffer;

		DL_DELETE(r_connection->buffer, aux);						/* Temporarily remove the first packet in the buffer. */
		DL_PREPEND(r_connection->buffer, packet);					/* Prepend the new packet to the buffer. */
		DL_PREPEND(r_connection->buffer, aux);						/* Prepend the first packet we previously removed in front of the new packet. */
	} else {
		DL_PREPEND(r_connection->buffer, packet);
	}

	r_connection->buffer_count++;

	struct routing_connection* is_in_ws;
	HASH_FIND(hh_ws, write_set, &(r_connection->peer), sizeof(int), is_in_ws);
	if (is_in_ws == NULL) {
		HASH_ADD(hh_ws, write_set, peer, sizeof(int), r_connection);									/* If it is not already in there, add the connection to the next hop to the write set. */
	}

	if (type == ACK) {
		dno_log("Inserted ACK in %s (data_connection)\n", r_connection->neighbor_ip_string);
	} else if (type == ACKREQ) {
		dno_log("Inserted ACKREQ in %s (data_connection)\n", r_connection->neighbor_ip_string);
	}
}

/* This function should only be called while the calling thread has a lock on termination_lock. */
void ackreq_received(struct connection* connection, int piggyback) {
	if (piggyback) {
		dno_log("Received ACKREQ from %s (piggyback)\n", connection->neighbor_ip_string);
		update_stats(ACKREQS_RECEIVED_PIGGYBACK);
	} else {
		dno_log("Received ACKREQ from %s\n", connection->neighbor_ip_string);
		update_stats(ACKREQS_RECEIVED);
	}

	if(!inactive) {
		send_ack(connection);
	} else {
		if (resume_on == connection->neighbor_id) {
			num_queued_ackreqs_routinginfo++;
		} else {
			send_ack(connection);
		}
	}
}

void ackreq_received_routingio(struct routing_connection* r_connection, int piggyback) {
	if (piggyback) {
		dno_log("Received ACKREQ from %s (data_connection)(piggyback)\n", r_connection->neighbor_ip_string);
		update_stats(ACKREQS_RECEIVED_PIGGYBACK);
	} else {
		dno_log("Received ACKREQ from %s (data_connection)\n", r_connection->neighbor_ip_string);
		update_stats(ACKREQS_RECEIVED);
	}

	if(!inactive) {
		insert_term_packet(r_connection, ACK);
	} else {
		write_lock_termination();
		if (resume_on == r_connection->peer) {
			num_queued_ackreqs_routingio++;
		} else {
			insert_term_packet(r_connection, ACK);
		}
		unlock_termination();
	}
}

void ack_received_routingio(struct routing_connection* r_connection) {
	dno_log("Received ACK from %s (data_connection)\n", r_connection->neighbor_ip_string);
	decrement_unack_msgs();
	update_stats(ACKS_RECEIVED);
}

void process_queued_ackreqs_routinginfo() {
	struct connection* c = find_connection(connections, resume_on);
	int i;

	for (i = 0; i < num_queued_ackreqs_routinginfo; i++) {
		send_ack(c);
		dno_log("(^queued^)");
	}

	num_queued_ackreqs_routinginfo = 0;
}

void process_queued_ackreqs_routingio() {
	struct routing_connection* r_connection;
	int i;

	write_lock_sets();

	/* Find r_connection corresponding to resume_on. */
	r_connection = find_connection_in_full_set(resume_on);

	for (i = 0; i < num_queued_ackreqs_routingio; i++) {
		insert_term_packet(r_connection, ACK);
		dno_log("Inserted ACK in %s (data_connection) (queued)\n", r_connection->neighbor_ip_string);
	}

	unlock_sets();
}

void send_ackreqs_routinginfo() {
	struct connection* connection, *tmp;

	HASH_ITER(hh, connections, connection, tmp) {
		if (connection->state == STATE_EXSTART) {
			continue;
		}

		num_unack_msgs++;
		send_ackreq(connection);
	}
}

void send_ackreqs_routingio() {
	struct routing_connection* r_connection, *tmp;

	write_lock_sets();

	HASH_ITER(hh, full_set, r_connection, tmp) {
		increment_unack_msgs();
		insert_term_packet(r_connection, ACKREQ);
		dno_log("Inserted ACKREQ in %s (data_connection) (flush)\n", r_connection->neighbor_ip_string);
	}

	unlock_sets();
}

int all_children_inactive() {
	struct child* child;

	DL_FOREACH(children, child) {
		if (child->inactive != 1) {
			return 0;
		}
	}

	return 1;
}

void setup_routing_table() {
	struct graph* graph = construct_graph();
	calculate_routing_table(graph);
	free_graph(graph);
}

void process_disconnecting_neighbors() {
	struct dc_element* dc_elt;

	DL_FOREACH(dc_list, dc_elt) {
		/* Remove the neighbor from the routing info connections. */
		unlock_termination();
		delete_routing_connection(dc_elt->router_id);															/* Remove routing connection. */
		write_lock_termination();

		remove_connection(dc_elt->router_id);																	/* Remove connection. (connections is already locked so call remove_connection directly) */

		delete_router_from_areas(dc_elt->router_id);															/* Delete the router from the areas. */
	}

	free_dc_list();
}

struct connection* exec_termination(struct connection* connection) {
	struct child* child;
	struct connection* child_connection;

	DL_FOREACH(children, child) {
		child_connection = find_connection(connections, child->router_id);
		send_termination(child_connection);
	}

	if (!dc) {
		if (connection != NULL) {
			struct dc_element* dc_elt = NULL;
			DL_SEARCH_SCALAR(dc_list, dc_elt, router_id, connection->neighbor_id);
			if (dc_elt != NULL) {
				connection = NULL;
			}
		}

		process_disconnecting_neighbors();

		purge_expired_lsas();
		setup_routing_table();
		reset();
	} else {
		/* This cleanup is normally done in the functions called in the previous block.
		 * In case the router is disconnecting those functions will not be called, so do the cleaning up here.
		 */
		free_dc_list();
		free(parent);
		free_children();
		run = 0;
		reset();
	}

	//TODO remove dno_print("Terminated");
	gettimeofday(&term_end, NULL);
	get_interval(&term_start, &term_end, &term_delay);
	struct timeval sync_delay;
	get_interval(&term_start, &sync_end, &sync_delay);

	print_stats_summary(term_delay, sync_delay, first_termination, dc);

	if (first_termination) first_termination = 0;

	times_terminated++;

	return connection;
}

void detect_termination() {
	if (terminate) {
		if (is_idle()) {
			write_lock_termination();
			idle = 1;

//			if (num_unack_msgs <= 0) {  //TODO this is a hack. It should never be lower than 0 but this seems to happen during the bd test.
//				is_free = 1;
//			}

			if (num_unack_msgs < 0) {
				dno_print("negative num_unack_msgs");
			} else if (num_unack_msgs == 0) {
				is_free = 1;
			}

			unlock_termination();
		}
	}

	write_lock_termination();
	if (is_free && all_children_inactive() && !inactive && parent != NULL && offer_count == 0 && resume_on == 0) {				/* The router should not go inactive if it is waiting for an ACKNOWLEDGE packet or it is possible in will be woken up by another router, resume_on will be overwritten by the new router and if the ACKNOWLEDGE packet for the first RESUME comes in it will not correspond with the resume_on value. */
		if (router_id == root) {
			exec_termination(NULL);
		} else {
			send_stop(parent);
			inactive = 1;
		}
	} else {
		int i;
		i = 0;
	}
	unlock_termination();
}

int is_termination_packet(uint8_t type) {
	switch (type) {
		case RESUME: return 1;
		case ACKNOWLEDGE: return 1;
		case STOP: return 1;
		case TERMINATION: return 1;
		case PARENT_OFFER: return 1;
		case PARENT_ACCEPT: return 1;
		case PARENT_REJECT: return 1;
		case ACKREQ: return 1;
		case ACK: return 1;
		default: return 0;
	}
}

struct connection* process_termination_packet(struct termination_packet* term_packet, struct connection* connection) {
	struct child* child;

	write_lock_termination();

	switch(term_packet->common_header.type) {
		case TERMINATION:
			dno_log("Received TERMINATION from %s\n", connection->neighbor_ip_string);
			update_stats(TERMINATIONS_RECEIVED);
			connection = exec_termination(connection);
			break;
		case RESUME:
			dno_log("Received RESUME from %s\n", connection->neighbor_ip_string);
			update_stats(RESUMES_RECEIVED);

			DL_SEARCH_SCALAR(children, child, router_id, connection->neighbor_id);

			if (child->inactive == 0) {
				dno_print("negative child->inactive");
				//break;
				/* TODO
				 * This is a hack. inactive should never be negative.
				 * There is a bug that allows a router to receive a resume after termination. This would normally cause it to decrement inactive below 0.
				 *
				 * Reproduce this error by having a node exit from the overlay while another node is sending to another node.
				 * If the sender is the parent of the receiver it will possibly receive a resume after termination.
				 */
			}

			child->inactive--;
			if (inactive) {
				send_resume(parent, term_packet->router_id_1, term_packet->router_id_2);

				log_ack_path(connection, term_packet->router_id_1, term_packet->router_id_2);					/* Store the pointer to the connection to which the ACKNOWLEDGE packet will have to be forwarded. */

				inactive = 0;
			} else {
				send_acknowledge(connection, term_packet->router_id_1, term_packet->router_id_2);				/* Send acknowledgment to the child router on the way to term_packet->router_id_2. */
			}
			break;
		case ACKNOWLEDGE:
			dno_log("Received ACKNOWLEDGE from %s\n", connection->neighbor_ip_string);
			update_stats(ACKNOWLEDGES_RECEIVED);

			if (term_packet->router_id_2 == router_id) {
				if (resume_on != term_packet->router_id_1) {//TODO debug code; remove.
					dno_print("Fatal error in termination detection: resume_on != term_packet->router_id_1");
					dno_print("resume_on: %d", resume_on);
					dno_print("term_packet->router_id_1: %d", term_packet->router_id_1);
					//exit(EXIT_FAILURE);
				}

				process_queued_ackreqs_routinginfo();

				unlock_termination(); 																			/* Necessary to prevent deadlock. */
				process_queued_ackreqs_routingio();
				write_lock_termination();
				num_queued_ackreqs_routingio = 0;																/* This should be in the function process_queued_ackreqs_routingio but is here for locking reasons. */

				resume_on = 0;
				/* resume_on needs to be reset or it is possible to receive ACKREQs from the neighbor this router was resuming_on
				 * (if it was this routers parent as well) after receiving an ACKNOWLEDGE from this routers parent.
				 * This will cause the router to queue the ACK because it is inactive but will never receive an ACKNOWLEDGE because it never sent a resume after going inactive.
				 *
				 * It will send the ACK to any router it is not resuming_on even when inactive. That is why setting resume_on to 0 after an ACKNOWLEDGE will prevent this.
				 */
			} else {
				struct connection* child_connection = find_next_child(term_packet->router_id_1, term_packet->router_id_2);
				send_acknowledge(child_connection, term_packet->router_id_1, term_packet->router_id_2);			/* Send acknowledgment to the child router on the way to term_packet->router_id_2. */
			}
			break;
		case STOP:
			dno_log("Received STOP from %s\n", connection->neighbor_ip_string);
			update_stats(STOPS_RECEIVED);

			DL_SEARCH_SCALAR(children, child, router_id, connection->neighbor_id);
			child->inactive++;
			break;
		case PARENT_OFFER:
			dno_log("Received PARENT_OFFER from %s\n", connection->neighbor_ip_string);
			update_stats(PARENT_OFFERS_RECEIVED);

			connection->has_parent = 1;

			if (parent == NULL) {
				send_parent_accept(connection);

				parent = (struct connection*) malloc(sizeof(struct connection));
				memcpy(parent, connection, sizeof(struct connection));

				send_parent_offers();
			} else {
				send_parent_reject(connection);
			}

			break;
		case PARENT_ACCEPT:
			dno_log("Received PARENT_ACCEPT from %s\n", connection->neighbor_ip_string);
			update_stats(PARENT_ACCEPTS_RECEIVED);

			add_child(connection->neighbor_id);
			offer_count--;
			break;
		case PARENT_REJECT:
			dno_log("Received PARENT_REJECT from %s\n", connection->neighbor_ip_string);
			update_stats(PARENT_REJECTS_RECEIVED);

			offer_count--;
			break;
		case ACKREQ:
			ackreq_received(connection, 0);
			break;
		case ACK:
			dno_log("Received ACK from %s\n", connection->neighbor_ip_string);
			update_stats(ACKS_RECEIVED);

			num_unack_msgs_minus(); //TODO replace with --, this is for debugging
			break;
		default:
			//todo remove, debug code
			dno_print("process_termination_packet: invalid packet type.");
			dno_print("connection->neighbor_id: %d", connection->neighbor_id);
			dno_print("connection->disconnected: %d", connection->disconnected);
			dno_print("term_packet->common_header.area_id: %d", term_packet->common_header.area_id);
			dno_print("term_packet->common_header.length: %d", term_packet->common_header.length);;
			dno_print("term_packet->common_header.router_id: %d", term_packet->common_header.router_id);
			dno_print("term_packet->common_header.type: %d", term_packet->common_header.type);

			dno_print("term_packet->router_id_1: %d", term_packet->router_id_1);
			dno_print("term_packet->router_id_2: %d", term_packet->router_id_2);
			break;
	}

	unlock_termination();

	return connection;
}

/* This function assumes that the caller has already locked the connections table and the termination lock. */
void send_parent_offers() {
	struct connection* connection, *tmp;

	if (parent != NULL) {
		HASH_ITER(hh, connections, connection, tmp) {
			if (!connection->has_parent) {
				send_parent_offer(connection);
				offer_count++;
			}
		}
	}
}

void send_parent_offers_unlocked() {
	write_lock_connections();					/* The order of the locks is important to prevent deadlock with the routinginfo thread. */
	write_lock_termination();

	send_parent_offers();

	unlock_termination();
	unlock_connections();
}

int has_smallest_router_id(struct area* area) {
	struct router* router;

	DL_FOREACH(area->routers, router) {
		if (get_router_id(router->ip_string) < router_id) {
			return 0;
		}
	}

	return 1;
}

//TODO right now only one area and thus one parent is considered. When implementing multiple areas, multiple parents also have to be supported.
void select_parents(struct area* areas) {
	if (areas->select_parent) {
		if (has_smallest_router_id(areas)) {
			write_lock_termination();
			parent = (struct connection*) malloc(sizeof(struct connection));
			parent->neighbor_id = 0;
			root = router_id;
			unlock_termination();
		}
	}
}

int child_compare(struct child* a, struct child* b) {
	if (a->router_id < b->router_id) {
		return -1;
	} else if (a->router_id == b->router_id) {
		return 0;
	} else {
		return 1;
	}
}

void print_parent_and_children() {
	char parent_id[INET6_ADDRSTRLEN];
	struct child* child;

	write_lock_termination();

	if (parent != NULL) {
		inet_ntop(AF_INET, &(parent->neighbor_id), parent_id, INET6_ADDRSTRLEN);
	} else {
		parent_id[0] = '\0';
	}
	DL_SORT(children, child_compare);

	dno_printf(0, "========== Printing parent and children ==========");

	dno_printf(0, "Parent: %s", parent_id);

	DL_FOREACH(children, child) {
		char child_id[INET6_ADDRSTRLEN];

		inet_ntop(AF_INET, &(child->router_id), child_id, INET6_ADDRSTRLEN);

		dno_printf(0, "Child: %s", child_id);
	}

	dno_printf(1, "==================================================");

	unlock_termination();
}

void print_termination_status() {
	char resume_on_string[INET6_ADDRSTRLEN];
	char root_string[INET6_ADDRSTRLEN];

	write_lock_termination();
	inet_ntop(AF_INET, &(resume_on), resume_on_string, INET6_ADDRSTRLEN);
	inet_ntop(AF_INET, &(root), root_string, INET6_ADDRSTRLEN);


	dno_printf(0, "========== Printing termination status ==========");

	dno_printf(0, "terminate:\t\t\t\t %d", terminate);
	dno_printf(0, "inactive:\t\t\t\t %d", inactive);
	dno_printf(0, "idle:\t\t\t\t %d", idle);
	dno_printf(0, "is_free:\t\t\t\t %d", is_free);
	dno_printf(0, "num_unack_msgs:\t\t\t %d", num_unack_msgs);
	dno_printf(0, "resume_on:\t\t\t\t %s", resume_on_string);
	dno_printf(0, "num_queued_ackreqs_routinginfo:\t %d", num_queued_ackreqs_routinginfo);
	dno_printf(0, "num_queued_ackreqs_routingio:\t %d", num_queued_ackreqs_routingio);
	dno_printf(0, "offer_count:\t\t\t\t %d", offer_count);
	dno_printf(0, "root:\t\t\t\t %s", root_string);
	dno_printf(0, "dc:\t\t\t\t\t %d", dc);

	dno_printf(0, "termination_turn:\t\t\t %d", termination_turn);

	dno_printf(1, "=================================================");

	unlock_termination();
}

void read_lock_termination() {
    if (pthread_rwlock_rdlock(&termination_lock) != 0) {
    	dno_print("read_lock_termination: Can't acquire readlock.\n");
    	exit(EXIT_FAILURE);
    }
}

void write_lock_termination_priv(const char *file, int line, const char *function) {
	char filename[MAX_HOSTNAME_LENGTH+1];
	FILE* dbgfile;

	if (LOG) {
		sprintf(filename, "%s_term_lock.dbg", hostname);
		dbgfile = fopen(filename, "a");

		fprintf(dbgfile, "%s(%d):%s() trying to write-lock termination_lock.\n", file, line, function);
		fclose(dbgfile);
	}

    if (pthread_rwlock_wrlock(&termination_lock) != 0) {
    	dno_print("write_lock_termination: Can't acquire writelock.\n");
    	exit(EXIT_FAILURE);
    }

    if (LOG) {
		dbgfile = fopen(filename, "a");
		fprintf(dbgfile, "%s(%d):%s() write-locked termination_lock.\n", file, line, function);
		fclose(dbgfile);
    }
}

void unlock_termination() {
	if(pthread_rwlock_unlock(&termination_lock) != 0) {
		dno_print("Error unlocking termination_lock.\n");
		exit(EXIT_FAILURE);
	}
}

