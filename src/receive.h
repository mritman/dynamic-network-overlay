/*
 * receive.h
 *
 *  Created on: Apr 22, 2011
 *      Author: mritman
 */

#ifndef RECEIVE_H_
#define RECEIVE_H_

#include <pthread.h>

pthread_t create_receive_thread();

#endif /* RECEIVE_H_ */
