/*
 * routingio.c
 *
 *  Created on: Aug 24, 2010
 *      Author: mritman
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <unistd.h>

#include "utils.h"
#include "routingtable.h"
#include "routingio.h"
#include "connections.h"
#include "dno.h"
#include "packets.h"
#include "termination.h"
#include "routinginfo.h"
#include "stats.h"

struct routing_connection* full_set 	= NULL;
struct routing_connection* read_set 	= NULL;
struct routing_connection* write_set	= NULL;
pthread_rwlock_t sets_lock;

struct pckt* 		receive_buffer = NULL;
int					receive_buffer_count = 0;
pthread_rwlock_t 	receive_buffer_lock;
struct pckt* 		send_buffer = NULL;
int					send_buffer_count = 0;
pthread_rwlock_t 	send_buffer_lock;

void read_lock_sets();
void write_lock_sets_priv();
void unlock_sets();
void read_lock_receive_buffer();
void write_lock_receive_buffer();
void unlock_receive_buffer();
void read_lock_send_buffer();
void write_lock_send_buffer();
void unlock_send_buffer();

void routingio_init() {
	int rv;

	/* Initialize the locks used for shared variables from the routingio module */
    rv = pthread_rwlock_init(&sets_lock,NULL);
	if (rv != 0) {
		dno_print_error("Lock init failed on sets_lock");
		exit(EXIT_FAILURE);
    }
    rv = pthread_rwlock_init(&receive_buffer_lock,NULL);
	if (rv != 0) {
		dno_print_error("Lock init failed on receive_buffer_lock");
		exit(EXIT_FAILURE);
    }
    rv = pthread_rwlock_init(&send_buffer_lock,NULL);
	if (rv != 0) {
		dno_print_error("Lock init failed on send_buffer_lock");
		exit(EXIT_FAILURE);
    }
}

struct pckt* init_packet() {
	struct pckt* packet = (struct pckt*) malloc(sizeof(struct pckt));
	if (packet == NULL) {
		dno_print("init_packet: Error allocating memory for new packet.\n");
		exit(EXIT_FAILURE);
	}
	packet->packet 	= NULL;
	packet->dest 	= 0;
	packet->src		= 0;
	packet->size 	= 0;
	packet->type	= 0;
	packet->pos 	= 0;
	packet->new		= 1;
	packet->next 	= NULL;
	packet->prev 	= NULL;

	return packet;
}

struct routing_connection* initialize_routing_connection(int fd, int fd_info, char* ipstr, uint16_t their_port) {
	struct routing_connection* new_routing_connection;

	new_routing_connection = (struct routing_connection*) malloc(sizeof(struct routing_connection));
	new_routing_connection->fd 					= fd;
	new_routing_connection->fd_info				= fd_info;
	new_routing_connection->peer 				= get_router_id(ipstr);
	new_routing_connection->queue 				= NULL;
	new_routing_connection->buffer 				= NULL;
	new_routing_connection->buffer_count 		= 0;
	new_routing_connection->recv_packet 		= init_packet();
	strncpy(new_routing_connection->neighbor_ip_string, ipstr, INET6_ADDRSTRLEN);

	return new_routing_connection;
}

void add_to_full_and_read_set(struct routing_connection* connection) {
	write_lock_sets();
	HASH_ADD_INT(full_set, peer, connection);
	HASH_ADD(hh_rs, read_set, peer, sizeof(int), connection);
	unlock_sets();
}

void add_to_read_set(struct routing_connection* connection) {
	write_lock_sets();
	HASH_ADD(hh_rs, read_set, peer, sizeof(int), connection);
	unlock_sets();
}

void add_to_write_set(struct routing_connection* connection) {
	write_lock_sets();
	HASH_ADD(hh_ws, write_set, peer, sizeof(int), connection);
	unlock_sets();
}

void remove_from_from_read_set(struct routing_connection* connection) {
	write_lock_sets();
	HASH_DELETE(hh_rs, read_set, connection);
	unlock_sets();
}

void remove_from_from_write_set(struct routing_connection* connection) {
	write_lock_sets();
	HASH_DELETE(hh_ws, write_set, connection);
	unlock_sets();
}

void enqueue(struct routing_connection* n, struct routing_connection* c) {
	struct queue_item* new_queue_item 	= (struct queue_item*) malloc(sizeof(struct queue_item));
	new_queue_item->next 				= NULL;
	new_queue_item->prev 				= NULL;
	new_queue_item->waiting_connection 	= c;

	DL_APPEND(n->queue, new_queue_item);
}

struct routing_connection* dequeue(struct routing_connection* n) {
	struct routing_connection* result 	= n->queue->waiting_connection;				/* Pointer to the connection that was waiting at the head of the queue. */
	struct queue_item* temp 			= n->queue;									/* Pointer to the head of the queue that can now be deleted. */

	DL_DELETE(n->queue, n->queue);													/* Delete head of the queue from the queue */
	free(temp);																		/* Free former head of queue */

	return result;																	/* Return the pointer to the waiting connection. */
}

int compare_routing_connection(struct queue_item* a, struct queue_item* b) {
	int result = 1;

	if (a->waiting_connection == b->waiting_connection) {
		result = 0;
	}

	return result;
}

int delete_from_queue(struct routing_connection* n, struct routing_connection* w) {
	struct queue_item* elt;
	struct queue_item temp;
	int result = 0;

	temp.waiting_connection = w;

	DL_SEARCH(n->queue, elt, &temp, compare_routing_connection);
	if (elt) { /* if a match was found */
		DL_DELETE(n->queue, elt);
		free(elt);
		result = 1;
	}

	return result;
}

/* This function is only called by functions that have already acquired the necessary lock to the read and write set.
 * Therefore no locking needs to be done inside this function.
 */
int delete_from_queues(struct routing_connection* w) {
	int 						result = 0;
	struct routing_connection* 	current_connection;

	/* Go through all of the queues of each connection until this connection is found and deleted */
	for(current_connection = read_set; current_connection != NULL; current_connection = current_connection->hh_rs.next) {
		result = delete_from_queue(current_connection, w);
		if (result == 1) {
			return result;
		}
	}
	for(current_connection = write_set; current_connection != NULL; current_connection = current_connection->hh_ws.next) {
		result = delete_from_queue(current_connection, w);
		if (result == 1) {
			return result;
		}
	}

	return result;
}

void free_pckt_buffer(struct routing_connection* connection) {
	struct pckt *elt, *tmp;

    DL_FOREACH_SAFE(connection->buffer, elt, tmp) {
    	DL_DELETE(connection->buffer, elt);
    	free(elt);
    }
}

void free_queue(struct routing_connection* connection) {
	struct queue_item *elt, *tmp;

    DL_FOREACH_SAFE(connection->queue, elt, tmp) {
    	DL_DELETE(connection->queue, elt);
    	free(elt);
    }
}

struct routing_connection* find_connection_in_full_set(uint32_t peer) {
    struct routing_connection* rc;

    HASH_FIND_INT(full_set, &peer, rc);

    return rc;
}

void delete_from_all_sets(struct routing_connection* connection) {
	struct routing_connection* c;

	HASH_FIND_INT(read_set, &(connection->peer), c);

	if (c != NULL) {
		HASH_DELETE(hh_rs, read_set, connection);
	}

	HASH_FIND_INT(write_set, &(connection->peer), c);

	if (c != NULL) {
		HASH_DELETE(hh_ws, write_set, connection);
	}

	HASH_DELETE(hh, full_set, connection);
}

void free_routing_connection(struct routing_connection* connection) {
	free_queue(connection);
	free_pckt_buffer(connection);
	free(connection->recv_packet);
	free(connection);
}

//TODO do something with the return values of the functions that are called in this functions.
int delete_routing_connection(uint32_t peer) {
	struct routing_connection* rc;

	write_lock_sets();

	rc = find_connection_in_full_set(peer);
	if (rc == NULL) {
		unlock_sets();
		return 0;
	}

	delete_from_all_sets(rc);
	delete_from_queues(rc);
	free_routing_connection(rc);

	unlock_sets();

	return 1;
}

void free_rsws() {
	struct routing_connection* current_connection, *tmp;

	write_lock_sets();

	HASH_CLEAR(hh_rs, read_set);
	HASH_CLEAR(hh_ws, write_set);

	HASH_ITER(hh, full_set, current_connection, tmp) {
		HASH_DEL(full_set, current_connection);
		free_routing_connection(current_connection);
	}

	unlock_sets();
}

void free_buffer(struct pckt* buffer) {
	struct pckt* packet, *tmp;

	DL_FOREACH_SAFE(buffer, packet, tmp) {
		DL_DELETE(buffer, packet);
		free(packet);
	}
}

void free_receive_buffer() {
	write_lock_receive_buffer();
	free_buffer(receive_buffer);
	unlock_receive_buffer();
}

void free_send_buffer() {
	write_lock_send_buffer();
	free_buffer(send_buffer);
	unlock_send_buffer();
}

int set_fd_read_and_write_sets(fd_set* fd_read_set, fd_set* fd_write_set) {
	struct routing_connection* current_connection;
	int highest_fd = 0;

	for(current_connection = read_set; current_connection != NULL; current_connection = current_connection->hh_rs.next) {
		FD_SET(current_connection->fd, fd_read_set);
		if (current_connection->fd > highest_fd) {
			highest_fd = current_connection->fd;
		}
	}
	for(current_connection = write_set; current_connection != NULL; current_connection = current_connection->hh_ws.next) {
		FD_SET(current_connection->fd, fd_write_set);
		if (current_connection->fd > highest_fd) {
			highest_fd = current_connection->fd;
		}
	}

	return highest_fd;
}

void unpack_packet_header(struct pckt* packet, char* header) {
	int offset = 0;

	packet->dest 	= unpack_int32_t(header, &offset);
	packet->src		= unpack_int32_t(header, &offset);
	packet->size 	= unpack_int32_t(header, &offset);
	packet->type	= *(header+offset);
	offset+= sizeof(uint8_t);
	packet->flags = *(header+offset);

	packet->pos 	= PACKET_HEADER;
}

int write_packet_header(uint32_t dest, uint32_t src, uint32_t size, char* packet, uint8_t type) {
	unsigned char flags;
	int offset = 0;

	offset = pack_int32_t(dest, packet, offset);
	offset = pack_int32_t(src, packet, offset);
	offset = pack_int32_t(size, packet, offset);
	memcpy(packet+offset, &type, sizeof(uint8_t));
	offset += sizeof(uint8_t);

	CLEAR_BITS(flags);
	if (termination_turn) SET_TERM_TURN_UP(flags);

	memcpy(packet+offset, &flags, sizeof(unsigned char));
	offset += sizeof(unsigned char);

	return offset;
}

int is_term_packet(struct pckt* packet) {
	if (packet->type == ACKREQ || packet->type == ACK) {
		return 1;
	}

	return 0;
}

void receive(struct routing_connection* c) {
	if (c->recv_packet->size == 0) { 																	/* This is the beginning of a new packet. */
		char 	header[PACKET_HEADER];
		memset(header, 0, PACKET_HEADER);
		int 	pos 		= 0;

		while(pos != PACKET_HEADER) {
			pos += read_bytes(c->fd, header, pos, PACKET_HEADER);

			/* Sometimes a socket is reported as readable by select even when it isn't.
			 * If the first read does not return any bytes we give up reading for now.
			 * Nothing about the c->recv_packet is changed at this point so it is safe to return here without any extra measures.
			 */
			if (pos == 0) return;
		}

		unpack_packet_header(c->recv_packet, header);

		c->recv_packet->packet 	= (char *) malloc(c->recv_packet->size);
		memcpy(c->recv_packet->packet, header, PACKET_HEADER);
	}

	c->recv_packet->pos += read_bytes(c->fd, c->recv_packet->packet, c->recv_packet->pos, c->recv_packet->size);
}

void process_send_buffer() {
	struct pckt* packet, *tmp;
	uint32_t next_hop;
	struct routing_connection* n = NULL;
	struct routing_connection* is_in_ws = NULL;

	write_lock_send_buffer();
	write_lock_sets();

	DL_FOREACH_SAFE(send_buffer, packet, tmp) {
		next_hop = find_next_hop(packet->dest);

		HASH_FIND_INT(full_set, &next_hop, n);
		if (n == NULL) {
			char ip[INET6_ADDRSTRLEN];
			convert_ip_ntop(packet->dest, ip);

			dno_print("process_send_buffer: no connection found for next hop to: %s\n", ip);

			unlock_sets();
			unlock_send_buffer();
			return;
		}

		if (n->buffer_count != BUFFER_SIZE) {
			DL_DELETE(send_buffer, packet);
			send_buffer_count--;

			DL_APPEND(n->buffer, packet);
			n->buffer_count++;

			HASH_FIND(hh_ws, write_set, &(n->peer), sizeof(int), is_in_ws);									/* Add n to the write set if it isn't already in it. */
			if (is_in_ws == NULL) {
				HASH_ADD(hh_ws, write_set, peer, sizeof(int), n);
			}
		}
	}

	unlock_sets();
	unlock_send_buffer();
}


/* Sets the current termination term value in the flag field of the already packed packet. */
void set_bit_flags(struct routing_connection* r_connection) {
	unsigned char 	packed_flags;
	unsigned char 	new_flags;
	char* 			packet = r_connection->buffer->packet;

	memcpy(&packed_flags, packet+FLAG_OFFSET, sizeof(unsigned char));

	CLEAR_BITS(new_flags);

	if (HAS_ACKREQ(packed_flags)) {
		SET_ACKREQ_UP(new_flags);
	}

	if (termination_turn) SET_TERM_TURN_UP(new_flags);

	if (terminate) {
		increment_unack_msgs();
		SET_ACKREQ_UP(new_flags);
		dno_log("Sent ACKREQ to %s (piggyback)\n", r_connection->neighbor_ip_string);
		update_stats(ACKREQS_SENT_PIGGYBACK);
	}

	memcpy(packet+FLAG_OFFSET, &new_flags, sizeof(unsigned char));
}

void *io_loop(void* thread_args) {
	int 							rv;
	fd_set 							fd_read_set;
	fd_set 							fd_write_set;
	struct timeval 					timeout;
	int 							highest_fd;
	struct routing_connection* 		c;

	while(run) {
		timeout.tv_sec 	= 0;
		timeout.tv_usec = 0;

		if (!terminate) {																						/* When waiting for termination do not send out new packets to ensure a finite termination delay. */
			process_send_buffer();
		}

		write_lock_sets();

		FD_ZERO(&fd_read_set); 		/* Clear the fd_read_set */
		FD_ZERO(&fd_write_set); 	/* Clear the fd_write_set */

		highest_fd = set_fd_read_and_write_sets(&fd_read_set, &fd_write_set);

		if (highest_fd == 0) {
			/* No connections ready. */
			unlock_sets();
			continue;
		}

		rv = select(highest_fd+1, &fd_read_set, &fd_write_set, NULL, &timeout);
		if (rv < 0) {
			dno_print_error("io_loop: select failed");
			exit(EXIT_FAILURE);
		}
		if (rv == 0) {
			/* Timeout */
			unlock_sets();
			continue;
		}

		for(c = read_set; c != NULL; c = c->hh_rs.next) {														/* Handle the connections that can be read from. */
			int dest;

			if (FD_ISSET(c->fd, &fd_read_set)) {
				receive(c);

				if (c->recv_packet->pos != c->recv_packet->size || c->recv_packet->size == 0) {					/* If c.packet is partially received, continue. If size == 0, the socket was spuriously marked as ready. In that case also continue. */
					continue;
				}

				if (c->recv_packet->type == ACKREQ) {
					ackreq_received_routingio(c, 0);
					free(c->recv_packet->packet);
					free(c->recv_packet);
				} else if (c->recv_packet->type == ACK) {
					ack_received_routingio(c);
					free(c->recv_packet->packet);
					free(c->recv_packet);
				} else {
					dest = c->recv_packet->dest;
					received_primary_msg_routingio(c, c->recv_packet->flags);						/* Part of the termination detection algorithm. */
					c->recv_packet->pos = 0;																		/* Set the position to the first byte of the packet. */
					update_stats(DATA_PACKETS_RECEIVED);

					if (dest == router_id) { 																		/* If destination of packet is here. */
						//TODO if the local receive buffer is full come up with a way to apply backpressure.
//						c->recv_packet->pos = 0;

						write_lock_receive_buffer();
						DL_APPEND(receive_buffer, c->recv_packet);													/* Deliver c->packet to user */
						receive_buffer_count++;
						update_stats(DATA_PACKETS_DELIVERED);
						unlock_receive_buffer();
					} else { 																						/* Else If need to forward */
						struct routing_connection* n = NULL;
						uint32_t next_hop;

						next_hop = find_next_hop(dest);																/* Get next hop for the packet. */
						if (next_hop == 0) {
							dno_print("io_loop: no routing information for packet destination found DROPPING PACKET.\n");
//							c->recv_packet->pos = 0;
							c->recv_packet->size = 0;
							continue;
						}

						HASH_FIND_INT(full_set, &next_hop, n);														/* Look up the connection to the next hop in the full set */
						if (n == NULL) {
							char ipstr[INET6_ADDRSTRLEN];
							convert_ip_ntop(next_hop, ipstr);
							dno_print("io_loop: no connection found for next hop %s.\n", ipstr);
							exit(EXIT_FAILURE);
						}

						if (n->buffer_count == BUFFER_SIZE) { 														/* If n.buffer is full, c needs to wait. */
							struct routing_connection* temp = c->hh_rs.next;										/* Save the pointer to the next item in the read set. */

//							c->recv_packet->pos = 0;																/* Set the position to the first byte of the packet. */

							enqueue(n, c);																			/* Put c in the queue of n. */
							HASH_DELETE(hh_rs, read_set, c);														/* Remove c from the read set. */

							c->hh_rs.next = temp;																	/* Restore the pointer to the next item in the read set so that the for loop can continue where it left of */

							continue;
						} else { 																					/* Else pass the packet to send buffer of the next hop. */
							struct routing_connection* is_in_ws;

//							c->recv_packet->pos = 0;																/* Set the position to the first byte of the packet. */

							DL_APPEND(n->buffer, c->recv_packet);													/* Put the packet in the send buffer of the next hop */
							n->buffer_count++;

							HASH_FIND(hh_ws, write_set, &(n->peer), sizeof(int), is_in_ws);
							if (is_in_ws == NULL) {
								HASH_ADD(hh_ws, write_set, peer, sizeof(int), n);									/* If it is not already in there, add the connection to the next hop to the write set. */
							}
						}
					}
				}

				c->recv_packet = init_packet();																	/* If this part of the function is reached, the previous receive buffer was either delivered to the user or forwarded. */
			}																									/* Therefore a new receive buffer is allocated to this connection. */
		}

//		unlock_sets();
//
//		//Unlocking and locking to allow other threads access to the shared variables (e.g. add_to_full_and_read_set())
//
//		write_lock_sets();

		for(c = write_set; c != NULL; c = c->hh_ws.next) {														/* Handle the connections that can be written to. */
			if (FD_ISSET(c->fd, &fd_write_set)) {
				if (c->buffer->new == 1) {
//					if (terminate /* && !is_term_packet(c->buffer->packet)TODO remove*/) {
//						increment_unack_msgs();
//						insert_term_packet(c, ACKREQ);
//					} TODO remove, replaced by set_bit_flags()

					set_bit_flags(c);																			/* Set current termination turn and ackreq bit flags in the packed packet_header. */

					c->buffer->new = 0;
				}
				rv = send(c->fd, c->buffer->packet + c->buffer->pos, c->buffer->size - c->buffer->pos, 0);		/* Try sending the packet. */
				if (rv == -1) {
					dno_print("io_loop: send to %s\n", c->neighbor_ip_string);
					exit(EXIT_FAILURE);

					//TODO find out the error and react appropriately.
					//If the connection is closed, mark it for deletion in the routing table,
					//remove it from all sets and free the routing_connection structure
					//remove it from the connections table
				}
				if (rv == c->buffer->size - c->buffer->pos) { 													/* The packet was fully sent. */
					if (!is_term_packet(c->buffer)) {
						//TODO remove: sent_primary_msg_routingio(c);											/* Part of the termination detection algorithm. */
						update_stats(DATA_PACKETS_FORWARDED);
					} else {
						if (c->buffer->type == ACK) {
							dno_log("Sent ACK to %s (data_connection)\n", c->neighbor_ip_string);
							update_stats(ACKS_SENT);
						} else if (c->buffer->type == ACKREQ) {
							dno_log("Sent ACKREQ to %s (data_connection)\n", c->neighbor_ip_string);
							update_stats(ACKREQS_SENT);
						} else {
							dno_print("Error: unrecognized packet. src: %d, dest: %d", c->buffer->src, c->buffer->dest);
						}
					}
					struct pckt* del = c->buffer;																/* Remove packet from list of packets and free its memory. */
					DL_DELETE(c->buffer, c->buffer);
					free(del->packet);
					free(del);
					c->buffer_count--;
				} else if (rv < c->buffer->size - c->buffer->pos) { 											/* If the packet was not fully sent, update its position and continue. */
					c->buffer->pos += rv;
					continue;
				}

				while (c->queue != NULL && c->buffer_count != BUFFER_SIZE) { 									/* While there are connections waiting and the current connection's buffer is not full. */
					struct routing_connection* w;

					w = dequeue(c);																				/* Get the next connection that is waiting in the queue. */

					DL_APPEND(c->buffer, w->recv_packet);														/* Put the packet from the waiting connection in the buffer of the current connection. */
					c->buffer_count++;

					w->recv_packet = init_packet();																/* Allocate a new receive buffer for the waiter. */

					HASH_ADD(hh_rs, read_set, peer, sizeof(int), w);											/* Add the waiting connection back to the read set. */
				}

				if (c->buffer_count == 0) {																		/* If the buffer of the current connection is empty. */
					struct routing_connection* temp = c->hh_ws.next;

					HASH_DELETE(hh_ws, write_set, c);															/* Delete it from the write set. */

					c->hh_ws.next = temp;
				}
			}
		}

		unlock_sets();
	}

	pthread_exit(NULL);
}

pthread_t create_routing_io_thread() {
	int 			rv;
	pthread_attr_t 	attr;
	pthread_t 		thread_id;

	/* Initialize and set thread detached attribute */
	rv = pthread_attr_init(&attr);
	if (rv != 0) {
		dno_print("create_routing_io_thread: Error initializing phtread attribute.");
		exit(EXIT_FAILURE);
	}
	rv = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	if (rv != 0) {
		dno_print("create_routing_io_thread: Error setting pthread attribute.");
		exit(EXIT_FAILURE);
	}

	/* Create thread */
	rv = pthread_create(&thread_id, &attr, io_loop, NULL);
	if (rv != 0) {
		dno_print("create_routing_io_thread: Error creating routing_io thread.");
		exit(EXIT_FAILURE);
	}

	rv = pthread_attr_destroy(&attr);
	if (rv != 0) {
		dno_print("create_routing_io_thread: Error destroying phtread attribute.");
		exit(EXIT_FAILURE);
	}

	return thread_id;
}

int dno_recv(char* buffer, int size) {
	struct pckt* temp;
	int pos = 0;
	int remaining_bytes_in_packet;

	write_lock_receive_buffer();
	while (receive_buffer != NULL && pos != size) {
		remaining_bytes_in_packet = receive_buffer->size - receive_buffer->pos;

		if (remaining_bytes_in_packet <= size - pos) {
			memcpy(buffer+pos, receive_buffer->packet, remaining_bytes_in_packet);
			pos += remaining_bytes_in_packet;

			temp = receive_buffer;
			DL_DELETE(receive_buffer, temp);
			free(temp->packet);
			free(temp);

			receive_buffer_count--;
		} else {
			memcpy(buffer+pos, receive_buffer->packet, size - pos);
			receive_buffer->pos += size - pos;
			pos	 				+= size - pos;
		}
	}
	unlock_receive_buffer();

	return pos;
}

uint32_t dno_send(uint32_t dest, char* buffer, uint32_t size) {
	uint32_t pos = 0;
	uint32_t max_payload_size = PACKET_SIZE-PACKET_HEADER;
	struct pckt* packet;

	if (dest == router_id) {
		dno_print("dno_send: invalid argument: local host's router_id as destination\n");
		exit(EXIT_FAILURE);
	}

	write_lock_send_buffer();

	while (send_buffer_count != BUFFER_SIZE && pos != size) {
		uint32_t aux_pos = 0;

		packet 			= init_packet();
		packet->src	 	= router_id;
		packet->dest 	= dest;
		if (size - pos <= max_payload_size) {
			packet->packet 	= (char *) malloc(size - pos + PACKET_HEADER);
			packet->size 	= size - pos + PACKET_HEADER;
			aux_pos 		= size - pos;
		} else {
			packet->packet 	= (char *) malloc(PACKET_SIZE);
			packet->size 	= PACKET_SIZE;
			aux_pos 		= max_payload_size;
		}
		if (packet->packet == NULL) {
			dno_print("dno_send_debug: error allocating memory");
			exit(EXIT_FAILURE);
		}

		write_packet_header(dest, router_id, packet->size, packet->packet, 0);

		memcpy(packet->packet + PACKET_HEADER, buffer + pos, packet->size-PACKET_HEADER);

		pos += aux_pos;

		DL_APPEND(send_buffer, packet);
		send_buffer_count++;
		update_stats(DATA_PACKETS_ORIGINATED);
	}

	unlock_send_buffer();

	return pos;
}

void dno_send_debug(uint32_t destination, uint32_t gb) {
	uint32_t pos 		= 0;
	uint32_t aux_pos 	= 0;
	uint32_t chunk_size = PACKET_SIZE-PACKET_HEADER;
	uint32_t size 		= 1000000000;

	char* buffer 		= (char*) malloc(chunk_size);
	if (buffer == NULL) {
		dno_print("dno_send_debug: error allocating memory");
		exit(EXIT_FAILURE);
	}

	while (gb != 0) {
		if ((pos <= (((2^32)-1)-chunk_size)) && pos + chunk_size <= size) {
			aux_pos = 0;

			while (aux_pos != chunk_size) {
				aux_pos += dno_send(destination, buffer, chunk_size);
			}

			pos += chunk_size;
		} else {
			uint32_t rest = size - pos;
			char* rest_buffer = (char*) malloc(rest);
			if (rest_buffer == NULL) {
				dno_print("dno_send_debug: error allocating memory");
				exit(EXIT_FAILURE);
			}

			aux_pos = 0;

			while (aux_pos != rest) {
				aux_pos += dno_send(destination, rest_buffer, rest);
			}

			pos += rest;

			if (pos == size) {
				gb--;
				pos = 0;
			}

			free(rest_buffer);
		}
	}

	free(buffer);
}

void print_routing_connections() {
	struct routing_connection* rconnection, *tmp;

	dno_printf(0, "========== Printing routing connections ==========");

	read_lock_sets();

	HASH_ITER(hh, full_set, rconnection, tmp) {
		dno_printf(0, "neighbor_id: %15s\t buffer count: %5d", rconnection->neighbor_ip_string, rconnection->buffer_count);
	}

	unlock_sets();

    dno_printf(1, "==================================================");
}

void print_data_sent() {
	read_lock_send_buffer();
	read_lock_receive_buffer();

	dno_printf(0, "========== Sent data statistics ==========");

	dno_printf(0, "Send-buffer count:\t\t%d", send_buffer_count);
	dno_printf(0, "Receive-buffer count:\t\t%d", receive_buffer_count);

	dno_printf(1, "==========================================");

	unlock_receive_buffer();
	unlock_send_buffer();
}

void read_lock_sets() {
    if (pthread_rwlock_rdlock(&sets_lock) != 0) {
    	dno_print("read_lock_sets: Can't acquire readlock.\n");
    	exit(EXIT_FAILURE);
    }
}

void write_lock_sets_priv(const char *file, int line, const char *function) {
	int rv;
	char filename[MAX_HOSTNAME_LENGTH+1];
	FILE* dbgfile;

	if (LOG) {
		sprintf(filename, "%s_sets_lock.dbg", hostname);
		dbgfile = fopen(filename, "a");

		fprintf(dbgfile, "%s(%d):%s() trying to write-lock sets_lock.\n", file, line, function);
		fclose(dbgfile);
	}

	rv = pthread_rwlock_wrlock(&sets_lock);
    if (rv != 0) {
    	dno_print("write_lock_sets: Can't acquire writelock.\n");
    	dno_print("%s(%d):%s() Called write_lock_sets.", file, line, function);
    	switch(rv) {
    	case EBUSY:
    		dno_print("Error: EBUSY");
    		break;
    	case EINVAL:
    		dno_print("Error: EINVAL");
    		break;
    	case EDEADLK:
    		dno_print("Error: EDEADLK");
    		break;
    	default:
    		break;
    	}
    	exit(EXIT_FAILURE);
    }

    if (LOG) {
		dbgfile = fopen(filename, "a");
		fprintf(dbgfile, "%s(%d):%s() write-locked sets_lock.\n", file, line, function);
		fclose(dbgfile);
    }
}

void unlock_sets() {
	if(pthread_rwlock_unlock(&sets_lock) != 0) {
		dno_print("Error unlocking sets_lock.\n");
		exit(EXIT_FAILURE);
	}
}

void read_lock_receive_buffer() {
    if (pthread_rwlock_rdlock(&receive_buffer_lock) != 0) {
    	dno_print("read_lock_receive_buffer: Can't acquire readlock.\n");
    	exit(EXIT_FAILURE);
    }
}

void write_lock_receive_buffer() {
    if (pthread_rwlock_wrlock(&receive_buffer_lock) != 0) {
    	dno_print("write_lock_receive_buffer: Can't acquire writelock.\n");
    	exit(EXIT_FAILURE);
    }
}

void unlock_receive_buffer() {
	if(pthread_rwlock_unlock(&receive_buffer_lock) != 0) {
		dno_print("Error unlocking receive_buffer_lock.\n");
		exit(EXIT_FAILURE);
	}
}

void read_lock_send_buffer() {
    if (pthread_rwlock_rdlock(&send_buffer_lock) != 0) {
    	dno_print("read_lock_send_buffer: Can't acquire readlock.\n");
    	exit(EXIT_FAILURE);
    }
}

void write_lock_send_buffer() {
    if (pthread_rwlock_wrlock(&send_buffer_lock) != 0) {
    	dno_print("write_lock_send_buffer: Can't acquire writelock.\n");
    	exit(EXIT_FAILURE);
    }
}

void unlock_send_buffer() {
	if(pthread_rwlock_unlock(&send_buffer_lock) != 0) {
		dno_print("Error unlocking send_buffer_lock.\n");
		exit(EXIT_FAILURE);
	}
}
