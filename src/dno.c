/*
 * dno.c
 *
 *  Created on: Aug 2, 2010
 *      Author: mritman
 *
 *      TODO
 *      	- Only supports IP4 but the networking code is largely IP version independent
 *      	- It is assumed that just the IP can be used to identify a node, this means that only one process per node is supported
 *
 *      	- Select in the IO loop will set an fd as readable even if it points to EOF, this causes a lot of unnecessary calls to recv
 *
 *			- Currently there is only one link state database, when multiple areas are implemented, support for multiple databases has to be implemented as well.
 *			-
 *			- Connections key field is an int but sometimes is used as a uint32_t
 *			-
 *			- Packets on the fd_info connection should always be sent in their entirety or data from different packets can get mangled when different threads send
 *			- data over these connections.
 *			-
 *			-
 *			- Each area needs its own parent and termination detection algorithm.
 *			-
 *			- Add new connecting router to areas if it is not in there.
 *			- Reconnect to areas if last connection has been disconnected.
 *			-
 *			- Normally the LSA age field is used to decide if a newly received LSA is newer even if it's sequence number is lower. This can happen when a router leaves the overlay, then joins again
 *			- but starts using the default lowest sequence number. This however can not happen in the assumed situation the overlay is currently designed for. It is always assumed a router leaves the overlay cleanly,
 *			- in this process its LSA is flushed from all LSDBs in the overlay. This makes LSA aging unnecessary.
 *			-
 *			- BUG: sometimes when a router exits it causes other routers to receive an invalid packet and free it twice.
 *			-
 *			- TODO make a bit in the packet headers that signifies if an ACK is desired. This will reduce the number of packets sent and received.
 *			- TODO synchronize term_turn status at connection to existing overlay
 */

#define _GNU_SOURCE								/* Needed for getline(). */
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h>
#include <time.h>

#include "dno.h"
#include "xml.h"
#include "utils.h"
#include "area.h"
#include "socksvr.h"
#include "connections.h"
#include "routingtable.h"
#include "routingio.h"
#include "latency.h"
#include "linkstatedatabase.h"
#include "routinginfo.h"
#include "termination.h"
#include "receive.h"
#include "stats.h"

volatile sig_atomic_t 	run = 1;
uint32_t 				router_id;

int exit_flag = 0;

void	initialize();
void	handle_input();
void 	sigint_handler(int sig);
void 	cleanup(int sock_svr_fd, pthread_t thread_id_sock_svr, pthread_t thread_id_routinginfo, pthread_t thread_id_io, pthread_t thread_id_receive);
void	fill_buffer(char* buffer);
int 	check_buffer(char* buffer);
void* 	send_receive(void* args);
void 	send_receive_thread(char* sender, char* receiver, char* ip_receiver);

int main(int argc, char **argv) {
	int 			sock_svr_fd;
	int 			local_port;
	char			local_ip[INET6_ADDRSTRLEN];
//	int 			rv;
	pthread_t		thread_id_sock_svr;
	pthread_t		thread_id_routinginfo;
	pthread_t		thread_id_io;
	pthread_t		thread_id_receive;

	char			topology_file[256];

	initialize();

	if (argc == 1) {
		strncpy(topology_file, "dno/topologies/top.xml\0", 256);
	} else if (argc >= 1) {
		strncpy(topology_file, "dno/topologies/\0", 256);
		strncat(topology_file, argv[1], 256);
	}

	if (!(argc == 2 || argc == 4)) {
		dno_print("Incorrect number of arguments: %d. Usage: %s <filename> \n", argc, argv[0]);
		return(0);
	}

	if (argc == 4) {
		test_id = atoi(argv[3]);
	}

	sock_svr_fd = setup_sock_server_socket(local_ip, &local_port);												/* Setup the socket that will be used by the socket server thread. */

	add_self_to_lsdb();

	thread_id_sock_svr = create_socket_svr_thread(sock_svr_fd);													/* Create the thread to serve connections. */

	areas = parse_topology(topology_file, local_ip);															/* Parse the input file that contains the information about to which areas this router belongs and which other routers are part of these areas. */

	synchronize(); //give all other nodes time to setup TODO come up with a better solution, for example retry connecting after a timeout

	getlatencys();																								/* Measure and estimate latency's to each other router for each area. */

	select_connections(DENSITY);																				/* Select which routers to connect to based on their latency. */

	establish_connections();																					/* Establish connections. */

	select_parents(areas);

	synchronize();

	//dno_print("Starting routinginfo thread.");

	thread_id_routinginfo = create_routing_information_thread();

	send_parent_offers_unlocked();																				/* If this router found a parent during connections establishment send a parent offer to all connections that indicated not having a parent. */

	thread_id_receive = create_receive_thread();

	thread_id_io = create_routing_io_thread();

	if (argc <= 2) {
		if (gxp_id == 0) {
			printf("Type \"help\" for a list of commands\n");
			printf("\nReady>\n");
			fflush(stdout);
		}
		while (run && !exit_flag) {
			handle_input();
		}
	} else {
		struct timespec wait;

		if (strncmp(argv[2], "su", strlen(argv[2])) == 0) {
			wait.tv_sec 	= 0;
			wait.tv_nsec 	= 250000000;

			while (times_terminated < 1) {
				nanosleep(&wait, NULL);
			}

			//write_lock_termination();
			//free_dc_list();
			//free(parent);
			//free_children();
			//run = 0;
			//unlock_termination();

			exit(EXIT_SUCCESS);
		} else if (strncmp(argv[2], "ad", strlen(argv[2])) == 0) {
			wait.tv_sec 	= 0;
			wait.tv_nsec 	= 250000000;

			while (times_terminated < 1) {
				nanosleep(&wait, NULL);
			}
			disconnect();
		} else if (strncmp(argv[2], "sd", strlen(argv[2])) == 0) {
			wait.tv_sec 	= 0;
			wait.tv_nsec 	= 250000000;

			while (times_terminated < 1) {
				nanosleep(&wait, NULL);
			}

			if ((get_nr_of_gxp_processes() / 2 )-1 == gxp_id) {
				disconnect();
			} else {
				wait.tv_sec 	= 0;
				wait.tv_nsec 	= 250000000;

				while (times_terminated < 2) {
					nanosleep(&wait, NULL);
				}

				exit(EXIT_SUCCESS);
			}
		} else if (strncmp(argv[2], "bd", strlen(argv[2])) == 0) {
			DL_SORT(areas->routers, sort_routers_by_ip);

			wait.tv_sec 	= 0;
			wait.tv_nsec 	= 250000000;

			while (times_terminated < 1) {
				nanosleep(&wait, NULL);
			}

			if (router_id == get_router_id(areas->routers->next->ip_string)) {
				sleep(2);
				//dno_print("Disconnecting");
				disconnect();
			} else if (router_id == get_router_id(areas->routers->ip_string)) {
				uint32_t dest = get_router_id(areas->routers->prev->ip_string);

				//dno_print("Sending %s 1 GByte.", areas->routers->prev->ip_string);
				dno_send_debug(dest, 4);
				sleep(1);

				disconnect();
			} else {
				while (times_terminated < 3) {
					nanosleep(&wait, NULL);
				}
				disconnect();
			}
		}
	}

	cleanup(sock_svr_fd, thread_id_sock_svr, thread_id_routinginfo, thread_id_io, thread_id_receive);

	//dno_print("Main thread exited");

    return 0;
}

void initialize() {
	int 				rv;
	struct sigaction 	sa;

    sa.sa_handler 	= sigint_handler;
    sa.sa_flags 	= 0;
    sigemptyset(&sa.sa_mask);

    rv = sigaction(SIGINT, &sa, NULL);
    if (rv == -1) {
        dno_print_error("sigaction");
        exit(EXIT_FAILURE);
    }

    init_utils();

	area_init();

	connections_init();

	lsdb_init();

	termination_init();

	routinginfo_init();

	routingio_init();

	init_stats();

	dno_log("------------------------ New session ------------------------\n");
}

void handle_input() {
	char* 	line = NULL;							/* Buffer to hold a line from the input. */
	size_t 	len  = 0;								/* Initial size of the buffer. */
	ssize_t	read;									/* The return value of getline. */
	char	target_host[MAX_HOSTNAME_LENGTH];		/* Hostname of the host the command is meant for. */
	target_host[0] = '\0';

	const char	delimiters[] = " ,;:!-";			/* Delimiters of tokens. */
	char*		token;								/* Pointer to current token. */
	char*		cp;									/* Pointer to copy of line. A copy is made because strtok() alters the input string. */

	read = getline(&line, &len, stdin);				/* Read in a line from stdin. */
	if (read == -1) {
		if (line) free(line);						/* Free the memory allocated by getline. */
		return;
	}
	line[read-1] = '\0';							/* Replace the return character with the null character. */

	cp    = strdup(line);               			/* Make writable copy to be used with strtok. */
	token = strtok(cp, delimiters);      			/* Get the first token from the line. */

	if (token == NULL) {
		//do nothing
	} else if (strcmp(token, HELP) == 0) {
		if (gxp_id == 0) {
			printf(HELP_STRING);
			printf("\nReady>\n");
			fflush(stdout);
		}
	} else if (strcmp(token, hostname) == 0 || strcmp(token, ALL_ROUTERS) == 0) {
		printf("\n");

		strncpy(target_host, token, MAX_HOSTNAME_LENGTH-1);

		token = strtok(NULL, delimiters);    		/* Get the second token from the line. */

		if (token == NULL) {
			dno_print("No command argument supplied. Type a command of the form: <hostname> <argument>");
		} else if (strcmp(token, PRINT_AREAS) == 0) {
			print_areas();
		} else if (strcmp(token, PRINT_CONNECTIONS) == 0) {
			print_connections();
		} else if (strcmp(token, PRINT_LSDB) == 0) {
			print_link_state_database(1);
		} else if (strcmp(token, PRINT_LSRS) == 0) {
			print_lsrs();
		} else if (strcmp(token, PRINT_PARENT_AND_CHILDREN) == 0) {
			print_parent_and_children();
		} else if (strcmp(token, PRINT_ROUTING_TABLE) == 0) {
			print_routing_table();
		} else if (strcmp(token, PRINT_TERM_STATUS) == 0) {
			print_termination_status();
		} else if (strcmp(token, PRINT_R_CONNECTIONS) == 0) {
			print_routing_connections();
		} else if (strcmp(token, SEND) == 0) {
			char* dest_string = NULL;
			char* size_string = NULL;
			uint32_t size;
			uint32_t dest;

			dest_string = strtok(NULL, delimiters);
			size_string = strtok(NULL, delimiters);

			if (dest_string != NULL && size_string != NULL) {
				dest = get_router_id(dest_string);
				size = atoi(size_string);
				dno_print("Sending %"PRIu32" GByte.", size);
				dno_send_debug(dest, size);
			}
		} else if (strcmp(token, PRINT_DATA_SENT) == 0) {
			print_data_sent();
		} else if (strcmp(token, EXIT_PROGRAM) == 0) {
			disconnect();
			exit_flag = 1;
		} else if (strcmp(token, PRINT_STATS) == 0) {
			print_stats();
		}

		if (strncmp(hostname, target_host, MAX_HOSTNAME_LENGTH) == 0) {
			printf("\nReady>\n");
		} else if (strncmp(hostname, ALL_ROUTERS, MAX_HOSTNAME_LENGTH) == 0 && gxp_id == 0) {
			printf("\nReady>\n");
		}
		fflush(stdout);
	}

	if (line) free(line);							/* Free the memory allocated by getline. */
	if (cp)   free(cp);								/* Free the memory allocated by strtok. */
	return;
}

void sigint_handler(int sig) {
	run = 0;
}

void join_thread(pthread_t thread_id) {
	int rv;

	rv = pthread_join(thread_id, NULL);
	if (rv != 0) {
		dno_print("main: warning: pthread_join returned %d\n", rv);
	}
}

void cleanup(int sock_svr_fd, pthread_t thread_id_sock_svr, pthread_t thread_id_routinginfo, pthread_t thread_id_io, pthread_t thread_id_receive) {
	//dno_print("cleaning up");

	join_thread(thread_id_sock_svr);
	//dno_print("Joined sock_svr thread.");

	join_thread(thread_id_routinginfo);
	//dno_print("Joined routinginfo thread.");

	join_thread(thread_id_receive);
	//dno_print("Joined receive thread.");

	join_thread(thread_id_io);
	//dno_print("Joined io thread.");

	free_rsws();							/* Free the read-set and write-set (and full-set) in the routingio.c module */
	free_receive_buffer();					/* Free receive buffer */
	free_send_buffer();						/* Free send buffer. */
	close_connections();					/* Close and free connections */
	free_routing_table();					/* Free the routing table */
    free_areas();							/* Free the areas */
    free_link_state_database();				/* Free the link-state database. */
    free_stats();
}

