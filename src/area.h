/*
 * area.h
 *
 *  Created on: Nov 22, 2010
 *      Author: mritman
 */

#ifndef AREA_H_
#define AREA_H_

#include <arpa/inet.h>

#define HIGHEST 1
#define LOWEST 	0

extern struct area* 	areas;
extern pthread_rwlock_t areas_lock;

struct router {
	char			ip_string[INET6_ADDRSTRLEN];		/* The IP string of the router */
	uint32_t 		latency; 							/* Latency in microseconds */
	int 			estimated;							/* Indicates whether the latency has been measured or estimated. */
	int				connect;							/* Connect == 1 indicates that the local router will try to set up a connection to this router. */

	struct router* 	prev;
	struct router* 	next;
};

struct area {
	int				select_parent;						/* Indicates whether the routers of this area need to select an initial parent. */
	uint32_t		area_id;
	struct router* 	routers;							/* List of routers that constitute this area. */
	struct router*  this_router;						/* Pointer to the element in the routers list that corresponds to this router. */

	struct area*	next;
	struct area*	prev;
};

void 	area_init();
void	initialize_router(struct router* router);
void 	initialize_area(struct area* area);
void 	read_lock_areas();
void 	write_lock_areas();
void 	unlock_areas();
void 	print_areas();
void 	delete_router_from_areas_by_string(char* ip_string);
void 	delete_router_from_areas(uint32_t router_id);
void 	free_areas();
int		count_routers(struct router* routers);
int 	compare_routers(struct router* a, struct router* b);
int		sort_routers_by_ip();

#endif /* AREA_H_ */
