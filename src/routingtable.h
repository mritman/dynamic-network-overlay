/*
 * routingtable.h
 *
 *  Created on: Aug 15, 2010
 *      Author: mritman
 */

#ifndef ROUTINGTABLE_H_
#define ROUTINGTABLE_H_

#include "uthash.h"
#include "graph.h"

extern struct routing_table_entry* 	routing_table;
extern pthread_rwlock_t 			routing_table_lock;

struct routing_table_entry {
	uint32_t		destination;			/* The key of the destination ip of the packet. */
	uint32_t		next_hop;				/* The key of the connection to the next hop. */

	UT_hash_handle 	hh;						/* Required for uthash */
};

void 	add_routing_entry(struct routing_table_entry* new_entry);

struct 	routing_table_entry* find_routing_entry(int destination);

void 	delete_routing_entry(struct routing_table_entry* entry);

void 	calculate_routing_table(struct graph* graph);

void 	print_routing_table();

uint32_t find_next_hop(uint32_t dest);

void 	free_routing_table();

#endif /* ROUTINGTABLE_H_ */
