HOW TO COMPILE

- glibc v2.7 or higher works, older versions may give an error when compiling
- Libxml2 is required: http://xmlsoft.org/

to compile run the makefile in the debug folder

HOW TO USE

The executable is named "dno" as in "dynamic network overlay".
It requires GXP to run: 

http://www.logos.ic.i.u-tokyo.ac.jp/gxp/index.php?FrontPage

Below is an example of how to run dno on two nodes.

The nodes in the example are:
node1.somedomain.com
node2.somedomain.com

Dno requires as input an XML file that tells it the nodes that will comprise the overlay.
For a two node overlay it would look something like this:

<?xml version="1.0"?>
<overlay_topology>
    <area>
	<select_parent>1</select_parent>
	<name>area_name</name>
	<area_id>0</area_id>
        <ip_range>
            <ip>192.168.1.</ip>
            <start>1</start>
	    <end>2</end>
        </ip_range>
    </area>
</overlay_topology>

The area name and id are not used so are not used, so are not important. What is important is the ip_range element.
There can be as many of these ip_range elements as needed. In the end, all that each dno process needs as input is a list of all nodesin the overlay that it needs to consider connecting to.

In this example you will want to run dno on both node 1 and node 2. This means that you need to explore both node1 and node2 with gxp before executing dno. Exploring nodes with gxp means that you can use gxp to execute commands simultaneously on these nodes or a subset of these nodes.

Before using GXP to run the executable on the participating nodes, the executable and topology files have to be on the filesystem of the participating nodes. It is easiest to use the same file hierarchy on all nodes.
I place the following files and folders in my home directory and run gxp from my home directory:

~/dno/debug/dno (the executable)
~/dno/topologies/top.xml (the topology file)

It is also best to run the commands below from the same directory as you will be running dno from (your home directory) because I believe this determines GXP's working directory. So in this example, make it your home directory as well.

First gxp needs to know how to reach the nodes that will be explored:

	gxpc use ssh node1 node

This tells gxp to use ssh from node1 to explore nodes whose hostname starts with "node".

Now type:

	gxpc explore node2.somedomain.com

You can also explore multiple nodes at the same time:

	gxpc explore node[[2-6]].somedomain.com

For full documentation please see the GXP website.

Then to start dno:

	gxpc mw dno/debug/dno 

If you run dno without any arguments it will try to load the file dno/topologies/top.xml. This can be used as default topology file. You can also give as first argument a filename, for example 2.xml:

	gxpc mw dno/debug/dno 2.xml

It will then look for the file dno/topologies/2.xml, so place the 2.xml file in the topologies folder.

Either of the above commands will start dno in interactive mode. In this mode you can type commands to inspect the state of the overlay. The following commands are available:

NOTE: all these commands have to be preceded by either "all" or a hostname to indicate by which nodes the command should be executed. 
EXAMPLE: "node1 connections" will cause node1 to print its connections, "all connections" will cause all nodes to print their connections

exit: exit the program
help: print this list of commands and how to use them
areas: print the areas
connections: print the connections used for routing algorithm
rconnections: print the the connections used for forwarding data
table: print the routing table
lsdb: print the contents of the link-state database
lsrs: print the link-state request list for each connection
pac: print the parent and children
term: print the variables related to the termination algorithm
send: use "send <ip_address> <number_of_gbytes>" to send a number of GBs to another node in the overlay
data: print statistics about data tranfers
stats: print stats related to OSPF and the termination algorithm

Instead of running dno in interactive mode you can run it in the following modes, that will cause it to run a scenario and print termination delay and packet statistics to the standerd output. The different scenarios are:

su : Stands for startup, the overlay will startup, wait for termination, print the termination stats and exit.
ad : Stands for all disconnect, the overlay will start up, print termination, disconnect, print termination and exit
sd : Stands for single disconnect, the overlay will start up, print termination, one node will disconnect, the remaining will print termination, the remaining nodes will exit
bd : Stands for bandwidth, the overlay will start up, print termination status, a node will begin a tranfer to another node, a third node will disconnect, the remaining nodes terminate and print termination status, the tranfser will finish, the remaining nodes will disconnect and print termination.

su is the most stable, the other scenarios may trigger bugs especially with a larger number of nodes.

The output of these scenarios is not labeled and meant to be redirected to a file. The resulting file will be a CSV file that for example can be imported into excel.

To run the overlay in one of these scenarios type:

	gxpc mw dno/debug/dno <topology_filename> <scenario>

Example:
	 gxpc mw dno/debug/dno 2.xml su

KILLDNO
When pressing Ctrl+C while running gxpc, for example if one of the dno processes crashed and the overlay is locked, it will try to stop all child processes on all machines.
Sometimes it fails to stop them all. In this can you can run killdno to kill the remaining dno processes.

LOGGING
You can enable logging by setting LOGGING to 1 in utils.h
