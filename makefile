SRC = src/
TMP = makefiletmp/
TMP2 = makefiletmp/
TMP3 = makefiletmp/

${TMP}% : ${SRC}%.c ${SRC}%.h
	scp $? mritman@${TARGET}:./dno/src/
	touch $@

${TMP2}% : ${SRC}%.h
	scp $? mritman@${TARGET}:./dno/src/
	touch $@

${TMP3}mkfl : debug/makefile
	scp $? mritman@${TARGET}:./dno/$?
	touch $@

